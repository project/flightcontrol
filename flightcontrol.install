<?php
/**
 * @file
 * Install, update and uninstall functions for the minimal installation profile.
 */

/**
 * Implements hook_install().
 *
 * Performs actions to set up the site for this profile.
 *
 * @see system_install()
 */
function flightcontrol_install() {
  // Allow visitor account creation, but with administrative approval.
  variable_set('user_register', USER_REGISTER_ADMINISTRATORS_ONLY);

  // Set default front page.
  variable_set('site_frontpage', 'dashboard');
}

/**
 * Implements hook_install_tasks().
 *
 * A list of custom tasks that will install my additional functionality.
 */
function flightcontrol_install_tasks($install_state) {

  // Include common helpers.
  module_load_include('inc', 'flightcontrol', 'flightcontrol.common');


  // Include tasks.
  module_load_include('inc', 'flightcontrol', 'flightcontrol.tasks');

  // Remove any status messages that might have been set. They are unneeded.
  drupal_get_messages('status', TRUE);

  $tasks = array();

  $tasks['_flightcontrol_enable_modules'] = array(
    'display_name' => st('Enable flight control modules.'),
    'type'         => 'batch',
  );

  $tasks['_flightcontrol_install_settings'] = array(
    'display_name' => st('Install flight control settings.'),
    'type'         => 'batch',
  );

  return $tasks;
}

/**
 * Implements hook_install_tasks_alter().
 */
function flightcontrol_install_tasks_alter(&$tasks, $install_state) {

  // Put flight control tasks just before configure_form task.
  // It's just a tiny beatification, to do all install processing before
  // the configuration form is presented.
  $flightcontrol_task_deltas = array(
    '_flightcontrol_enable_modules',
    '_flightcontrol_install_settings',
  );

  // Get original flightcontrol tasks from tasks array,
  // set them aside and unset in original tasks array.
  $flightcontrol_tasks = array();
  foreach ($flightcontrol_task_deltas as $delta) {
    $flightcontrol_tasks[$delta] = $tasks[$delta];
    unset($tasks[$delta]);
  }

  // Loop through tasks.
  // If task 'configure form' is hit, just insert flightcontrol tasks first.
  // Then proceed.
  $ordered_tasks = array();
  foreach ($tasks as $delta => $task) {
    if ($delta == 'install_configure_form') {
      $ordered_tasks = array_merge($ordered_tasks, $flightcontrol_tasks);
    }
    $ordered_tasks[$delta] = $task;
  }
  $tasks = $ordered_tasks;
}
