<?php
/**
 * @file
 * Enables modules and site configuration for a flightcontrol site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function flightcontrol_form_install_configure_form_alter(&$form, $form_state) {
  // When using Drush, no form alters.
  if (drupal_is_cli()) {
    return;
  }
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = 'Drupal Flight Control';

  // Configure email address for flightcontrol notifications.
  $form['site_information']['deployer_notification_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Send Flightcontrol notifications to this address, if any'),
    '#default_value' => variable_get('deployer_notification_email', ''),
    '#required' => FALSE,
    '#description' => t('Used for notifications, such as security updates for REMOTE projects.<br/>Leave empty to suppress email notifications'),
  );
  $form['#submit'][] = 'flightcontrol_install_configure_form_submit';
}
