<?php
/**
 * @file
 * The module file.
 */

/**
 * Implements hook_menu().
 */
function deployer_profit_counter_menu() {
  $items['admin/deployer/profit_counter'] = array(
    'title' => 'Profit counter settings',
    'description' => 'Profit counter related configuration.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('deployer_profit_counter_admin'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'deployer_profit_counter.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_block_info().
 */
function deployer_profit_counter_block_info() {
  $blocks = array();
  $blocks['deployer_profit_counter'] = array(
    'info' => t('Counts profit for each deploy.'),
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function deployer_profit_counter_block_view($delta = '') {
  // The $delta parameter tells us which block is being requested.
  $block = array();
  switch ($delta) {
    case 'deployer_profit_counter':
      global $user;
      if (in_array('authenticated user', $user->roles)) {
        $block['subject'] = t('Profit overview');
        $block['content'] = _deployer_profit_counter_content($delta);
      }
      break;
  }
  return $block;
}

/**
 * Returns data for profit counter.
 *
 * Query for deploy nodes, for each period defined.
 * Categorize nodes wrt
 *  - backups (commit tag '--dummy--')
 *  - test deploys
 *  - acceptence deploys
 *  - production deploys
 *
 * @param string $delta
 *   Which block is this ?
 *
 * @return array
 *   Render array.
 */
function _deployer_profit_counter_content($delta) {

  // Time to calculate profit with.
  $spent_backup = variable_get('deployer_deploy_time_backup', 20);
  $spent_test = variable_get('deployer_deploy_time_test', 30);
  $spent_accept = variable_get('deployer_deploy_time_accept', 30);
  $spent_prod = variable_get('deployer_deploy_time_prod', 60);

  // Define periods for calculation.
  $resume = array(
    array(
      'age' => 604800,
      'title' => t('last 7 days'),
    ),
    array(
      'age' => 2592000,
      'title' => t('last 30 days'),
    ),
    array(
      'age' => 31536000,
      'title' => t('last 365 days'),
    ),
    array(
      // 100 years. Must be enough ?
      'age' => 3153600000,
      'title' => t('all time'),
    ),
  );

  $markup = '';
  foreach ($resume as $period) {
    $age = time() - $period['age'];
    $query = db_select('node', 'n');

    // Environment referenced from deploy.
    $query->join('field_data_field_environment_id', 'env', 'n.nid = env.entity_id');
    $query->join('node', 'env_node', 'env_node.nid = env.field_environment_id_target_id');
    $query->join('field_data_field_server_type', 'type', 'type.entity_id = env_node.nid');
    $query->join('field_data_field_commit_tag', 'ctag', 'n.nid = ctag.entity_id');

    $query->condition('n.type', 'deploy');
    $query->condition('n.status', 1);
    $query->condition('n.created', $age, '>');
    $query->fields('n', array('nid', 'created', 'title'));
    $query->fields('type', array('field_server_type_value'));
    $query->fields('ctag', array('field_commit_tag_value'));
    $results = $query->execute();

    // Server type OTAP, prod, accept or lower (test).
    $test = 0;
    $accept = 0;
    $prod = 0;
    $backup = 0;
    foreach ($results as $row) {
      if ($row->field_commit_tag_value == '--dummy--') {
        $backup++;
      }
      elseif ($row->field_server_type_value <= 1) {
        $test++;
      }
      elseif ($row->field_server_type_value == 2) {
        $accept++;
      }
      elseif ($row->field_server_type_value == 3) {
        $prod++;
      }
    }

    $total = $results->rowCount();

    $time_saved_backups = round($backup * $spent_backup / 60);
    $time_saved_test = round($test * $spent_test / 60);
    $time_saved_accept = round($accept * $spent_accept / 60);
    $time_saved_prod = round($prod * $spent_prod / 60);

    $time_saved_tot = $time_saved_backups + $time_saved_test + $time_saved_accept + $time_saved_prod;

    $vars = array();
    $vars['label_hidden'] = 0;
    $vars['title_attributes'] = '';
    $vars['label'] = t('Profit for @time', array('@time' => $period['title']));
    $vars['content_attributes'] = '';
    $vars['classes'] = 'deployer_profit_counter';
    $vars['attributes'] = '';

    $vars['items'][]['#markup'] = t('Backups: @backups (@h hours)', array('@backups' => $backup, '@h' => $time_saved_backups));
    $vars['item_attributes'][] = '';

    $vars['items'][]['#markup'] = t('Test deploys: @test (@h hours)', array('@test' => $test, '@h' => $time_saved_test));
    $vars['item_attributes'][] = '';

    $vars['items'][]['#markup'] = t('Acceptation deploys: @accept (@h hours).', array('@accept' => $accept, '@h' => $time_saved_accept));
    $vars['item_attributes'][] = '';

    $vars['items'][]['#markup'] = t('Production deploys: @prod (@h hours).', array('@prod' => $prod, '@h' => $time_saved_prod));
    $vars['item_attributes'][] = '';

    $vars['items'][]['#markup'] = t('Total: @d (@h hours).', array('@d' => $total, '@h' => $time_saved_tot));
    $vars['item_attributes'][] = '';

    $markup .= theme_field($vars);
  }

  $result = array(
    '#markup' => $markup,
  );
  return $result;
}
