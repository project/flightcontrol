<?php

/**
 * @file
 * The administrative function for the deployer_profit_counter module.
 */

/**
 * Admin form used to congure the deployer_profit_counter module.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array
 *
 * @return array
 *   A renderable array of the form.
 */
function deployer_profit_counter_admin($form, &$form_state) {
  $form['deployer_deploy_time_backup'] = array(
    '#type' => 'textfield',
    '#title' => t('Time used per seperate backup job'),
    '#default_value' => variable_get('deployer_deploy_time_backup', ''),
    '#required' => TRUE,
    '#description' => t('Used for profit counter.'),
  );

  $form['deployer_deploy_time_test'] = array(
    '#type' => 'textfield',
    '#title' => t('Time used per deploy (TEST)'),
    '#default_value' => variable_get('deployer_deploy_time_test', ''),
    '#required' => TRUE,
    '#description' => t('Used for profit counter.'),
  );

  $form['deployer_deploy_time_accept'] = array(
    '#type' => 'textfield',
    '#title' => t('Time used per deploy (ACCEPT)'),
    '#default_value' => variable_get('deployer_deploy_time_accept', ''),
    '#required' => TRUE,
    '#description' => t('Used for profit counter.'),
  );

  $form['deployer_deploy_time_prod'] = array(
    '#type' => 'textfield',
    '#title' => t('Time used per deploy (PROD)'),
    '#default_value' => variable_get('deployer_deploy_time_prod', ''),
    '#required' => TRUE,
    '#description' => t('Used for profit counter.'),
  );

  return system_settings_form($form);
}
