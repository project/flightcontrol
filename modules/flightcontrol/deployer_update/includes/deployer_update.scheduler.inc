<?php
/**
 * @file
 * This file provides scheduler functions.
 *
 * Updates are performed on cron time, in N parallel
 * processes.
 */

// Parallel processes to run updates.
const DEPLOYER_UPDATE_PARALLEL_PROC = 10;

// Load all helpers.
module_load_include('inc', 'deployer_update', 'includes/deployer_update.helpers');

/**
 * Function to create health checks queues.
 *
 * Each queue is handled by separate spawned php process.
 * Only add environments to queue if health-check has expired.
 */
function deployer_update_create_update_queue() {
  $last_run = variable_get('deployer_update_updates_last_run', 0);
  $min_interval = variable_get('deployer_update_interval', 7200);
  if ($last_run <= time() - $min_interval) {
    // Initiate queues.
    // One for each concurrent process.
    $queues = array();
    $queues_empty = TRUE;
    for ($index = 0; $index < DEPLOYER_UPDATE_PARALLEL_PROC; $index++) {
      $queues[$index] = DrupalQueue::get('deployer_update_queue_' . $index, $reliable = TRUE);
      // There is no harm in trying to recreate existing.
      $queues[$index]->createQueue();

      $count = $queues[$index]->numberOfItems();
      if ($count) {
        $queues_empty = FALSE;
      }
    }

    // Get all projects.
    // Only if all queues are empty.
    if ($queues_empty) {

      // Get published projects where auto-updates are turned on.
      $query = db_select('node', 'n')
        ->condition('n.status', 1, '=')
        ->fields('n', array('nid'));
      // Auto-update fields.
      $query->join('field_data_field_auto_update_core', 'up_core', 'n.nid = up_core.entity_id');
      $query->join('field_data_field_auto_updates', 'up_mods', 'n.nid = up_mods.entity_id');
      $or_cond = db_or()->condition('up_core.field_auto_update_core_value', 1)->condition('up_mods.field_auto_updates_value', 1);
      $query->condition($or_cond);
      // Group by deploy-nid.
      $query->groupBy('n.nid');
      $result = $query->execute();

      // Add servers to queue.
      $queue_index = 0;
      foreach ($result as $row) {
        $queues[$queue_index]->createItem($row->nid);

        // Cycle queues round robin.
        if ($queue_index == (count($queues) - 1)) {
          $queue_index = 0;
        }
        else {
          $queue_index++;
        }
      }
    }
    variable_set('deployer_update_updates_last_run', time());
  }
}

/**
 * Handle updates for projects.
 *
 * Invokes nohup processes to perform updates in parallel.
 */
function deployer_update_cron_process() {
  module_load_include('module', 'deployer', 'deployer');

  // Check drush binary.
  if (!deployer_check_local_drush()) {
    watchdog('deployer_update', 'Cannot perform updates. Local drush binary not found or executable!', array(), WATCHDOG_WARNING, $link = NULL);
    return;
  }

  // Update queue (create new, or add environments).
  deployer_update_create_update_queue();

  // Spawn new process for each queue.
  for ($index = 0; $index < DEPLOYER_UPDATE_PARALLEL_PROC; $index++) {
    $queue = DrupalQueue::get('deployer_update_queue_' . $index, $reliable = TRUE);
    // There is no harm in trying to recreate existing.
    $queue->createQueue();

    // Items in queue? Spawn.
    $count = $queue->numberOfItems();
    if ($count) {
      _deployer_update_debug(t('Invoking process for auto-updates queue @index (@num items)', array('@index' => $index, '@num' => $count)), __FUNCTION__);
      deployer_update_queue_invoke($index);
    }
  }
}

/**
 * Spawn new drush process to perform updates.
 *
 * @param int $index
 *   Queue index.
 */
function deployer_update_queue_invoke($index) {
  exec('nohup drush deployer_update_process_queue ' . $index . ' > /dev/null 2>&1 &');
}

/**
 * Process health check queue.
 *
 * Invoked by drush.
 *
 * @param int $index
 *   Queue index.
 */
function deployer_update_process_queue($index) {
  if ($index >= 0 && $index <= DEPLOYER_UPDATE_PARALLEL_PROC) {

    // Detect debug mode.
    // Stuff is printed with 'dd()'.
    $debug = variable_get('deployer_update_debug', FALSE);
    if ($debug && !function_exists('dd')) {
      $debug = FALSE;
    }

    $start = time();

    $queue = DrupalQueue::get('deployer_update_queue_' . $index, $reliable = TRUE);
    $count = $queue->numberOfItems();
    if (!$count) {
      $debug ? dd(t('No items in queue @index', array('@index' => $index))) : NULL;
      return;
    }

    $debug ? dd(t('Updates check queue @index: Starting. Need to check @num projects for updates', array('@num' => $count))) : NULL;

    // Claim new item 30s.
    while ($item = $queue->claimItem(30)) {
      $project_id = $item->data;

      // Perform update checks.
      module_load_include('inc', 'deployer_update', 'includes/deployer_update.project');
      deployer_update_project($project_id);

      $queue->deleteItem($item);

      // Stop if time's up.
      if (time() > $start + DEPLOYER_UPDATE_QUEUE_RUN_TIME) {
        $debug ? dd(t('Updates queue @index: Time is up! will process rest next time.')) : NULL;
        break;
      }
    }
  }
}
