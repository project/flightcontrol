<?php
/**
 * @file
 * Provides functions to fetch module last release info.
 *
 * Latest release info stored in variable 'deployer_project_releases'.
 */

// Default mod whitelist.
define('DEPLOYER_UPDATE_MODULE_WHITELIST', 'admin_menu
devel
date
ds
ds_extras
ctools
field_group
entityreference
field_collection
link
menu_block
menu_breadcrumb
pathauto
views
features
features_diff
strongarm
webform
wysiwyg
metatag
honeypot
libraries
token
module_filter
better_formats
paragraphs
search_api
search_api_db
search_api_autocomplete
facetapi
locale
l10n_update');

/**
 * Get modules on whitelist.
 *
 * @return array
 *   Modules.
 */
function deployer_update_get_whitelist() {
  $whitelist = variable_get('deployer_update_whitelist', DEPLOYER_UPDATE_MODULE_WHITELIST);
  if (!empty($whitelist)) {
    $modules = explode(PHP_EOL, $whitelist);

    // Trim whitespace etc.
    foreach ($modules as &$module) {
      $module = trim($module);
    }
    return $modules;
  }
  return array();
}

/**
 * Fetch release-info for all projects on whitelist.
 */
function deployer_update_fetch_whitelist_releases() {
  $projects = deployer_update_get_whitelist();

  // Also fetch release info for drupal core.
  if (!in_array('drupal', $projects)) {
    $projects[] = 'drupal';
  }

  // Fetch.
  foreach ($projects as $project) {
    deployer_update_fetch_project_releases($project, '7.x');
    deployer_update_fetch_project_releases($project, '6.x');
  }
}

/**
 * Processes a task to fetch available update data for a single project.
 *
 * Once the release history XML data is downloaded, it is parsed and saved into
 * the {cache_update} table in an entry just for that project.
 *
 * @param string $project
 *   Associative array of information about the project to fetch data for.
 * @param string $core_version
 *   Drupal core version.
 *
 * @return BOOL
 *   TRUE if we fetched parsable XML, otherwise FALSE.
 */
function deployer_update_fetch_project_releases($project, $core_version = '7.x') {

  module_load_include('inc', 'update', 'update.fetch');

  $url = UPDATE_DEFAULT_URL;
  $url .= '/' . $project . '/' . $core_version;

  $xml = drupal_http_request($url);
  if (!isset($xml->error) && isset($xml->data)) {
    $data = $xml->data;
  }
  else {
    watchdog('deployer_update', 'Unable to fetch releases for project "@project", core version "@core"', array('@project' => $project, '@core' => $core_version), WATCHDOG_ALERT, $link = NULL);
  }

  if (!empty($data)) {
    $available = update_parse_xml($data);
    if (!empty($available)) {
      $releases = variable_get('deployer_project_releases', array());
      $security_release_date = array();

      // Get last security-version for project.
      foreach ($available['releases'] as $version => $release) {
        // Set date for this major version to 0 of not already set.
        !isset($security_release_date[$release['version_major']]) ? $security_release_date[$release['version_major']] = 0 : NULL;

        if (isset($release['terms']['Release type'])
          // Security update.
          && in_array('Security update', $release['terms']['Release type'])
          // Release date higher than last one or 0.
          // Used to replace $security_release_date in each foreach-cycle, to
          // get latest release. We don't know order of supplied
          // releases in array.
          && $release['date'] > $security_release_date[$release['version_major']] &&
          // Version_extra must be empty (usually used for 'unstable3',
          // 'beta2' etc.
          (!isset($release['version_extra']) || empty($release['version_extra']))) {

          // Set newest security version for project, core version and
          // major version.
          $security_release_date[$release['version_major']] = $release['date'];
          $releases[$project][$core_version]['security_versions'][$release['version_major']] = array(
            'version' => $version,
            'datestamp' => $release['date'],
          );
        }
      }

      $releases[$project][$core_version]['all'] = $available;
      variable_set('deployer_project_releases', $releases);
    }
  }
}
