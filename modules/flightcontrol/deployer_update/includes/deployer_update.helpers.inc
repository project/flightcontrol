<?php
/**
 * @file
 * Provides functions to
 *  - Provide static folders to save repo data etc.
 *  - parse project versions.
 *    Code stolen from drush.
 *    @see http://api.drush.ws/api/drush/commands%21pm%21pm.drush.inc/function/pm_parse_version/master .
 *  - Output debug: _deployer_update_debug();
 *  - Send email report: _deployer_update_send_report();
 *  - Check if a module is locked: _deployer_update_module_locked().
 *  - Set static values for $label: _deployer_update_statics().
 *  - Set message: _deployer_update_set_message().
 */

/**
 * Class DeployerExec
 *
 * Excute shell commands.
 * Available properties:
 *   ->output  : ouput array
 *   ->status  : shell exit status
 *   ->ok      : TRUE on success, FALSE on failure
 */
class DeployerExec {
  /**
   * Execute command.
   *
   * @param string $cmd
   *   Shell command.
   * @param string $path
   *   Path to execute from.
   */
  public function __construct($cmd, $path = '') {
    // Keep current folder to return to it.
    $oldpwd = getcwd();

    // First get into path ?
    if (!empty($path)) {
      $cmd = 'cd ' . $path . ' && ' . $cmd;
    }

    $this->output = array();
    $this->status = 1;
    $this->ok = FALSE;
    exec($cmd, $this->output, $this->status);

    if ($this->status === 0) {
      $this->ok = TRUE;
    }

    // Always get back to current working folder.
    chdir($oldpwd);
  }
}

/**
 * Debug function, to output in /tmp/drupal_debug.txt (dd).
 *
 * @param string $message
 *   Message to print.
 * @param string $label_suffix
 *   Append this to label.
 */
function _deployer_update_debug($message, $label_suffix = '') {
  static $debug = NULL;
  static $label = NULL;

  if (!is_bool($debug)) {
    // Detect debug mode.
    // Stuff is printed with 'dd()'.
    $debug = variable_get('deployer_update_debug', FALSE);
    if ($debug && !function_exists('dd')) {
      $debug = FALSE;
    }
  }

  if ($debug) {

    // Create label.
    if (!is_string($label)) {
      $label = substr(md5(microtime()), rand(0, 26), 4);
    }

    // Append suffix to label.
    $print_label = $label;
    if (!empty($label_suffix)) {
      $print_label = $label . '  -  ' . $label_suffix;
    }

    dd($message, $print_label);
  }
}

/**
 * Set message for project.
 *
 * @param string|array $message
 *   Message.
 *   If array of strings is used, each element is assumed to be a new line.
 * @param int $type
 *   Type of message:
 *     1: common
 *     2: failed
 *     3: ok
 *
 * @param int|NULL $nid
 *   Project nid.
 *
 * @return bool
 *   TRUE: ok, FALSE on failure.
 */
function _deployer_update_set_message($message, $type = DEPLOYER_UPDATE_MSG_TYPE_COMMON, $nid = NULL) {
  // Nid not given? Get static.
  if (is_null($nid)) {
    $nid = _deployer_update_statics('project_id');
  }

  if ($nid) {

    // Transform array of strings to 1 string.
    if (is_array($message)) {
      $message = implode(PHP_EOL, $message);
    }

    // Store in DB.
    $obj = db_insert('deployer_update_msg')
      ->fields(array(
        'nid' => $nid,
        'type' => $type,
        'timestamp' => time(),
        'message' => $message,
      ))
      ->execute();

    return $obj ? TRUE : FALSE;
  }

  return FALSE;
}

/**
 * Get / set statics.
 *
 * @param string $label
 *   Label for saved static.
 *
 * @param null $value
 *   Value to store.
 *
 * @return bool|null
 *   FALSE or set value.
 */
function _deployer_update_statics($label, $value = NULL) {
  static $statics = array();

  if (isset($value)) {
    $statics[$label] = $value;
    return $value;
  }

  return isset($statics[$label]) ? $statics[$label] : FALSE;
}

/**
 * Create / return new static folder.
 *
 * @param string $base
 *   Base path. Usually starts with 'private://'.
 * @param string $folder
 *   Folder name.
 *
 * @return bool|string
 *   FALSE on failure
 *   Real path on success.
 */
function deployer_update_get_storage($base = 'private://deployer_update', $folder = '') {
  // Resolve.
  $real_base = drupal_realpath($base);
  if (!$real_base) {
    return FALSE;
  }

  // Compose path.
  if (empty($folder)) {
    $folder = uniqid();
  }

  // Create folder.
  $path = $real_base . '/' . $folder;
  if (is_dir($path) || drupal_mkdir($path, $mode = NULL, $recursive = TRUE)) {
    return $path;
  }

  return FALSE;
}

/**
 * Code borrowed from drush: pm_parse_version().
 *
 * Unfortunately needed to copy the code, because including
 * drush would be quite a nasty thing, since drush is a standalone app.
 */
function deployer_update_pm_parse_version($version, $core_version) {
  $core_parts = deployer_update_pm_parse_version_decompound($version);

  // If no major version, we have no version at all. Pick a default.
  $drupal_version_default = $core_version;
  if ($core_parts['major'] == '') {
    $core_parts['major'] = ($drupal_version_default) ? $drupal_version_default : drush_get_option('default-major', 7);
  }

  // If something as 7.x-1.0-beta1, the project specific version is
  // in $version['extra'] and we need to parse it.
  if (strpbrk($core_parts['extra'], '.-')) {
    $nocore_parts = deployer_update_pm_parse_version_decompound($core_parts['extra']);
    $nocore_parts['offset'] = $core_parts['offset'];
    $project_version = deployer_update_pm_parse_version_compound($nocore_parts);
    $version_parts = array(
      'version' => $core_parts['major'] . '.x-' . $project_version,
      'drupal_version' => $core_parts['major'] . '.x',
      'project_version' => $project_version,
      'version_major' => $nocore_parts['major'],
      'version_minor' => $core_parts['minor'],
      'version_patch' => ($nocore_parts['patch'] == 'x') ? '' : $nocore_parts['patch'],
      'version_extra' => ($nocore_parts['patch'] == 'x') ? 'dev' : $nocore_parts['extra'],
      'version_offset' => $core_parts['offset'],
    );
  }
  // At this point we have half a version and must decide
  // if this is a drupal major or a project.
  else {
    // If working on a bootstrapped site, core_parts has the project version.
    if ($drupal_version_default) {
      $project_version = deployer_update_pm_parse_version_decompound($core_parts);
      $version = ($project_version) ? $drupal_version_default . '.x-' . $project_version : '';
      $version_parts = array(
        'version' => $version,
        'drupal_version' => $drupal_version_default . '.x',
        'project_version' => $project_version,
        'version_major' => $core_parts['major'],
        'version_minor' => $core_parts['minor'],
        'version_patch' => ($core_parts['patch'] == 'x') ? '' : $core_parts['patch'],
        'version_extra' => ($core_parts['patch'] == 'x') ? 'dev' : $core_parts['extra'],
        'version_offset' => $core_parts['offset'],
      );
    }
    // Not working on a bootstrapped site, core_parts is core version.
    else {
      $version_parts = array(
        'version' => '',
        'drupal_version' => $core_parts['major'] . '.x',
        'project_version' => '',
        'version_major' => '',
        'version_minor' => '',
        'version_patch' => '',
        'version_extra' => '',
        'version_offset' => '',
      );
    }
  }
  return $version_parts;
}

/**
 * Code borrowed from drush: pm_parse_version_decompound().
 */
function deployer_update_pm_parse_version_decompound($version) {
  $pattern = '/^(\d+)(?:.(\d+))?(?:\.(x|\d+))?(?:-([a-z0-9\.-]*))?(?:\+(\d+)-dev)?$/';

  $matches = array();
  preg_match($pattern, $version, $matches);

  $parts = array(
    'major' => '',
    'minor' => '',
    'patch' => '',
    'extra' => '',
    'offset' => '',
  );
  if (isset($matches[1])) {
    $parts['major'] = $matches[1];
    if (isset($matches[2])) {
      if (isset($matches[3]) && $matches[3] != '') {
        $parts['minor'] = $matches[2];
        $parts['patch'] = $matches[3];
      }
      else {
        $parts['patch'] = $matches[2];
      }
    }
    if (!empty($matches[4])) {
      $parts['extra'] = $matches[4];
    }
    if (!empty($matches[5])) {
      $parts['offset'] = $matches[5];
    }
  }

  return $parts;
}

/**
 * Code borrowed from drush: pm_parse_version_compound().
 */
function deployer_update_pm_parse_version_compound($parts) {
  $project_version = '';
  if ($parts['patch'] != '') {
    $project_version = $parts['major'];
    if ($parts['minor'] != '') {
      $project_version = $project_version . '.' . $parts['minor'];
    }
    if ($parts['patch'] == 'x') {
      $project_version = $project_version . '.x-dev';
    }
    else {
      $project_version = $project_version . '.' . $parts['patch'];
      if ($parts['extra'] != '') {
        $project_version = $project_version . '-' . $parts['extra'];
      }
    }
    if ($parts['offset'] != '') {
      $project_version = $project_version . '+' . $parts['offset'] . '-dev';
    }
  }

  return $project_version;
}

/**
 * Check if module is locked for this project.
 *
 * @param object $project
 *   Node object containing locks field.
 * @param string $module
 *   Module name to check for.
 * @param string $field
 *   Name of field that stores locks in JSON format.
 *
 * @return bool
 *   TRUE: module is locked.
 *   False otherwise.
 */
function _deployer_update_module_locked($project, $module, $field = 'field_locked_modules') {
  // Save locks in static to improve performance a bit.
  $projects_locks = &drupal_static(__FUNCTION__, array());
  if (!isset($projects_locks[$project->nid])) {
    $locks_field = field_get_items('node', $project, $field);
    if (isset($locks_field[0]['value'])
      && !empty($locks_field[0]['value'])
      && $locks_array = json_decode($locks_field[0]['value'], $assoc = TRUE)) {
      $projects_locks[$project->nid] = $locks_array;
    }
    else {
      // Set empty array to prevent parsing node field every call
      // if no locks are set.
      $projects_locks[$project->nid] = array();
    }
  }

  return isset($projects_locks[$project->nid][$module]['locked']) ? $projects_locks[$project->nid][$module]['locked'] : FALSE;
}

/**
 * Flush message queue and send report.
 */
function deployer_update_process_messages_queue() {

  $all_messages = db_select('deployer_update_msg', 'm')
    ->fields('m')
    ->orderBy('timestamp', 'ASC')
    ->execute()
    ->fetchAllAssoc('mid');

  $project_messages = array();
  if (!empty($all_messages)) {

    // Remove all messages.
    db_delete('deployer_update_msg')
      ->execute();

    foreach ($all_messages as $message) {
      $project_messages[$message->nid][$message->type][] = $message;
    }
  }

  $report_ok = '';
  $report_failed = '';
  foreach ($project_messages as $nid => $messages) {

    // Only send message if
    // - fail or OK messages are set (so no report for only common messages)
    // - if messages are related to a real project node.
    if ((isset($messages[DEPLOYER_UPDATE_MSG_TYPE_OK]) || isset($messages[DEPLOYER_UPDATE_MSG_TYPE_FAILED]))
      && $project = node_load($nid)) {
      // Title of project.
      // Set title for message.
      $site_name = '';
      $client = field_get_items('node', $project, 'field_client');
      $client = node_load($client[0]['target_id']);
      if ($client) {
        $site_name = $client->title . ' // ';
      }
      $site_name .= $project->title;

      // Header.
      $report = '############################################################' . PHP_EOL;
      $report .= '##  ' . $site_name . PHP_EOL;

      // Set link to version control.
      $code_project = field_get_items('node', $project, 'field_code_project');
      if (isset($code_project[0]['code_project'])) {
        if ($link = deployer_repo_link($code_project[0]['code_project'], 'url')) {
          $report .= '##  ' . $link . PHP_EOL;
        }
      }
      $report .= '############################################################' . PHP_EOL;

      // Common messages.
      if (isset($messages[DEPLOYER_UPDATE_MSG_TYPE_COMMON])) {
        $report .= t('Common messages:') . PHP_EOL;
        foreach ($messages[DEPLOYER_UPDATE_MSG_TYPE_COMMON] as $msg) {
          $report .= _deployer_update_message_format($msg);
        }
        $report .= PHP_EOL;
      }

      // Failed messages.
      if (isset($messages[DEPLOYER_UPDATE_MSG_TYPE_FAILED])) {
        $report .= t('Failed:') . PHP_EOL;
        foreach ($messages[DEPLOYER_UPDATE_MSG_TYPE_FAILED] as $msg) {
          $report .= _deployer_update_message_format($msg);
        }
        $report .= PHP_EOL;
      }

      // OK messages.
      if (isset($messages[DEPLOYER_UPDATE_MSG_TYPE_OK])) {
        $report .= t('OK:') . PHP_EOL;
        foreach ($messages[DEPLOYER_UPDATE_MSG_TYPE_OK] as $msg) {
          $report .= _deployer_update_message_format($msg);
        }
        $report .= PHP_EOL;
      }

      // Extra whitespace.
      $report .= PHP_EOL . PHP_EOL;

      // Group reports based on failure.
      if (isset($messages[DEPLOYER_UPDATE_MSG_TYPE_FAILED])) {
        $report_failed .= $report;
      }
      else {
        $report_ok .= $report;
      }
    }
  }
  $report = $report_failed . $report_ok;

  // Go go go!
  if (!empty($report)) {
    $params['report'] = $report;
    $to = variable_get('deployer_notification_email', '');
    $from_addr = variable_get('site_mail', '');
    $from = 'DFC AutoUpdates <' . $from_addr . '>';

    if (!empty($to) && !empty($from_addr)) {
      drupal_mail('deployer_update', 'update_report', $to, language_default(), $params, $from, $send = TRUE);
    }
  }
}

/**
 * Format message.
 *
 * Multilines are formatted in a readable way.
 *
 * @param object $message
 *   Standard object containing properties
 *     - timestamp
 *     - message.
 *
 * @return string
 *   Message.
 */
function _deployer_update_message_format($message) {
  $lines = explode(PHP_EOL, $message->message);

  $out = '  :: ' . date('d-m-Y H:i', $message->timestamp) . ' ' . $lines[0] . PHP_EOL;
  unset($lines[0]);
  if (!empty($lines)) {
    foreach ($lines as $line) {
      $out .= '  :: ' . $line . PHP_EOL;
    }
  }

  return $out;
}

/**
 * Lock module for auto-updates on a project.
 *
 * @param int $project_nid
 *   Project node id.
 * @param string $module
 *   String of module to lock.
 * @param string $reason
 *   Reason why.
 * @param string $field
 *   Field name to act on.
 */
function deployer_update_lock_module($project_nid, $module, $reason, $field = 'field_locked_modules') {

  // Get node from DB, and set lock into field.
  // Reset node_load, because this functiong might be used
  // in parallel requests (drush workers).
  if ($project = node_load($project_nid, NULL, $reset = TRUE)) {
    $w = entity_metadata_wrapper('node', $project);
    $lock_field_data = $w->$field->value();
    if ($lock_field_data) {
      $locks = json_decode($lock_field_data, TRUE);
    }
    else {
      $locks = array();
    }

    // Set values.
    $locks[$module]['locked'] = 1;
    $locks[$module]['reason'] = $reason;
    $json = json_encode($locks);

    // Save.
    $w->$field->set($json);
    $w->save();
  }
}

/**
 * Place lockfile for given string / key.
 *
 * @param string $string
 *   String / key to compose lockfile for.
 *
 * @param int $max_age
 *   Max age. If lock already exists, replace it if it's
 *   older than time()-$max_age.
 *
 * @return bool
 *   TRUE on success, FALSE on failure.
 */
function deployer_update_set_lock($string, $max_age = 3600) {
  $base = 'private://deployer_update';
  $folder = 'locks';
  $path = deployer_update_get_storage($base, $folder);

  $lockfile = $path . '/' . md5($string);
  if (!is_file($lockfile)) {
    $result = file_put_contents($lockfile, $string);
    return ($result) ? TRUE : FALSE;
  }
  else {
    $mtime = filemtime($lockfile);
    if ($mtime && (time() - $mtime) > $max_age) {
      unlink($lockfile);
      $result = file_put_contents($lockfile, $string);
      return ($result) ? TRUE : FALSE;
    }
  }
  return FALSE;
}

/**
 * Check if fresh lockfile already exists.
 *
 * @param string $string
 *   String / key to compose lockfile for.
 * @param int $max_age
 *   Max age. If lock already exists, replace it if it's
 *   older than time()-$max_age.
 *
 * @return bool
 *   TRUE if fresh lockfile exists, FALSE if it doesn't.
 */
function deployer_update_locked($string, $max_age = 3600) {
  $base = 'private://deployer_update';
  $folder = 'locks';
  $path = deployer_update_get_storage($base, $folder);

  $lockfile = $path . '/' . md5($string);
  if (is_file($lockfile)) {
    $mtime = filemtime($lockfile);
    if ($mtime && (time() - $mtime) > $max_age) {
      return FALSE;
    }
    return TRUE;
  }
  return FALSE;
}

/**
 * Remove lockfile.
 *
 * @param string $string
 *   String / key to compose lockfile for.
 *
 * @return bool
 *   Always TRUE.
 */
function deployer_update_release_lock($string) {
  $base = 'private://deployer_update';
  $folder = 'locks';
  $path = deployer_update_get_storage($base, $folder);

  $lockfile = $path . '/' . md5($string);
  if (is_file($lockfile)) {
    unlink($lockfile);
  }
  return TRUE;
}
