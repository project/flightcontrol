<?php
/**
 * @file
 * This file provides functions to update a project with
 * latest security updates.
 *
 * _deployer_update_set_message() is used to set messages, which
 * might be e-mailed later on.
 */

/**
 * Perform updates for project.
 *
 * @param int $project_id
 *   Project NID.
 */
function deployer_update_project($project_id) {
  module_load_include('inc', 'deployer_update', 'includes/deployer_update.helpers');

  _deployer_update_debug(t('Starting updates check for project ID @id', array('@id' => $project_id)), __FUNCTION__);

  // Set project id for messages.
  _deployer_update_statics('project_id', $project_id);

  $project = node_load($project_id);

  if ($project) {
    // Create wrapper for entity, for later use.
    $project_wrapper = entity_metadata_wrapper('node', $project);

    // Get git clone link.
    // Basic auth must be included.
    $git_clone_link = deployer_repo_git_link($project_id);
    if (!$git_clone_link) {
      // No clone link available.
      return FALSE;
    }

    $project_name = $project->title;

    // Create repo object.
    $repo = new DeployerGitAPI($git_clone_link);

    // Check if local repo is already available.
    // Otherwise, clone into new one.
    _deployer_update_debug(t('Cloning / fetching repo now'), __FUNCTION__);
    $repo_path = field_get_items('node', $project, 'field_repo_path');
    $path = FALSE;

    $drupal_main_path = 'private://deployer_update';
    $main_path = drupal_realpath($drupal_main_path);

    // Whether or not using existing or fresh repo.
    $using_existing_repo = FALSE;
    if (isset($repo_path[0]['value'])) {
      $path = $main_path . $repo_path[0]['value'];
    }
    if (($path) &&
      !empty($path) &&
      is_dir($path)
    ) {
      if (DeployerGitAPI::isValidRepo($path)) {
        // Fetch.
        _deployer_update_debug(t('Valid repo found in @path', array('@path' => $path)), __FUNCTION__);
        $repo->workingFolder($path);
        $repo_ok = $repo->gitFetch();
        $using_existing_repo = TRUE;
      }
      else {
        // Clean up.
        _deployer_update_debug(t('Cleaning up broken repo in @path', array('@path' => $path)), __FUNCTION__);
        exec('rm -rf ' . $path);
        $repo->workingFolder($path);
        $repo_ok = $repo->gitClone();
      }
    }
    else {
      if (!$main_path) {
        _deployer_update_debug(t('Unable to use private filesystem!'), __FUNCTION__);
        watchdog('deployer_update', 'Unable to use private filesystem!', array(), WATCHDOG_ALERT, $link = NULL);
        return FALSE;
      }
      $repo_relative_path = '/repos/' . uniqid();
      $repo->workingFolder($main_path . $repo_relative_path);
      $repo_ok = $repo->gitClone();
    }

    // Proceed on valid repo.
    if ($repo_ok) {
      _deployer_update_debug(t('Repo ok!'), __FUNCTION__);

      // Do not throw away when done.
      $repo->isPersistent(TRUE);

      // Register all branches.
      $branches = $repo->getAllBranches();

      // If auto-update (config) branch exists, check out.
      // If not, leave it with default branch.
      // First look for branch config in project node,
      // else use global config.
      if (isset($project->field_update_to_branch)) {
        $preferred_base_branch = $project_wrapper->field_update_to_branch->value();
        if (!$preferred_base_branch) {
          $preferred_base_branch = variable_get('deployer_update_working_branch', 'develop');
        }
      }
      if (isset($preferred_base_branch) && isset($branches[$preferred_base_branch])) {
        $repo->gitCheckout($branches[$preferred_base_branch]);
      }

      // Keep track of starting commit.
      $starting_commit = $repo->getCurrentCommitHash();

      // Track origin branch to track changes.
      $branch = $repo->getCurrentBranch();

      _deployer_update_debug(t('Using branch @branch', array('@branch' => $branch->branch)), __FUNCTION__);
      _deployer_update_set_message(t('Using branch !branch', array('!branch' => $branch->branch)), DEPLOYER_UPDATE_MSG_TYPE_COMMON);

      // Reset branch to remote branch if it's a non-fresh clone.
      if ($using_existing_repo) {
        $repo->gitResetBranchToOrigin($branch);
      }

      // Update drupal core if needed.
      if ($project_wrapper->field_auto_update_core->value()) {
        $last_commit = $repo->getCurrentCommitHash();
        $update_drupal_core = variable_get('deployer_update_drupalcore', FALSE);
        if ($update_drupal_core) {
          $core_update = deployer_update_check_n_update_core($repo);
          if ($core_update) {
            $repo->gitCommit(t('Auto-update: core updated to @version', array('@version' => $core_update)));
          }
          else {
            $repo->gitResetHard($last_commit);
          }
        }
      }

      // Module updates.
      if ($project_wrapper->field_auto_updates->value()) {
        $last_commit = $repo->getCurrentCommitHash();
        $updates = deployer_update_project_modules_check_and_perform_main($repo, $project);
        if (empty($updates)) {
          // No updates performed.
          // Reset repo.
          $repo->gitResetHard($last_commit);
        }
      }

      // Push or remove new branch.
      // Only if changes occurred.
      $current_commit = $repo->getCurrentCommitHash();
      if ($current_commit != $starting_commit) {
        $diff = $repo->gitDiffRefs($starting_commit, $current_commit);
        if ($diff && !empty($diff)) {
          _deployer_update_debug(t('Pushing repo!'), __FUNCTION__);
          $result = $repo->gitPush();
          if (!$result) {
            $repo->gitResetBranchToOrigin($branch);
            _deployer_update_set_message(t('FAILED TO PUSH COMMITS!'), DEPLOYER_UPDATE_MSG_TYPE_COMMON);
            _deployer_update_debug(t('Unable to push repo for project @name (@pid) !!', array('@name' => $project_name, '@pid' => $project_id)), __FUNCTION__);
            watchdog('deployer_update', 'Unable to push repo for project @name (@pid) !!<br/>You should fix this since updates will be done over and over again, but not pushed.', array('@name' => $project_name, '@pid' => $project_id), WATCHDOG_WARNING, l($project_name, 'node/' . $project_id));
          }
        }
        else {
          _deployer_update_debug(t('Nothing changed, resetting repo!'), __FUNCTION__);
          $repo->gitResetBranchToOrigin($branch);
        }
      }

      // Save repo-folder to project if new path was created.
      if (isset($drupal_main_path) && isset($repo_relative_path)) {
        $project->field_repo_path['und'][0]['value'] = $repo_relative_path;
        field_attach_update('node', $project);
      }
    }
    else {
      // Clean up repo folder, since it's invalid.
      _deployer_update_debug(t('Unable to clone or fetch repo in @path !', array('@path' => $repo->getRepoFolder())), __FUNCTION__);
      watchdog('deployer_update', 'Repo invalid in @path !', array('@path' => $repo->getRepoFolder()), WATCHDOG_ALERT, $link = NULL);
      $repo->isPersistent(FALSE);
    }
  }
  else {
    _deployer_update_debug(t('Unable to load project for id @id', array('@id' => $project_id)), __FUNCTION__);
  }
}

/**
 * Check and perform core update.
 *
 * @param DeployerGitAPI $repo
 *   Git repo containing valid drupal project.
 *
 * @return bool|string
 *   New version string on success, FALSE on failure.
 */
function deployer_update_check_n_update_core(DeployerGitAPI $repo) {
  $failed = FALSE;

  // Track messages.
  $messages = array();

  // Load helpers.
  module_load_include('inc', 'deployer_update', 'includes/deployer_update.helpers');

  // Get current core version.
  $path = $repo->getRepoFolder();
  $exec = new DeployerExec('drush --root="' . $path . '" ev "echo VERSION" 2> /dev/null', $path);

  if ($exec->ok) {
    $current_version = trim($exec->output[0]);
    $version_parts = explode('.', $current_version);
    $major_version = $version_parts[0];

    // Find newer security release, if any.
    $update = FALSE;
    $releases = variable_get('deployer_project_releases', array());
    if (isset($releases['drupal'][$major_version . '.x']['security_versions'][$major_version]['version'])) {
      $latest = $releases['drupal'][$major_version . '.x']['security_versions'][$major_version]['version'];
      if ($latest > $current_version) {
        _deployer_update_debug(t('Drupal core version upgrade => @version', array('@version' => $latest)), __FUNCTION__);
        $update = $latest;
      }
    }

    if ($update) {
      // Set message.
      $messages[] = t('Update core !current to !version', array('!current' => $current_version, '!version' => $update));

      // Download old core version to see if .htaccess and robots.txt were
      // changed.
      $base = 'private://deployer_update';
      $folder = 'core_versions';
      $local_core_versions_path = deployer_update_get_storage($base, $folder);
      if (!$local_core_versions_path) {
        _deployer_update_debug(t('Unable to create path @path', array('@path' => $base . '/' . $folder)), __FUNCTION__);
        watchdog('deployer_update', 'Unable to create path @path', array('@path' => $base . '/' . $folder), WATCHDOG_ALERT, $link = NULL);
        $failed = TRUE;
      }

      // Download current drupal version.
      $current_version_path = $local_core_versions_path . '/drupal-' . $current_version;
      if (!$failed && !deployer_update_locked($current_version_path) && (!is_dir($current_version_path) || !is_file($current_version_path . '/index.php'))) {
        // Lock our download.
        deployer_update_set_lock($current_version_path);

        _deployer_update_debug(t('Downloading core version @version to @path', array('@version' => $current_version, '@path' => $current_version_path)));
        $exec = new DeployerExec('drush --destination="' . $local_core_versions_path . '" dl drupal-' . $current_version, $path);
        if (!$exec->ok) {
          // Not ok? Remove version folder.
          $failed = TRUE;
          _deployer_update_debug(' ' . t('FAILED'), __FUNCTION__);
          $exec = new DeployerExec('rm -rf ' . $current_version_path, $path);
        }

        // Release lock.
        deployer_update_release_lock($current_version_path);
      }
      elseif (!$failed && deployer_update_locked($current_version_path)) {
        // Wait for lock to disappear
        // (probably other worker already downloading).
        $max_cycles = 100;
        $cycles = 0;
        while (deployer_update_locked($current_version_path) && $cycles < $max_cycles) {
          sleep(3);
          $cycles++;
        }
        if (deployer_update_locked($current_version_path)) {
          $failed = TRUE;
        }
      }

      // Download new version.
      $new_version_path = $local_core_versions_path . '/drupal-' . $update;
      if (!$failed && !deployer_update_locked($new_version_path) && (!is_dir($new_version_path) || !is_file($new_version_path . '/index.php'))) {
        // Lock our download.
        deployer_update_set_lock($new_version_path);

        // Download current drupal version.
        _deployer_update_debug(t('Downloading core version @version', array('@version' => $update)), __FUNCTION__);
        $exec = new DeployerExec('drush --destination="' . $local_core_versions_path . '" dl drupal-' . $update, $path);
        if (!$exec->ok) {
          // Not ok? Remove version folder.
          $failed = TRUE;
          _deployer_update_debug(' ' . t('FAILED'), __FUNCTION__);
          $exec = new DeployerExec('rm -rf ' . $new_version_path, $path);
        }

        // Release lock.
        deployer_update_release_lock($new_version_path);
      }
      elseif (!$failed && deployer_update_locked($new_version_path)) {
        // Wait for lock to disappear
        // (probably other worker already downloading).
        $max_cycles = 100;
        $cycles = 0;
        while (deployer_update_locked($new_version_path) && $cycles < $max_cycles) {
          sleep(3);
          $cycles++;
        }
        if (deployer_update_locked($new_version_path)) {
          $failed = TRUE;
        }
      }

      // Check for changes in repo wrt original files in drupal core.
      $files_to_patch = array(
        '.htaccess',
        'robots.txt',
        '.gitignore',
      );
      $patches = array();
      foreach ($files_to_patch as $file) {
        // Diff original version against version in repo.
        $patchfile = sys_get_temp_dir() . '/' . uniqid();
        $exec = new DeployerExec('diff -urN ' . $current_version_path . '/' . $file . ' ' . $file . ' | tee ' . $patchfile, $path);
        if ($exec->ok && !empty($exec->output)) {
          // Difference detected!
          $patches[$file] = $patchfile;

          // Debug.
          _deployer_update_debug(t('Patch file diff for @file:', array('@file' => $file)));
          $submsg = '';
          foreach ($exec->output as $line) {
            $submsg .= '  :: ' . $line . PHP_EOL;
          }
          _deployer_update_debug($submsg);
        }
      }

      // Update.
      if (!$failed) {
        // Sync new drupal core to repo.
        $exec = new DeployerExec("rsync -v -r --delete " . $new_version_path . "/ " . $path . " --exclude '/sites/' --exclude '/*patch*' --exclude '/*PATCH*' --exclude '/.git/'", $path);
        !$exec->ok ? $failed = TRUE : NULL;
        _deployer_update_debug(t('Core update status: @state', array('@state' => var_export($exec->ok, TRUE))));

        // Copy default.settings.php from new to current repo sites folder.
        $exec = new DeployerExec('find ./sites -mindepth 1 -maxdepth 1 -type d -not -name all -exec cp ' . $new_version_path . '/sites/default/default.settings.php "{}"/ \;', $path);
      }

      // Patch files from $files_to_patch.
      if (!$failed && !empty($patches)) {
        foreach ($patches as $file => $patch) {
          $exec = new DeployerExec('git apply ' . $patch, $path);
          if ($exec->ok) {
            $messages[] = DEPOYER_UPDATE_MSG_NESTED_PREFIX . t('File @file patched', array('@file' => $file));
          }
          else {
            $messages['common'][] = DEPOYER_UPDATE_MSG_NESTED_PREFIX . t('Failed patching file @file. Please check because it was not the original.', array('@file' => $file));
          }

          // Cleanup.
          unlink($patch);
        }
      }

      // Debug.
      count($messages) ? _deployer_update_debug('Messages: ' . PHP_EOL . var_export($messages, TRUE), __FUNCTION__) : NULL;

      if (!$failed) {
        $messages[] = DEPOYER_UPDATE_MSG_NESTED_PREFIX . t('core update ok');
        _deployer_update_set_message($messages, DEPLOYER_UPDATE_MSG_TYPE_OK);
        return $update;
      }

      _deployer_update_set_message($messages, DEPLOYER_UPDATE_MSG_TYPE_FAILED);
    }

  }

  return FALSE;
}

/**
 * Check for updates in project-path.
 *
 * @param DeployerGitAPI $repo
 *   Repo containing valid drupal project.
 * @param object $drupal_project_node
 *   Node object. Drupal project.
 *
 * @return array|bool
 *   Array: updates. Empty if no updates are performed.
 *   FALSE on failure.
 */
function deployer_update_project_modules_check_and_perform_main(DeployerGitAPI $repo, $drupal_project_node) {
  // Get name.
  $drupal_project_name = $drupal_project_node->title;

  _deployer_update_debug(t('Checking updates for project @project', array('@project' => $drupal_project_name)), __FUNCTION__);

  // Track spotted modules (reset here) to see
  // if project has duplicate modules.
  // Duplicate modules get locked.
  module_load_include('inc', 'deployer_update', 'includes/deployer_update.helpers');
  _deployer_update_statics('spotted_modules', array());

  // Get whitelist.
  module_load_include('inc', 'deployer_update', 'includes/deployer_update.fetch');
  $whitelist_projects = deployer_update_get_whitelist();
  $updates = array();

  // Get repo path.
  $path = $repo->getRepoFolder();

  // Proceed if module-list is filled, or need to update core.
  if (!empty($whitelist_projects) && is_dir($path) && is_readable($path)) {
    _deployer_update_debug(t('Performing updates in @path', array('@path' => $path)), __FUNCTION__);

    // Get all info files in repo.
    $info_files = drupal_system_listing('|^.*\.info$|i', $path, $key = 'uri', $min_depth = 1);
    _deployer_update_debug(t('Found @num info-files', array('@num' => count($info_files))), __FUNCTION__);
    foreach ($info_files as $uri => $info_file) {
      $module = $info_file->name;

      // Only match if is module in sites/*/modules,
      // and not locked for this project.
      if (preg_match('|sites/[^/]*/modules/.*\.info$|i', $uri)) {
        if (in_array($module, $whitelist_projects) && !_deployer_update_module_locked($drupal_project_node, $module)) {
          _deployer_update_debug(t('Check updates for project @project', array('@project' => $module)), __FUNCTION__);
          $info = drupal_parse_info_file($info_file->uri);
          deployer_update_check_module_update($module, $info, $updates, $drupal_project_node);
        }
      }
    }

    // Perform updates.
    if (!empty($updates)) {
      if ($result = deployer_update_perform_module_updates($updates, $drupal_project_name, $repo)) {
        return $result;
      }
    }
    else {
      _deployer_update_debug(t('No updates', array()), __FUNCTION__);
    }
  }
  else {
    _deployer_update_debug(t('Unable to change path to @path', array('@path' => $path)), __FUNCTION__);
  }

  return array();
}

/**
 * Check for update: info-data against latest-releases list.
 *
 * @param string $module
 *   Module name. (basename of info file).
 * @param array $info
 *   Parsed info file.
 * @param array $updates
 *   Array updates get registered in.
 * @param object $drupal_project_node
 *   Node object. Drupal project.
 */
function deployer_update_check_module_update($module, $info, &$updates, $drupal_project_node) {
  // This is used to track every module,
  // so we know if there are any duplicates.
  // If duplicate occurs, we lock it.
  $spotted_modules = _deployer_update_statics('spotted_modules');
  if (!$spotted_modules) {
    $spotted_modules = array();
  }

  $releases = variable_get('deployer_project_releases', array());
  $project = ($info['project'] ? $info['project'] : FALSE);
  $core_version = ($info['core'] ? $info['core'] : FALSE);
  $date_stamp = ($info['datestamp'] ? $info['datestamp'] : FALSE);

  // Complete info-data ?
  if ($project && $core_version && $date_stamp) {

    // Only add module to updates if it has no duplicates.
    if (!isset($spotted_modules[$module])) {
      $spotted_modules[$module] = TRUE;
      _deployer_update_statics('spotted_modules', $spotted_modules);

      // Parse version string.
      $version_info = deployer_update_pm_parse_version($info['version'], $core_version);

      // Only register update if module has 'version_major' and has
      // security version.
      if (isset($releases[$module][$core_version]['security_versions'][$version_info['version_major']])) {
        $latest_security_version = $releases[$module][$core_version]['security_versions'][$version_info['version_major']];
        _deployer_update_debug(t('@module latest version: @version', array('@module' => $module, '@version' => $latest_security_version['version'])), __FUNCTION__);
        _deployer_update_debug('     ' . t('current version: @version', array('@version' => $info['version'])), __FUNCTION__);
        if ($latest_security_version['datestamp'] > $date_stamp) {
          // We have a security update (newer version).
          $updates[$module] = array(
            'new_version' => $latest_security_version['version'],
            'current_version' => $info['version'],
          );
          _deployer_update_debug(t('++   Need to update @module to @version', array('@module' => $module, '@version' => $latest_security_version['version'])), __FUNCTION__);
        }
      }
    }
    else {
      // Duplicate module detected. Will cause trouble, need to lock it.
      unset($updates[$module]);
      deployer_update_lock_module($drupal_project_node->nid, $module, t('Duplicate module detected!'));
      _deployer_update_set_message(t('Module @mod locked, because it has duplicates in webroot!', array('@mod' => $module)), DEPLOYER_UPDATE_MSG_TYPE_FAILED);
      _deployer_update_debug(t('Module @mod locked, because it has duplicates in webroot!', array('@mod' => $module)), __FUNCTION__);
    }
  }
}

/**
 * Perform updates.
 *
 * @param array $updates
 *   Array containing updates.
 * @param string $drupal_project_name
 *   Project name.
 * @param DeployerGitAPI $repo
 *   Repo containing valid drupal project.
 *
 * @return bool|array
 *   Updates array on success, FALSE on failure.
 */
function deployer_update_perform_module_updates($updates, $drupal_project_name, DeployerGitAPI $repo) {

  // Get working path.
  $path = $repo->getRepoFolder();

  // Loop through all updates.
  foreach ($updates as $module => $update) {

    $version = $update['new_version'];
    $current_version = $update['current_version'];

    $last_commit = $repo->getCurrentCommitHash();

    $package = $module . '-' . $version;
    $command = 'drush dl ' . $package . ' -y';
    $exec = new DeployerExec($command, $path);
    if (!$exec->ok) {
      // Reset to last commit.
      $repo->gitResetHard($last_commit);

      // Notify.
      watchdog('deployer_update', 'Unable to update @module (@command)' . PHP_EOL . 'output:' . PHP_EOL . '!output', array(
        '@module' => $module,
        '@command' => $command,
        '!output' => implode(PHP_EOL, $exec->output)), WATCHDOG_ALERT, $link = NULL);
      _deployer_update_debug(t('Update failed: @c', array('@c' => $command)), __FUNCTION__);
      _deployer_update_set_message(t('!module => !version', array('!module' => $module, '!version' => $version)), DEPLOYER_UPDATE_MSG_TYPE_FAILED);
    }
    else {
      _deployer_update_debug(t('Update OK: @c', array('@c' => $command)), __FUNCTION__);
      $repo->gitCommit(t('Auto-update: module @module updated to version @version', array('@module' => $module, '@version' => $version)));
      _deployer_update_set_message(t('!module !current => !version', array(
        '!current' => $current_version,
        '!module' => $module,
        '!version' => $version)), DEPLOYER_UPDATE_MSG_TYPE_OK);
    }

    // Get commits from remote in case someone just pushed something.
    // This is to avoid conflicts etc.
    $repo->gitPullRebase();
  }

  return $updates;
}
