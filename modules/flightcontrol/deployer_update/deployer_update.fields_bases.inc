<?php
/**
 * @file
 * Field definitions / instances.
 */

/**
 * Implements hook_deployer_ctypes_fields().
 */
function deployer_update_deployer_ctypes_fields() {
  $fields = array();

  $fields[] = array(
    'field_name'  => 'field_auto_update_core',
    'type'        => 'list_boolean',
    'cardinality' => 1,
    'settings' => array(
      'allowed_values' => array(
        0 => '',
        1 => '',
      ),
      'allowed_values_function' => '',
    ),
    'module' => 'list',
  );

  $fields[] = array(
    'field_name'  => 'field_auto_updates',
    'type'        => 'list_boolean',
    'cardinality' => 1,
    'settings' => array(
      'allowed_values' => array(
        0 => '',
        1 => '',
      ),
      'allowed_values_function' => '',
    ),
    'module' => 'list',
  );

  $fields[] = array(
    'field_name'  => 'field_locked_modules',
    'type'        => 'text_long',
    'cardinality' => 1,
    'settings' => array(),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'module' => 'text',
  );

  $fields[] = array(
    'field_name'  => 'field_update_to_branch',
    'type'        => 'text',
    'cardinality' => 1,
    'settings' => array(
      'max_length' => 255,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'module' => 'text',
  );

  return $fields;
}
