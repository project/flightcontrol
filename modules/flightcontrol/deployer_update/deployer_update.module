<?php
/**
 * @file
 * Module file.
 */

// Used to structure related messages for report.
const DEPOYER_UPDATE_MSG_NESTED_PREFIX = ' > ';
// Stop queue worker after this amount of time.
const DEPLOYER_UPDATE_QUEUE_RUN_TIME = 600;

// Message types.
const DEPLOYER_UPDATE_MSG_TYPE_COMMON = 1;
const DEPLOYER_UPDATE_MSG_TYPE_FAILED = 2;
const DEPLOYER_UPDATE_MSG_TYPE_OK = 3;

include_once 'deployer_update.fields_bases.inc';
include_once 'deployer_update.fields_instances.inc';

/**
 * Implements hook_menu().
 */
function deployer_update_menu() {
  $items['admin/deployer/update'] = array(
    'title' => 'Update settings',
    'description' => 'Auto-update configuration.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('deployer_update_admin'),
    'access arguments' => array('configure global auto-updates'),
    'file' => 'deployer_update.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 *
 * Main permissions for this module.
 */
function deployer_update_permission() {
  return array(
    'configure global auto-updates' => array(
      'title' => t('Configure global auto-update settings'),
      'description' => '',
    ),
  );
}

/**
 * Implements hook_cron().
 */
function deployer_update_cron() {
  if (variable_get('deployer_update_switch', FALSE)) {
    // Fetch release info.
    $last_run = variable_get('deployer_update_release_info_last_run', 0);
    if ($last_run <= time() - 1200) {
      module_load_include('inc', 'deployer_update', 'includes/deployer_update.fetch');
      deployer_update_fetch_whitelist_releases();
      variable_set('deployer_update_release_info_last_run', time());
    }

    // Update projects.
    module_load_include('inc', 'deployer_update', 'includes/deployer_update.scheduler');
    deployer_update_cron_process();

    // Send report.
    $last_run = variable_get('deployer_update_report_last_run', 0);
    if ($last_run <= time() - variable_get('deployer_update_report_interval', 14400)) {
      module_load_include('inc', 'deployer_update', 'includes/deployer_update.helpers');
      deployer_update_process_messages_queue();
      variable_set('deployer_update_report_last_run', time());
    }
  }
}

/**
 * Implements hook_field_widget_info().
 */
function deployer_update_field_widget_info() {
  // Add field widget deployer_update_module_locks.
  // This transforms json data for text field into different form fields.
  return array(
    'deployer_update_module_locks' => array(
      'label' => t('Module locks'),
      'field types' => array('text_long'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function deployer_update_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // If deployer_update_module_locks widget is used,
  // use json data to enable / disable module locks.
  if ($instance['widget']['type'] == 'deployer_update_module_locks') {

    module_load_include('inc', 'deployer_update', 'includes/deployer_update.fetch');

    $element['locks'] = array(
      '#type' => 'fieldset',
      '#title' => t('Locked modules'),
      '#description' => t('Locked modules do not get auto-updated'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => array(
        'class' => array(
          'deployer-update-locked-modules',
        ),
      ),
    );

    $whitelist = deployer_update_get_whitelist();
    $saved_data = array();
    if (isset($items[0]['value'])) {
      $saved_data = json_decode($items[0]['value'], $assoc = TRUE);
    }

    if (!empty($whitelist)) {
      foreach ($whitelist as $module) {

        $element['locks'][$module] = array(
          '#type' => 'fieldset',
          '#title' => $module,
          '#attributes' => array(
            'class' => array(
              'module',
            ),
          ),
        );

        $element['locks'][$module]['locked'] = array(
          '#type' => 'checkbox',
          '#title' => t('Lock'),
          '#default_value' => isset($saved_data[$module]['locked']) ? $saved_data[$module]['locked'] : FALSE,
        );

        $element['locks'][$module]['reason'] = array(
          '#type' => 'textfield',
          '#title' => t('Reason'),
          '#default_value' => isset($saved_data[$module]['reason']) ? $saved_data[$module]['reason'] : '',
        );
      }
    }
  }

  return $element;
}

/**
 * Implements hook_form_alter().
 */
function deployer_update_form_alter(&$form, $form_state, $form_id) {
  // Add custom submit handler.
  $form['#submit'][] = "deployer_update_form_submit_handler";
}

/**
 * Submit handler.
 *
 * Process json from field with widget deployer_update_module_locks into json.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function deployer_update_form_submit_handler($form, &$form_state) {
  if (isset($form['#entity_type']) && $form['#entity_type'] == 'node' && $form['#bundle'] == 'project') {
    if (isset($form_state['field']) && !empty($form_state['field'])) {
      foreach ($form_state['field'] as $field => $field_data) {
        if ($field_data[LANGUAGE_NONE]['instance']['widget']['type'] == 'deployer_update_module_locks') {
          if (isset($form_state['input'][$field][LANGUAGE_NONE][0]['locks']) &&
            !empty($form_state['input'][$field][LANGUAGE_NONE][0]['locks'])) {
            $form_state['values'][$field][LANGUAGE_NONE][0]['value'] = json_encode($form_state['input'][$field][LANGUAGE_NONE][0]['locks']);
            unset($form_state['input'][$field][LANGUAGE_NONE][0]['locks']);
          }
        }
      }
    }
  }
}

/**
 * Implements hook_mail().
 */
function deployer_update_mail($key, &$message, $params) {
  switch ($key) {
    case 'update_report':
      $message['subject'] = t('DFC: Update report');
      $message['body'][] = $params['report'];
      break;
  }
}
