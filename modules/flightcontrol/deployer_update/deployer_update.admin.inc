<?php

/**
 * @file
 * The administrative function for the update module.
 */

/**
 * Admin form used to congure the deployer update module.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array
 *
 * @return array
 *   A renderable array of the form.
 */
function deployer_update_admin($form, &$form_state) {

  $form['deployer_update_switch'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto updates'),
    '#description' => t('Turn this off to disable the auto-update feature.'),
    '#default_value' => variable_get('deployer_update_switch', FALSE),
    '#required' => FALSE,
  );

  $form['deployer_update_working_branch'] = array(
    '#type' => 'textfield',
    '#title' => t('Default branch to perform updates on'),
    '#description' => t('<strong>Overridden by project specific configuration</strong><br/>This should be "develop" in most cases.<br/>Commits are made on this branch.<br/><strong>If this branch does not exist, default branch (usually master) is used!</strong>'),
    '#default_value' => variable_get('deployer_update_working_branch', 'develop'),
    '#required' => TRUE,
  );

  module_load_include('inc', 'deployer_update', 'includes/deployer_update.fetch');
  $form['deployer_update_whitelist'] = array(
    '#type' => 'textarea',
    '#title' => t('Module-whitelist for auto-updates'),
    '#default_value' => variable_get('deployer_update_whitelist', DEPLOYER_UPDATE_MODULE_WHITELIST),
    '#required' => TRUE,
    '#description' => t('Enter a module project name on each line.'),
  );

  $form['deployer_update_drupalcore'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include drupal core in auto-update'),
    '#description' => t('If this is checked, Drupal core will also be updated.'),
    '#default_value' => variable_get('deployer_update_drupalcore', FALSE),
    '#required' => FALSE,
  );

  $form['deployer_update_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimal interval to perform code updates on'),
    '#default_value' => variable_get('deployer_update_interval', 7200),
    '#required' => TRUE,
  );

  $form['deployer_update_report_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Interval to send reports'),
    '#default_value' => variable_get('deployer_update_report_interval', 14400),
    '#required' => TRUE,
  );

  $form['deployer_update_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto-update debug mode'),
    '#description' => t('Debug is printed in /tmp/drupal_debug.txt for auto-updates.<br/>Note that devel-module must be turned on!'),
    '#default_value' => variable_get('deployer_update_debug', FALSE),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}
