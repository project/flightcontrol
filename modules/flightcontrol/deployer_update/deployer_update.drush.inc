<?php
/**
 * @file
 * Drush commands:
 *  - deployer_update_process_queue [queue index]
 *    Process queue to perform updates on several projects.
 */

/**
 * Implements hook_drush_command().
 */
function deployer_update_drush_command() {
  $items = array();

  // Health check per environment.
  $items['deployer_update_process_queue'] = array(
    'description' => 'Process auto-updates queue',
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUPAL_FULL',
    'callback' => 'deployer_update_process_queue_drush_wrapper',
    'arguments' => array(
      'index' => 'The index of the queue, 0 to ' . DEPLOYER_UPDATE_PARALLEL_PROC,
    ),
  );

  return $items;
}

/**
 * Wrapper for deployer_update_queue_invoke().
 *
 * @param int $index
 *   Queue index.
 */
function deployer_update_process_queue_drush_wrapper($index) {
  module_load_include('inc', 'deployer_update', 'includes/deployer_update.scheduler');
  deployer_update_process_queue($index);
}
