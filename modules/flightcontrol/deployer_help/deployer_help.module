<?php
/**
 * @file
 * The module file.
 */

/**
 * Implements hook_block_info().
 */
function deployer_help_block_info() {
  $blocks = array();
  $blocks['deployer_help'] = array(
    'info' => t('Drupal Flight Control help / quick start.'),
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function deployer_help_block_view($delta = '') {
  $block = array();
  // The $delta parameter tells us which block is being requested.
  switch ($delta) {
    case 'deployer_help':
      global $user;
      if ($user->uid == 1 || in_array('administrator', $user->roles)) {
        $block['subject'] = t('Quick start info');
        $block['content'] = _deployer_help_content($delta);
      }
      break;
  }
  return $block;
}

/**
 * Returns help info.
 *
 * @param string $delta
 *   Which block is this ?
 *
 * @return array
 *   Render array.
 */
function _deployer_help_content($delta) {
  $markup = '
  ' . t('Most important thing to do: Check quick start documentation on our') . ' ' . l(t('project page'), 'https://www.drupal.org/project/flightcontrol') . '!<br/>
  ' . t('Done that? Configure flight control settings:') . '<br/>
  <ul>
    <li>' . l(t('General settings'), 'admin/deployer/admin')  . '</li>
    <li> ' . l(t('Version control connectors'), 'admin/deployer/versioncontrol') . '<ul>
    <li>' . l(t('GitHub connector (optional)'), 'admin/deployer/versioncontrol/github')  . '</li>
    <li>' . l(t('Stash connector (optional)'), 'admin/deployer/versioncontrol/stash')  . '</li>
    <li>' . l(t('GitLab connector (optional)'), 'admin/deployer/versioncontrol/gitlab')  . '</li>
    <li>' . l(t('BitBucket connector (optional)'), 'admin/deployer/versioncontrol/bitbucket')  . '</li>
    <li>' . l(t('Git API, only used for auto-updates'), 'admin/deployer/versioncontrol/git_api')  . '</li>
    </ul></li>
    <li>' . l(t('Auto-updates (optional)'), 'admin/deployer/update')  . '</li>
  </ul>
<p/>
' . t('Next, add a new') . ' ' . l(t('client'), 'node/add/client') . '<br/>
' . t('Then, open your newly added client-page, and click on the "Add project" link in the left sidebar.') . '</br>
' . t('After adding a project, open the new project-page, and click on the "Add environment" link in the left sidebar, and configure connection parameters.') . '</br>
<p/>' . t('Last thing to do, is to open your project page again, and test you connections with click on the "Backup" link.') . '</br>
<p/><h3>' . t('Troubleshooting') . '</h3>
<ul>
<li>' . t('Check the') .  ' ' . l(t('Drupal logs'), 'admin/reports/dblog') . '</li>
<li>' . t('Check the') .  ' ' . l(t('Drupal status report'), 'admin/reports/status') . '</li>
<li>' . t('Check the README in the "deployer" module, inside this Drupal webroot') . '<br/>
You might have forgotten to install OS / PHP packages.
</li>
<li>' . t('Check the') .  ' ' . l(t('DFC project page'), 'https://www.drupal.org/project/flightcontrol') . '</li>
</ul>

  ';

  // Add link to disable the block.
  $markup .= '<p>' . l(t('Disable this block >>'), 'admin/structure/block/manage/deployer_help/deployer_help/configure') . '</p>';

  $result = array(
    '#markup' => $markup,
  );
  return $result;
}
