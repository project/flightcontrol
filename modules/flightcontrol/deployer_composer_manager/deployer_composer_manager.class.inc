<?php

/**
 * @file
 * Composer wrapper.
 */

/**
 * Class DeployerComposerApi.
 *
 * Wrapper for composer handling.
 */
class DeployerComposerApi {

  /**
   * Constructor for this class.
   */
  public function __construct() {
    $this->bin = drupal_realpath(DEPLOYER_COMPOSER_MANAGER_PATH);
    $this->home = drupal_realpath(DEPLOYER_COMPOSER_MANAGER_COMPOSER_HOME);
    $this->workingFolder = FALSE;
    $this->error = array();
    $this->lastOutput = '';
    $this->lastStatus = '';
    $this->lastCommand = '';

    if (!is_dir($this->home)) {
      mkdir($this->home, 0700);
    }
    return $this;
  }

  /**
   * Set working folder.
   *
   * @param string $path
   *   Path to work in.
   *
   * @return bool
   *   TRUE: ok. FALSE: not possible.
   */
  public function setWorkingFolder($path) {
    if (is_dir($path)) {
      $this->workingFolder = $path;
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get working folder.
   *
   * @return string|bool
   *   Path, or FALSE on failure.
   */
  public function getWorkingFolder() {
    return $this->workingFolder;
  }

  /**
   * Execute composer command(s).
   *
   * @param string|array $command
   *   Provide a single composer argument, or an array of arguments.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function exec($command) {
    if (is_array($command)) {
      $params = array();
      foreach ($command as $param) {
        $params[] = escapeshellarg($param);
      }
      $command = implode($command);
    }
    else {
      $command = escapeshellarg($command);
    }

    return $this->composerCommand($command);
  }

  /**
   * Composer command wrapper.
   *
   * Mainly used to make sure current working folder is valid.
   *
   * @param string $cmd
   *   Command string, like 'clone https://my.git/repo'.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  private function composerCommand($cmd) {
    // Check binary.
    if (!file_exists($this->bin) || !is_executable($this->bin)) {
      $this->error[] = t('Composer binary not found or not executable');
      return FALSE;
    }

    // Note: 'exit 133' is used to know if folder is accessible or not.
    $bin_flags = '--no-interaction --verbose';
    $command = $this->lastCommand = 'export COMPOSER_HOME="' . $this->home . '"; cd "' . $this->workingFolder . '" || exit 133 && ' . $this->bin . ' ' . $bin_flags . ' ' . $cmd . ' 2>&1';
    $output = array();
    $status = 1;
    exec($command, $output, $status);
    $this->lastOutput = $output;
    $this->lastStatus = $status;

    if ($status === 0) {
      return TRUE;
    }
    else {
      $this->error[] = t('Unable to execute command: !c', array('!c' => $this->lastCommand));
      $this->error[] = t('Exit status: @s', array('@s' => $this->lastStatus));
      $this->error = array_merge($this->error, $this->lastOutput);
      return FALSE;
    }
  }

}
