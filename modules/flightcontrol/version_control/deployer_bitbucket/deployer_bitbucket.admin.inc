<?php

/**
 * @file
 * The administrative function for the deployer_bitbucket module.
 */

/**
 * Admin form used to configure the deployer bitbucket module.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array
 *
 * @return array
 *   A renderable array of the form.
 */
function deployer_bitbucket_admin($form, &$form_state) {
  $form['deployer_bitbucket_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate bitbucket connector'),
    '#default_value' => variable_get('deployer_bitbucket_active', FALSE),
  );
  $form['deployer_bitbucket_basic_auth_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Bitbucket username'),
    '#default_value' => variable_get('deployer_bitbucket_basic_auth_username', ''),
    '#required' => FALSE,
  );
  $form['deployer_bitbucket_basic_auth_password'] = array(
    '#type' => 'password',
    '#title' => t('Bitbucket password'),
    '#required' => FALSE,
    '#deployer_password' => TRUE,
  );
  return system_settings_form($form);
}
