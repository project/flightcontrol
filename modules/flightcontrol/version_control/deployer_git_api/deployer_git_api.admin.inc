<?php

/**
 * @file
 * The administrative function for the update module.
 */

/**
 * Admin form used to configure the deployer git API module.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array
 *
 * @return array
 *   A renderable array of the form.
 */
function deployer_git_api_admin($form, &$form_state) {

  $form['deployer_git_api_git_message_author'] = array(
    '#type' => 'textfield',
    '#title' => t('Author to report with git messages.'),
    '#default_value' => variable_get('deployer_git_api_git_message_author', ''),
    '#required' => TRUE,
    '#description' => t('Make sure this is in format <i>@example</i>.', array('@example' => 'Name <email>')),
  );

  $form['deployer_git_api_ignore_invalid_ssl'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ignore invalid SSL certificates'),
    '#default_value' => variable_get('deployer_git_api_ignore_invalid_ssl', FALSE),
    '#description' => '<p>' . t('Use with caution. <br/>Makes main-in-the-middle attacks peace a cake!!') .
    '<p/><p>Also note that this might not help in case of certificates with wrong common names, due to a bug in libcurl3-gnutls.<br/>' .
    'See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=741293</p>',
  );

  $form['deployer_git_api_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Git controller debug mode'),
    '#description' => t('Debug is printed in /tmp/drupal_debug.txt for each git command.<br/>Note that devel-module must be turned on!<br/><strong>This might print passwords as well!</strong>'),
    '#default_value' => variable_get('deployer_git_api_debug', FALSE),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}
