<?php
/**
 * @file
 * Provides git classes
 *  - DeployerGitAPI (repository)
 *  - DeployerGitAPIBranch (branch)
 */

class DeployerGitAPI {

  /**
   * Create new repository-clone.
   *
   * @param string $repo
   *   Repository clone link, starting with http(s):// .
   */
  public function __construct($repo) {

    // Won't work if repo is not valid.
    if (strpos($repo, 'http') !== 0) {
      return FALSE;
    }

    $this->repo = $repo;
    $this->valid_repo = FALSE;
    $this->lean_clone = FALSE;
    $this->local_branches = array();
    $this->current_branch = FALSE;
    $this->remote_branches = array();
    $this->persistent = FALSE;

    // Set debug mode.
    if (variable_get('deployer_git_api_debug', FALSE)) {
      $this->debug(TRUE);
    }
    else {
      $this->debug(FALSE);
    }
    return TRUE;
  }

  /**
   * Destructor.
   *
   * Clean up.
   */
  public function __destruct() {
    // Remove temp folder.
    if (isset($this->working_folder) && !$this->isPersistent()) {
      exec('rm -rf ' . $this->working_folder);
    }
  }

  /**
   * Get or set debug state.
   *
   * Devel must be turned on.
   *
   * @param bool $switch
   *   TRUE: debug on
   *   FALSE: debug off
   *   NULL (empty): revturn current state.
   *
   * @return bool
   *   TRUE: debug on
   *   FALSE: debug off
   */
  public function debug($switch = NULL) {
    // Check for 'dd' once.
    static $devel_on;
    if (!is_bool($devel_on)) {
      if (function_exists('dd')) {
        $devel_on = TRUE;
      }
      else {
        $devel_on = FALSE;
      }
    }

    // Always false if devel is off.
    if (!$devel_on) {
      return FALSE;
    }

    // Get current state.
    if ($switch === NULL) {
      return $this->debug;
    }

    // Set state.
    else {
      $this->debug = $switch;
      return $switch;
    }
  }

  /**
   * Get / set working base path.
   *
   * @param string $path
   *   New working base path.
   *   If NULL is given, get current base folder.
   *
   * @return string
   *   Base folder path.
   */
  public function workingBaseFolder($path = NULL) {
    if (is_string($path)) {
      $this->working_base_path = $path;
    }
    return $this->working_base_path;
  }

  /**
   * Set/get persistence mode.
   *
   * Default: off.
   * If not persistent, clone is removed on shutdown of request.
   *
   * @param bool $switch
   *   TRUE: Do not cleanup.
   *   FALSE: Cleanup afterwards (default).
   *   NULL: Get current value.
   *
   * @return bool
   *   Current persistence-state.
   */
  public function isPersistent($switch = NULL) {
    if ($switch !== NULL) {
      $this->persistent = $switch;
    }
    return $this->persistent;
  }

  /**
   * Make all clones lean (only last revision).
   *
   * @param bool $switch
   *   TRUE: on
   *   FALSE: off
   */
  public function setLeanClone($switch) {
    $this->lean_clone = $switch;
  }

  /**
   * Check and/or create temp folder to store repo in.
   *
   * @param string $path
   *   NULL: get current value.
   *   String: Set working folder.
   *
   * @return bool|string
   *   FALSE on failure, path on success.
   */
  public function workingFolder($path = NULL) {
    // Since we mix op syscalls and php functons like is_dir(),
    // we need to clear php stat cache.
    clearstatcache();

    // Set working folder + base wrt path.
    if (!empty($path)) {
      $this->workingBaseFolder(basename($path));
      $this->working_folder = $path;

      if (!is_dir($path)) {
        return drupal_mkdir($this->working_folder, NULL, $recursive = TRUE);
      }

      return $this->working_folder;
    }

    // Fix base.
    if (!$this->workingBaseFolder()) {
      $this->workingBaseFolder('private://deployer_update');
    }

    // Set working folder.
    if (!isset($this->working_folder)) {
      $this->working_folder = $this->workingBaseFolder() . '/' . uniqid();

      if (!is_dir($this->working_folder)) {
        return drupal_mkdir($this->working_folder, NULL, $recursive = TRUE);
      }
    }

    // Recreate if needed.
    if (!is_dir($this->working_folder)) {
      return drupal_mkdir($this->working_folder, NULL, $recursive = TRUE);
    }

    return $this->working_folder;
  }

  /**
   * Get folder where repo is stored.
   *
   * @return string|bool
   *   FALSE: no valid folder set.
   */
  public function getRepoFolder() {
    if ($this->workingFolder()) {
      return $this->working_folder;
    }
    return FALSE;
  }

  /**
   * Set working branch.
   *
   * Comes in use if you'd like to specify a branch for lean cloning etc.
   *
   * @param DeployerGitAPIBranch $branch
   *   Branch object.
   */
  public function setBranch(DeployerGitAPIBranch $branch) {
    // Store branch object in this object.
    if (!isset($this->local_branches[$branch->branch])) {
      $this->local_branches[$branch->branch] = $branch;
    }

    // Set current branch.
    $this->current_branch = $branch;
  }

  /**
   * Check if path contains valid git repo.
   *
   * @param string $path
   *   Path to repository.
   *
   * @return bool
   *   TRUE on valid git repo, FALSE on failure.
   */
  static public function isValidRepo($path) {
    // Ignore SSL ?
    static $ignore_invalid_ssl;
    if (!is_bool($ignore_invalid_ssl)) {
      $ignore_invalid_ssl = variable_get('deployer_git_api_ignore_invalid_ssl', FALSE);
    }
    $ignore_invalid_ssl ? $git_cmd_prefix = '-c http.sslVerify=false ' : $git_cmd_prefix = '';

    $command = 'cd "' . $path . '" && git ' . $git_cmd_prefix . 'ls-remote 2>/dev/null';
    $output = array();
    $status = 1;
    exec($command, $output, $status);

    if ($status === 0 && !empty($output)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Fetch all for existing git repository.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function gitFetch() {

    $result = $this->gitCMD('fetch --all');
    if ($result) {
      // Register all branches into $this->remote_branches;
      $this->getAllBranches();

      $this->valid_repo = TRUE;
      $this->default_branch = $this->getCurrentBranch();
    }

    return $result;
  }

  /**
   * Fetch all for existing git repository.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function gitPullRebase() {
    return $this->gitCMD('pull --rebase');
  }

  /**
   * Clone git repository.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function gitClone() {
    $this->workingFolder();

    // Lean cloning ?
    $this->lean_clone ? $git_clone_prefix = '--depth 1 ' : $git_clone_prefix = '';

    if ($this->current_branch) {
      $result = $this->gitCMD('clone ' . $git_clone_prefix . ' -b ' . $this->current_branch->branch . ' ' . $this->repo . ' ./');
    }
    else {
      $result = $this->gitCMD('clone ' . $git_clone_prefix . $this->repo . ' ./');
    }

    if ($result) {
      $this->valid_repo = TRUE;
      $this->default_branch = $this->getCurrentBranch();

      // Register all branches into $this->remote_branches;
      $this->getAllBranches();
    }

    return $result;
  }

  /**
   * Checkout specific branch.
   *
   * Can also be a new branch if DeployerGitAPIBranch::isNew(TRUE) is set.
   *
   * @param DeployerGitAPIBranch $branch
   *   Branch object.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function gitCheckout(DeployerGitAPIBranch $branch) {
    if (!$this->valid_repo) {
      return FALSE;
    }

    // Return TRUE if branch is already checked out.
    if ($this->getCurrentBranch()->branch == $branch->branch) {
      return TRUE;
    }
    // Store branch object in this object.
    if (!isset($this->local_branches[$branch->branch])) {
      $this->local_branches[$branch->branch] = $branch;
    }

    // Checkout branch.
    if ($branch->isNew()) {
      $result = $this->gitCMD('checkout -b ' . $branch->branch);
    }
    else {
      $result = $this->gitCMD('checkout ' . $branch->branch);
    }

    if ($result) {
      // Set current branch.
      $this->current_branch = $branch;
    }

    return $result;
  }

  /**
   * Commit current state.
   *
   * @param string $message
   *   Commit message.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function gitCommit($message = '') {
    if (!$this->valid_repo) {
      return FALSE;
    }

    // Set author.
    $author = variable_get('deployer_git_api_git_message_author', 'DeployerGitAPI <noreply@noreply.noreply>');

    // Stage all changes, including deletes.
    $result = FALSE;
    if ($this->gitCMD('add -A .')) {

      // Escape $message.
      $message = str_replace("'", '"', $message);
      $result = $this->gitCMD('commit --author="' . $author . '"' . " -a -m'" . $message . "'");
    }

    return $result;
  }

  /**
   * Push current branch.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function gitPush() {
    if (!$this->valid_repo) {
      return FALSE;
    }

    // Checkout branch.
    if ($branch = $this->getCurrentBranch()) {
      if ($branch->isNew()) {
        $result = $this->gitCMD('push -u origin ' . $branch->branch);
      }
      else {
        $result = $this->gitCMD('push');
      }
      return $result;
    }
    return FALSE;
  }

  /**
   * Hard reset repo.
   *
   * Also, git clean is performed to get rid of untracked files / dirs.
   *
   * @param string $hash
   *   If given, reset to this hash or tag.
   *   Possible values are like
   *     df0ddfdddbe74a3c3b4abcf89232b59cd1e33790 (hash), or
   *     v1.2.1 (tag).
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function gitResetHard($hash = '') {
    $suffix = '';
    if (!empty($hash)) {
      $suffix = ' ' . $hash;
    }
    if ($this->gitCMD('reset --hard' . $suffix)) {
      return $this->gitCMD('clean -f -d');
    }
    return FALSE;
  }

  /**
   * Reset branch to origin.
   *
   * @param DeployerGitAPIBranch $branch
   *   Branch to reset TO. Could be develop or master etc.
   *   So your CURRENT branch is reset to the state of remote/$branch !
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function gitResetBranchToOrigin(DeployerGitAPIBranch $branch) {
    if ($branch->isNew()) {
      // Cannot reset, branch is new.
      return FALSE;
    }
    return $this->gitCMD('reset --hard origin/' . $branch->branch);
  }

  /**
   * Get diff between 2 branches.
   *
   * @param DeployerGitAPIBranch $branch1
   *   Branch 1.
   * @param DeployerGitAPIBranch $branch2
   *   Branch 2.
   *
   * @return bool|array
   *   Output array or FALSE on failure.
   */
  public function gitDiffBranches(DeployerGitAPIBranch $branch1, DeployerGitAPIBranch $branch2) {
    return $this->gitDiffRefs($branch1->branch, $branch2->branch);
  }

  /**
   * Get diff between 2 refs (hashes etc).
   *
   * @param string $ref1
   *   Hash 1.
   * @param string $ref2
   *   Hash 2.
   *
   * @return bool|array
   *   Output array or FALSE on failure.
   */
  public function gitDiffRefs($ref1, $ref2) {
    // Use cat to get rid of interactive git interface which generates
    // status codes other than 0.
    if ($this->gitCMD('diff --name-status "' . $ref1 . '".."' . $ref2 . '" 2>/dev/null | cat')) {
      return $this->last_output;
    }
    return FALSE;
  }

  /**
   * Delete local branch.
   *
   * @param DeployerGitAPIBranch $branch
   *   Branch to delete.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function gitRemoveLocalBranch(DeployerGitAPIBranch $branch) {
    if (!$this->valid_repo) {
      return FALSE;
    }

    // Will not remove default branch.
    if ($branch->branch == $this->default_branch->branch) {
      return FALSE;
    }

    // Switch to default.
    $this->gitCheckout($this->default_branch);

    $result = $this->gitCMD('branch -D ' . $branch->branch);
    if (isset($this->local_branches[$branch->branch])) {
      unset($this->local_branches[$branch->branch]);
    }

    return $result;
  }

  /**
   * Git command wrapper.
   *
   * Mainly used to make sure current working folder is valid.
   *
   * @param string $cmd
   *   Command string, like 'clone https://my.git/repo'.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  private function gitCMD($cmd) {
    $this->workingFolder();

    // Ignore SSL ?
    static $ignore_invalid_ssl;
    if (!is_bool($ignore_invalid_ssl)) {
      $ignore_invalid_ssl = variable_get('deployer_git_api_ignore_invalid_ssl', FALSE);
    }
    $ignore_invalid_ssl ? $git_cmd_prefix = '-c http.sslVerify=false ' : $git_cmd_prefix = '';

    // Note: 'exit 133' is used to know if folder is accessible or not.
    $command = $this->last_command = 'cd "' . $this->working_folder . '" || exit 133 && git ' . $git_cmd_prefix . $cmd;
    $output = array();
    $status = 1;
    exec($command, $output, $status);
    $this->last_output = $output;
    $this->last_status = $status;

    // Debug.
    if ($this->debug()) {
      dd('Command: (' . $status . ') ' . $command);

      if ($status == 133) {
        dd('   ::  Cannot enter ' . $this->working_folder);
      }
      else {
        dd('In path ' . $this->working_folder);
        if (!empty($output)) {
          dd('Output:');
          foreach ($output as $line) {
            dd('  :: ' . $line);
          }
        }
      }
    }

    if ($status === 0) {
      return TRUE;
    }
    else {
      $this->error[] = t('Unable to execute command !c', array('!c' => $this->last_command));
      if ($this->last_status == 128) {
        $this->error[] = t('Exit status was 128, you probably need to enable the ignore-invalid-ssl option');
      }
      else {
        $this->error[] = t('Exit status: @s', array('@s' => $this->last_status));
      }
      return FALSE;
    }
  }

  /**
   * Get current working branch.
   *
   * @return DeployerGitAPIBranch|bool
   *   Branch object or FALSE on failure.
   */
  public function getCurrentBranch() {
    if (!$this->valid_repo) {
      return FALSE;
    }

    $result = $this->gitCMD('rev-parse --abbrev-ref HEAD');
    if (!$result) {
      return FALSE;
    }

    // Save branch to object.
    $branch_name = $this->last_output[0];
    if (!isset($this->local_branches[$branch_name])) {
      $this->current_branch = $this->local_branches[$branch_name] = new DeployerGitAPIBranch($branch_name);
    }
    return $this->current_branch;
  }

  /**
   * Get current working commit hash.
   *
   * @return bool|string
   *   String: commit hash
   *   FALSE on failure
   */
  public function getCurrentCommitHash() {
    if (!$this->valid_repo) {
      return FALSE;
    }

    if ($this->gitCMD('rev-parse --verify HEAD')) {
      $hash = trim($this->last_output[0]);
      return $hash;
    }

    return FALSE;
  }

  /**
   * Get list of all remote branches.
   *
   * Only works for normal clones, not for lean clones.
   * Branches are stored in $this->remote_branches.
   *
   * @return array
   *   Array containing all remote branches.
   */
  public function getAllBranches() {
    if (!$this->valid_repo) {
      return FALSE;
    }

    // Get remotes.
    if ($this->gitCMD("for-each-ref --format='%(refname)' refs/remotes/origin/")) {
      $remotes = $this->last_output;
      foreach ($remotes as $remote) {
        $branch_name = str_replace('refs/remotes/origin/', '', $remote);
        if (!isset($this->local_branches[$branch_name])) {
          $this->remote_branches[$branch_name] = new DeployerGitAPIBranch($branch_name);
        }
        else {
          // Mark branch as already remotely existent.
          $this->local_branches[$branch_name]->isNew(FALSE);
        }
      }
    }

    return $this->remote_branches;
  }

}

/**
 * Class DeployerGitAPIBranch
 *
 * Branch object to use for DeployerGitAPI object.
 */
class DeployerGitAPIBranch {

  /**
   * Create branch object.
   *
   * @param string $branch
   *   Branch name.
   */
  public function __construct($branch) {
    $this->branch = $branch;
    $this->new = FALSE;
  }

  /**
   * Set branch to 'new', or ask current state.
   *
   * @param bool $switch
   *   TRUE: set branch to new one.
   *   FALSE: set branch to 'old' one.
   *   NULL: ask current value.
   *
   * @return bool
   *   Whether branch is new or not.
   */
  public function isNew($switch = NULL) {
    if ($switch === NULL) {
      return $this->new;
    }

    $this->new = $switch;
    return $switch;
  }
}
