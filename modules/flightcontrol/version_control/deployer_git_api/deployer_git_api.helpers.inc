<?php
/**
 * @file
 * File providing abstraction / helper functions for some git interaction.
 */

/**
 * Clone git repo from http link and report path on disk.
 *
 * Note that this folder is persistent, so you need to clean it up yourself!
 *
 * @param string $git_clone_link
 *   Cloneable link, including basic auth if needed.
 * @param string $hash
 *   Hash or tag to check out.
 *   Leave empty to just clone the repo.
 *   Possible values are like
 *     df0ddfdddbe74a3c3b4abcf89232b59cd1e33790 (hash), or
 *     v1.2.1 (tag).
 * @param bool $cleanup_git_folder
 *   TRUE: remove .git folder from repo => not a repo anymore.
 *   FALSE: leave repo/git intact.
 *
 * @return bool|string
 *   Path to repo, or FALSE on failure.
 */
function deployer_git_api_clone_git_repo($git_clone_link, $hash = '', $cleanup_git_folder = TRUE) {
  // Create repo object.
  $repo = new DeployerGitAPI($git_clone_link);
  // Make sure folder does not get removed on object destruction.
  $repo->isPersistent(TRUE);
  $repo->workingBaseFolder(file_directory_temp() . '/flightcontrol_deploy_gitclone/' . uniqid());
  if ($repo->gitClone()) {
    if ($repo->gitResetHard($hash)) {
      $path = $repo->getRepoFolder();
      if ($path) {
        if ($cleanup_git_folder) {
          // Get rid of .git folder.
          $command = '[ -d "' . $path . '/.git" ] && rm -rf "' . $path . '/.git"';
          exec($command);
        }
        return $path;
      }
    }
    else {
      $caller = deployer_get_function_caller();
      watchdog('deployer_git_api', 'Hash @hash unknown in repo called from <br/>' . var_export($caller, TRUE), array('@hash' => $hash), WATCHDOG_ALERT, $link = NULL);
    }
  }
  else {
    $caller = deployer_get_function_caller();
    watchdog('deployer_git_api', 'Unable to clone repo called from <br/>' . var_export($caller, TRUE), array(), WATCHDOG_ALERT, $link = NULL);
  }

  return FALSE;
}
