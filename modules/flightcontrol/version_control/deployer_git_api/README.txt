Git API module.

This module provides an abstraction layer to interact with git repositories
that are configured in projects.

Example use:

$id = 2273; // ID of project node.
$link = deployer_repo_git_link($id);
dsm($link);
$repo = new DeployerGitAPI($link);
$repo->isPersistent(TRUE);

$branch = new DeployerGitAPIBranch('master');
$repo->setBranch($branch);
if (!$repo->gitClone()) {
  dsm($repo->error);
}
$drush = _deployer_get_drush();
dsm($drush);
dsm($repo->getCurrentBranch());

module_load_include('inc', 'deployer_update', 'includes/deployer_update.project');
$path = $repo->getRepoFolder();
dsm($path);
deployer_update_project_get_local_versions($path);
