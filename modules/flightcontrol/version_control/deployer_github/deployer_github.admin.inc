<?php

/**
 * @file
 * The administrative function for the deployer_github module.
 */

/**
 * Admin form used to congure the deployer github module.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array
 *
 * @return array
 *   A renderable array of the form.
 */
function deployer_github_admin($form, &$form_state) {
  $form['deployer_github_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate github connector'),
    '#default_value' => variable_get('deployer_github_active', FALSE),
  );

  $form['deployer_github_auth_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Github username'),
    '#default_value' => variable_get('deployer_github_auth_username', ''),
    '#required' => FALSE,
  );
  $form['deployer_github_auth_password'] = array(
    '#type' => 'password',
    '#title' => t('Github password'),
    '#default_value' => variable_get('deployer_github_auth_password', ''),
    '#required' => FALSE,
    '#deployer_password' => TRUE,
  );

  $form['deployer_github_organization'] = array(
    '#type' => 'textfield',
    '#title' => t('Github organizations'),
    '#default_value' => variable_get('deployer_github_organization', ''),
    '#required' => TRUE,
    '#description' => t('Github organizations to fetch repositories for, comma separated (no spaces).'),
  );
  return system_settings_form($form);
}
