<?php
/**
 * @file
 * The module file.
 */

/**
 * Implements hook_menu().
 */
function deployer_github_menu() {
  $items['admin/deployer/versioncontrol/github'] = array(
    'title' => 'Github settings',
    'description' => 'Github related configuration.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('deployer_github_admin'),
    'access arguments' => array('administer versioncontrol connectors'),
    'file' => 'deployer_github.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_deployer_code_repo_option_provider().
 *
 * Function to set option provider for repo-type.
 */
function deployer_github_deployer_code_repo_option_provider() {
  return array(
    'github' => 'deployer_github_repos',
  );
}

/**
 * Function to provide github repositories in array.
 *
 * Use auth-variable which is defined in the github config form.
 *
 * @return ret_array
 *   bool|array('option1', 'option2', etc)
 */
function deployer_github_repos() {

  if (!variable_get('deployer_github_active', FALSE)) {
    return array();
  }

  if (!$orgs = variable_get('deployer_github_organization')) {
    watchdog('deployer_github', 'Var deployer_github_organization not set', array(), WATCHDOG_ALERT, $link = NULL);
    return FALSE;
  }

  $orgs = (explode(',', $orgs));
  if (empty($orgs)) {
    watchdog('deployer_github', 'No organizations configured for github!', array(), WATCHDOG_ALERT, $link = NULL);
    return FALSE;
  }

  // Basic auth.
  $options['headers']['Authorization'] = 'Basic ' . base64_encode(variable_get('deployer_github_auth_username', '') . ':' . variable_get('deployer_github_auth_password', ''));
  $repos_all = array();
  foreach ($orgs as $org) {
    if (!$repos = drupal_http_request('https://api.github.com/orgs/' . $org . '/repos?per_page=10000', $options)) {
      watchdog('deployer_github', 'Git auth not working, or request not valid (Status code ' . $repos->code . ')', array(), WATCHDOG_ALERT, $link = NULL);
      return FALSE;
    }
    switch ($repos->code) {
      case 200:
        watchdog('deployer_github', 'Git response ok 200', array(), WATCHDOG_INFO, $link = NULL);
        break;

      case 403:
        watchdog('deployer_github', 'Git auth wrong', array(), WATCHDOG_ALERT, $link = NULL);
        return FALSE;

      case 404:
        watchdog('deployer_github', 'Git auth wrong, or not-found returned', array(), WATCHDOG_ALERT, $link = NULL);
        return FALSE;

      default:
        watchdog('deployer_github', 'Something wrong with git, status code ' . $repos->code . ' .', array(), WATCHDOG_ALERT, $link = NULL);
        return FALSE;

    }

    $repos_all = array_merge($repos_all, drupal_json_decode($repos->data));
  }

  foreach ($repos_all as $repo) {
    $ret_array[] = $repo['full_name'];
  }

  return $ret_array;
}

/**
 * Implements hook_deployer_tag_option_provider().
 *
 * Function to set option provider for tags.
 */
function deployer_github_deployer_tag_option_provider() {
  return array(
    'github' => 'deployer_github_tags',
  );
}

/**
 * Implements hook_deployer_codebase_provider().
 *
 * Function to set filename provider for tarball.
 */
function deployer_github_deployer_codebase_provider() {
  return array(
    'github' => 'deployer_github_codebase',
  );
}

/**
 * Implements hook_deployer_tag_link_option_provider().
 *
 * Function to set tag-link provider, to let user link
 * directly to a website or something else showing info on the tag.
 */
function deployer_github_deployer_tag_link_option_provider() {
  return array(
    'github' => 'deployer_github_tag_link',
  );
}

/**
 * Implements hook_deployer_repo_link_option_provider().
 *
 * Function to set repo-link provider, to let user link
 * directly to a website or something else showing info on the repo.
 */
function deployer_github_deployer_repo_link_option_provider() {
  return array(
    'github' => 'deployer_github_repo_link',
  );
}

/**
 * Make request to api and return reply data in array (json decoded), or FALSE.
 *
 * @param string $request
 *   Request string (url).
 * @param bool $raw_data
 *   Return data instead of json if set to TRUE.
 *
 * @return bool|object
 *   Reply OR FALSE.
 */
function _deployer_github_request($request, $raw_data = FALSE) {
  // Basic auth.
  $options['headers']['Authorization'] = 'Basic ' . base64_encode(variable_get('deployer_github_auth_username', '') . ':' . variable_get('deployer_github_auth_password', ''));
  if (!$reply = drupal_http_request($request, $options)) {
    watchdog('deployer_github', 'Git auth not working, or request not valid (Status code ' . $reply->code . ')', array(), WATCHDOG_ALERT, $link = NULL);
    return FALSE;
  }
  switch ($reply->code) {
    case 200:
      watchdog('deployer_github', 'Git response ok 200', array(), WATCHDOG_INFO, $link = NULL);
      break;

    case 403:
      watchdog('deployer_github', 'Git auth wrong', array(), WATCHDOG_ALERT, $link = NULL);
      return FALSE;

    case 404:
      watchdog('deployer_github', 'Git auth wrong, or not-found returned', array(), WATCHDOG_ALERT, $link = NULL);
      return FALSE;

    default:
      watchdog('deployer_github', 'Something wrong with git, status code ' . $reply->code . ' .', array(), WATCHDOG_ALERT, $link = NULL);
      return FALSE;

  }
  if ($raw_data) {
    return $reply->data;
  }
  return drupal_json_decode($reply->data);
}

/**
 * Provide tags.
 *
 * Function to provide tags / commits for repository based on
 * OTAP-type (tst -> acc -> prd).
 *
 * @param string $repo
 *   Repository name.
 *
 * @param string $branch
 *   Branch name.
 *
 * @param int $otap_type
 *   OTAP type.
 *
 * @return array
 *   Commits and / or tags.
 */
function deployer_github_tags($repo, $branch, $otap_type) {

  if (!variable_get('deployer_github_active', FALSE)) {
    return array();
  }

  if ($otap_type > 1) {
    $request = 'https://api.github.com/repos/' . $repo . '/tags?per_page=10000';
  }
  else {
    // Get branche data to get branch-sha.
    $request = 'https://api.github.com/repos/' . $repo . '/branches/' . $branch;

    if (!$branch = _deployer_github_request($request)) {
      return FALSE;
    }
    $branch_sha = $branch['commit']['sha'];
    $request = 'https://api.github.com/repos/' . $repo . '/commits?sha=' . $branch_sha . '&per_page=10000';
  }

  if (!$tags = _deployer_github_request($request)) {
    return FALSE;
  }

  $ret_array = array();
  foreach ($tags as $tag) {
    if ($otap_type > 1) {
      $ret_array['tag|' . $tag['name']] = $tag['name'];
    }
    else {
      $ret_array['hash|' . $tag['sha']] = $tag['commit']['author']['date'] . ' - ' . $tag['sha'] . ' - ' . $tag['commit']['author']['email'] . ' - ' . $tag['commit']['message'];
    }
  }

  return $ret_array;
}


/**
 * Function to fetch codebase and provide pathname on disk.
 *
 * @param string $repo
 *   Repository name.
 *
 * @param string $tag
 *   Tag or commit hash.
 *
 * @return string
 *   Real path on disk of folder containing webroot to deploy.
 */
function deployer_github_codebase($repo, $tag) {
  $tag = explode('|', $tag);
  $tag = $tag[1];

  // Generate clone link, and fetch repo.
  $git_clone_link = deployer_github_repo_git_link($repo);
  return deployer_git_api_clone_git_repo($git_clone_link, $tag);
}

/**
 * Function to provide link to github repo.
 *
 * @param string $repo
 *   Repository string as provided by deployer_github_repos().
 *
 * @param string $branch
 *   Branch string.
 *
 * @param string $tag_str
 *   Tag string as provided by deployer_github_tags().
 *
 * @param string $format
 *   Format for link.
 *
 * @return array
 *   Link array.
 */
function deployer_github_tag_link($repo, $branch, $tag_str, $format = 'full') {

  $tag_parts = explode('|', $tag_str);
  $tag_type = $tag_parts[0];
  $tag = $tag_parts[1];

  if ($tag_type == 'tag') {
    $url = 'https://github.com/' . $repo . '/releases/tag/' . $tag;

    if ($format == 'short') {
      $title = $tag;
    }
    else {
      $title = t('Visit on github: %t% on %r%', array('%t%' => 'release ' . $tag, '%r%' => $repo));
    }
  }
  elseif ($tag_type == 'hash') {
    $url = 'https://github.com/' . $repo . '/commit/' . $tag;

    if ($format == 'short') {
      $title = $tag;
    }
    else {
      $title = t('Visit on github: %t% on %r%', array('%t%' => 'commit ' . $tag, '%r%' => $repo));
    }
  }

  if (isset($url) && isset($title)) {
    return array(
      'url' => $url,
      'title' => $title,
    );
  }

  return FALSE;
}

/**
 * Function to provide link to github commit of tag.
 *
 * @param string $repo
 *   Repository string as provided by deployer_github_repos().
 *
 * @param string $format
 *   'full' or 'short'.
 *
 * @return array
 *   Link array.
 */
function deployer_github_repo_link($repo, $format) {
  if ($format == 'full') {
    return t('Visit on github:') . ' ' . l($repo, 'https://github.com/' . $repo);
  }
  elseif ($format == 'url') {
    return 'https://github.com/' . $repo;
  }
  else {
    return l($repo, 'https://github.com/' . $repo);
  }
}

/**
 * Implements hook_deployer_tag_option_provider().
 *
 * Function to set option provider for git clone links.
 */
function deployer_github_deployer_git_repo_link_option_provider() {
  return array(
    'github' => 'deployer_github_repo_git_link',
  );
}

/**
 * Function to provide git clone link.
 *
 * @param string $repo
 *   Repo to provide link for.
 *
 * @return bool|string
 *   False on failure, url on success.
 */
function deployer_github_repo_git_link($repo) {
  $auth = "'" . variable_get('deployer_github_auth_username', '') . "':'" . variable_get('deployer_github_auth_password', '') . "'";
  $link = 'https://' . $auth . '@github.com/' . $repo . '.git';
  return $link;
}
