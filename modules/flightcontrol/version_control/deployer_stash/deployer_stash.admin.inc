<?php

/**
 * @file
 * The administrative function for the deployer_stash module.
 */

/**
 * Admin form used to congure the deployer stash module.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array
 *
 * @return array
 *   A renderable array of the form.
 */
function deployer_stash_admin($form, &$form_state) {
  $form['deployer_stash_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate stash connector'),
    '#default_value' => variable_get('deployer_stash_active', FALSE),
  );

  $form['deployer_stash_auth_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Stash username'),
    '#default_value' => variable_get('deployer_stash_auth_username', ''),
    '#required' => FALSE,
  );
  $form['deployer_stash_auth_password'] = array(
    '#type' => 'password',
    '#title' => t('Stash password'),
    '#default_value' => variable_get('deployer_stash_auth_password', ''),
    '#required' => FALSE,
    '#deployer_password' => TRUE,
  );

  $form['deployer_stash_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Stash api url'),
    '#default_value' => variable_get('deployer_stash_url', ''),
    '#required' => TRUE,
    '#description' => t('Like https://stash.organization.com:8443.'),
  );
  return system_settings_form($form);
}
