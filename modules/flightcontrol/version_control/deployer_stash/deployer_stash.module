<?php
/**
 * @file
 * The module file.
 */

/**
 * Implements hook_menu().
 */
function deployer_stash_menu() {
  $items['admin/deployer/versioncontrol/stash'] = array(
    'title' => 'Stash settings',
    'description' => 'Stash related configuration.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('deployer_stash_admin'),
    'access arguments' => array('administer versioncontrol connectors'),
    'file' => 'deployer_stash.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_deployer_code_repo_option_provider().
 *
 * Function to set option provider for repo-type.
 */
function deployer_stash_deployer_code_repo_option_provider() {
  return array(
    'stash' => 'deployer_stash_repos',
  );
}

/**
 * Function to provide stash repositories in array.
 *
 * @return bool|array
 *   bool | array('option1', 'option2', etc)
 */
function deployer_stash_repos() {

  if (!variable_get('deployer_stash_active', FALSE)) {
    return array();
  }

  if (!$url = variable_get('deployer_stash_url')) {
    watchdog('deployer_stash', 'Var deployer_stash_url not set', array(), WATCHDOG_ALERT, $link = NULL);
    return FALSE;
  }

  // Basic auth.
  $options['headers']['Authorization'] = 'Basic ' . base64_encode(variable_get('deployer_stash_auth_username', '') . ':' . variable_get('deployer_stash_auth_password', ''));
  if (!$repos = drupal_http_request($url . '/rest/api/1.0/repos?limit=10000', $options)) {
    watchdog('deployer_stash', 'Stash auth not working, or request not valid (Status code ' . $repos->code . ')', array(), WATCHDOG_ALERT, $link = NULL);
    return FALSE;
  }
  switch ($repos->code) {
    case 200:
      watchdog('deployer_stash', 'Stash response ok 200', array(), WATCHDOG_INFO, $link = NULL);
      break;

    case 403:
      watchdog('deployer_stash', 'Stash auth wrong', array(), WATCHDOG_ALERT, $link = NULL);
      return FALSE;

    case 404:
      watchdog('deployer_stash', 'Stash auth wrong, or not-found returned', array(), WATCHDOG_ALERT, $link = NULL);
      return FALSE;

    default:
      watchdog('deployer_stash', 'Something wrong with git, status code ' . $repos->code . ' .', array(), WATCHDOG_ALERT, $link = NULL);
      return FALSE;

  }

  $repos = drupal_json_decode($repos->data);
  $ret_array = array();

  foreach ($repos['values'] as $repo) {
    $ret_array[] = $repo['project']['key'] . '/' . $repo['slug'];
  }

  return $ret_array;
}

/**
 * Implements hook_deployer_tag_option_provider().
 *
 * Function to set option provider for tags for this type.
 */
function deployer_stash_deployer_tag_option_provider() {
  return array(
    'stash' => 'deployer_stash_tags',
  );
}

/**
 * Implements hook_deployer_codebase_provider().
 *
 * Function to set stash provider for codebase path from version control.
 */
function deployer_stash_deployer_codebase_provider() {
  return array(
    'stash' => 'deployer_stash_codebase',
  );
}

/**
 * Implements hook_deployer_tag_link_option_provider().
 *
 * Function to set tag-link provider, to let user link
 * directly to a website or something else showing info on the tag.
 */
function deployer_stash_deployer_tag_link_option_provider() {
  return array(
    'stash' => 'deployer_stash_tag_link',
  );
}

/**
 * Implements hook_deployer_repo_link_option_provider().
 *
 * Function to set repo-link provider, to let user link
 * directly to a website or something else showing info on the repo.
 */
function deployer_stash_deployer_repo_link_option_provider() {
  return array(
    'stash' => 'deployer_stash_repo_link',
  );
}

/**
 * Provide tags.
 *
 * Function to provide tags / commits for repository based on
 * otap-type (tst -> acc -> prd).
 *
 * @param string $repo
 *   Repository name.
 *
 * @param string $branch
 *   Branch string.
 *
 * @param int $otap_type
 *   otap type.
 *
 * @return array
 *   Commits and / or tags.
 */
function deployer_stash_tags($repo, $branch, $otap_type) {
  if (!variable_get('deployer_stash_active', FALSE)) {
    return array();
  }

  if (!$url = variable_get('deployer_stash_url')) {
    watchdog('deployer_stash', 'Var deployer_stash_url not set', array(), WATCHDOG_ALERT, $link = NULL);
    return FALSE;
  }

  $repo = explode('/', $repo);
  $project = $repo[0];
  $repo = $repo[1];

  if ($otap_type > 1) {
    $request = $url . '/rest/api/1.0/projects/' . $project . '/repos/' . $repo . '/tags?limit=10000';
  }
  else {
    $request = $url . '/rest/api/1.0/projects/' . $project . '/repos/' . $repo . '/commits?until=' . $branch . '&limit=10000';
  }
  $tags = stash_request($request);

  $ret_array = array();
  foreach ($tags['values'] as $tag) {
    $id = rawurlencode($tag['id']);

    if ($otap_type > 1) {
      $display_id = rawurlencode($tag['displayId']);
      $display_id_show = check_plain($tag['displayId']);
      $ret_array['tag|' . $id . '|' . $display_id] = $display_id_show;
    }
    else {
      $msg = check_plain($tag['message']);
      $ret_array['hash|' . $id] = date('Y-m-d H:i', $tag['authorTimestamp'] / 1000) . ' - ' . $id . ' - ' . $tag['author']['emailAddress'] . ' - ' . $msg;
    }
  }
  return $ret_array;
}


/**
 * Function to provide codebase path on disk.
 *
 * @param string $repo
 *   Repository name.
 *
 * @param string $tag
 *   Tag or commit hash.
 *
 * @return string
 *   Real path on disk of folder containing webroot to deploy.
 */
function deployer_stash_codebase($repo, $tag) {
  if (!$url = variable_get('deployer_stash_url')) {
    watchdog('deployer_stash', 'Var deployer_stash_url not set', array(), WATCHDOG_ALERT, $link = NULL);
    return FALSE;
  }

  $tag = explode('|', $tag);
  $tag = rawurldecode($tag[1]);

  // Generate clone link, and fetch repo.
  $git_clone_link = deployer_stash_repo_git_link($repo);
  return deployer_git_api_clone_git_repo($git_clone_link, $tag);
}

/**
 * Function to provide link to stash commit of tag.
 *
 * @param string $repo
 *   Repository string as provided by deployer_stash_repos().
 *
 * @param string $branch
 *   Branch string.
 *
 * @param string $tag_str
 *   Tag string as provided by deployer_stash_tags().
 *
 * @param string $format
 *   Format for link.
 *
 * @return array
 *   Link array.
 */
function deployer_stash_tag_link($repo, $branch, $tag_str, $format = 'full') {

  $repo = explode('/', $repo);
  $project = $repo[0];
  $repo = $repo[1];

  $tag_parts = explode('|', $tag_str);
  $tag_type = $tag_parts[0];
  $tag = $tag_parts[1];

  if (!$url = variable_get('deployer_stash_url')) {
    watchdog('deployer_stash', 'Var deployer_stash_url not set', array(), WATCHDOG_ALERT, $link = NULL);
    return FALSE;
  }

  if ($tag_type == 'tag') {
    $tag_id = $tag_parts[2];
    $url = $url . '/projects/' . $project . '/repos/' . $repo . '/browse?at=' . $tag;

    if ($format == 'short') {
      $title = $tag_id;
    }
    else {
      $title = t('Visit on stash: %t% on %r%', array('%t%' => 'release ' . $tag_id, '%r%' => $repo));
    }
  }
  elseif ($tag_type == 'hash') {
    $url = $url . '/projects/' . $project . '/repos/' . $repo . '/commits/' . $tag;

    if ($format == 'short') {
      $title = $tag;
    }
    else {
      $title = t('Visit on stash: %t% on %r%', array('%t%' => 'commit ' . $tag, '%r%' => $repo));
    }
  }

  if (isset($url) && isset($title)) {
    return array(
      'url' => $url,
      'title' => $title,
    );
  }

  return FALSE;
}

/**
 * Function to provide link to stash repo.
 *
 * @param string $repo
 *   Repository string as provided by deployer_stash_repos().
 *
 * @param string $format
 *   'full' or 'short'.
 *
 * @return array
 *   Link array.
 */
function deployer_stash_repo_link($repo, $format) {

  $repo = explode('/', $repo);
  $project = $repo[0];
  $repo = $repo[1];

  if (!$url = variable_get('deployer_stash_url')) {
    watchdog('deployer_stash', 'Var deployer_stash_url not set', array(), WATCHDOG_ALERT);
    return FALSE;
  }

  if ($format == 'full') {
    return t('Visit on stash:') . ' ' . l($repo, $url . '/projects/' . $project . '/repos/' . $repo);
  }
  elseif ($format == 'url') {
    return $url . '/projects/' . $project . '/repos/' . $repo;
  }
  else {
    return l($repo, $url . '/projects/' . $project . '/repos/' . $repo);
  }
}

/**
 * Implements hook_deployer_git_repo_link_option_provider().
 *
 * Function to set option provider for git clone links.
 */
function deployer_stash_deployer_git_repo_link_option_provider() {
  return array(
    'stash' => 'deployer_stash_repo_git_link',
  );
}

/**
 * Function to provide git clone link.
 *
 * @param string $repo
 *   Repo to provide link for.
 *
 * @return bool|string
 *   False on failure, url on success.
 */
function deployer_stash_repo_git_link($repo) {
  if (!$url = variable_get('deployer_stash_url')) {
    watchdog('deployer_stash', 'Var deployer_stash_url not set', array(), WATCHDOG_ALERT, $link = NULL);
    return FALSE;
  }

  // Include basic auth, compose clone url.
  $auth = "'" . variable_get('deployer_stash_auth_username', '') . "':'" . variable_get('deployer_stash_auth_password', '') . "'";
  $git_clone_link = preg_replace('|(https?://)|', '\1' . $auth . '@', $url) . '/scm/' . $repo . '.git';
  return $git_clone_link;
}

/**
 * Perform stash API request.
 *
 * @param string $url
 *   URL including protocol etc.
 * @param bool $raw_data
 *   TRUE: return data object,
 *   FALSE: return json.
 *
 * @return bool|mixed
 *   data object of json.
 */
function stash_request($url, $raw_data = FALSE) {
  // Basic auth.
  $options['headers']['Authorization'] = 'Basic ' . base64_encode(variable_get('deployer_stash_auth_username', '') . ':' . variable_get('deployer_stash_auth_password', ''));
  if (!$response = drupal_http_request($url, $options)) {
    watchdog('deployer_stash', 'Stash auth not working, or request not valid (Status code ' . $response->code . ')', array(), WATCHDOG_ALERT, $link = NULL);
    return FALSE;
  }
  switch ($response->code) {
    case 200:
      watchdog('deployer_stash', 'Stash response ok 200', array(), WATCHDOG_INFO, $link = NULL);
      break;

    case 403:
      watchdog('deployer_stash', 'Stash auth wrong', array(), WATCHDOG_ALERT, $link = NULL);
      return FALSE;

    case 404:
      watchdog('deployer_stash', 'Stash auth wrong, or not-found returned', array(), WATCHDOG_ALERT, $link = NULL);
      return FALSE;

    default:
      watchdog('deployer_stash', 'Something wrong with Stash, status code ' . $response->code . ' .', array(), WATCHDOG_ALERT, $link = NULL);
      return FALSE;
  }

  if ($raw_data) {
    return $response->data;
  }
  return drupal_json_decode($response->data);
}
