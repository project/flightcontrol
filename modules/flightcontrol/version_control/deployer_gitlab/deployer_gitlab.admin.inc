<?php

/**
 * @file
 * The administrative function for the deployer_gitlab module.
 */

/**
 * Admin form used to congure the deployer gitlab module.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array
 *
 * @return array
 *   A renderable array of the form.
 */
function deployer_gitlab_admin($form, &$form_state) {

  $form['deployer_gitlab_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate gitlab connector'),
    '#default_value' => variable_get('deployer_gitlab_active', FALSE),
  );

  $form['deployer_gitlab_auth_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Gitlab username'),
    '#default_value' => variable_get('deployer_gitlab_auth_username', ''),
    '#required' => FALSE,
  );
  $form['deployer_gitlab_auth_password'] = array(
    '#type' => 'password',
    '#title' => t('Gitlab password'),
    '#default_value' => variable_get('deployer_gitlab_auth_password', ''),
    '#required' => FALSE,
    '#deployer_password' => TRUE,
  );

  $form['deployer_gitlab_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Gitlab url'),
    '#default_value' => variable_get('deployer_gitlab_url', ''),
    '#required' => TRUE,
    '#description' => t('Gitlab url, like https://my-cool-gitlab:8555'),
  );

  $form['deployer_gitlab_auth'] = array(
    '#type' => 'textfield',
    '#title' => t('Gitlab private token'),
    '#default_value' => variable_get('deployer_gitlab_auth', ''),
    '#required' => TRUE,
    '#description' => t('Get private token from a users account settings page.'),
  );

  return system_settings_form($form);
}
