<?php
/**
 * @file
 * Display list of field widgets.
 */

/**
 * Display tables with flightcontrol widgets and common widgets.
 */
function deployer_devel_field_formatters() {
  $formatters = field_info_formatter_types();

  $header = array('Machine name', 'Label', 'Module');
  $rows_common = array();
  $rows_flightcontrol = array();

  // Sort.
  ksort($formatters);

  foreach ($formatters as $delta => $widget) {
    if (strpos($delta, 'flightcontrol') !== FALSE || strpos($delta, 'deployer') !== FALSE) {
      $rows_flightcontrol[] = array($delta, $widget['label'], $widget['module']);
    }
    else {
      $rows_common[] = array($delta, $widget['label'], $widget['module']);
    }
  }

  $markup = theme('html_tag', array(
    'element' => array(
      '#tag' => 'h2',
      '#value' => t('Flightcontrol formatters'),
    ),
  ));
  $markup .= theme('table', array(
    'header' => $header,
    'rows' => $rows_flightcontrol,
  ));

  $markup .= theme('html_tag', array(
    'element' => array(
      '#tag' => 'h2',
      '#value' => t('Common formatters'),
    ),
  ));
  $markup .= theme('table', array(
    'header' => $header,
    'rows' => $rows_common,
  ));

  return $markup;
}
