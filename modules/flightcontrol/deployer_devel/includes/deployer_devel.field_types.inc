<?php
/**
 * @file
 * Display list of fields.
 */

/**
 * Display tables with field types
 */
function deployer_devel_field_types() {
  $fields = field_info_fields();

  $header = array('Machine name', 'Module');
  $rows_common = array();
  $rows_flightcontrol = array();

  // Sort.
  ksort($fields);

  foreach ($fields as $delta => $field) {
    $rows_common[] = array($delta, $field['module']);
  }
  $markup = theme('html_tag', array(
    'element' => array(
      '#tag' => 'h2',
      '#value' => t('Common field types'),
    ),
  ));
  $markup .= theme('table', array(
    'header' => $header,
    'rows' => $rows_common,
  ));

  return $markup;
}
