<?php
/**
 * @file
 * This file includes functionality to add exported blocks.
 */

/**
 * Add exported blocks to the system.
 */
function _deployer_export_blocks_install() {

  // Get a list of all roles defined by other modules.
  $defaults = module_invoke_all('deployer_export_boxes');

  if (empty($defaults)) {
    return;
  }

  // We remove the version, as we now want to deal with actual block settings.
  unset($defaults['version']);

  $themes_rehashed = array();
  $active_themes = _fe_block_get_active_themes();
  // The fallback theme for theme specific settings.
  $theme_default = variable_get('theme_default', 'bartik');

  foreach ($defaults as $block) {

    // Core custom blocks are prepared with a delta value.
    $block = _fe_block_prepare_custom_blocks_for_import($block);

    // Remove the additional settings from the block array, to process them
    // later. We explicitely set NULL, if no setting was given in the defaults.
    $block_themes = $block['themes'];
    $block_node_types = isset($block['node_types']) ? $block['node_types'] : NULL;
    $block_roles = isset($block['roles']) ? $block['roles'] : NULL;
    $block_css_class = isset($block['css_class']) ? $block['css_class'] : NULL;
    $block_i18n_block_language = isset($block['i18n_block_language']) ? $block['i18n_block_language'] : NULL;
    unset($block['themes']);
    unset($block['node_types']);
    unset($block['roles']);
    unset($block['css_class']);
    unset($block['i18n_block_language']);

    // Restore theme specific settings for every active theme.
    foreach ($active_themes as $theme) {

      // Rehash if we did not yet.
      if (empty($themes_rehashed[$theme])) {
        _block_rehash($theme);
        $themes_rehashed[$theme] = TRUE;
      }

      // Get the theme specific setting for the active theme.
      if (isset($block_themes[$theme])) {
        $key = $theme;
      }
      // Or fallback on the default theme.
      elseif (isset($block_themes[$theme_default])) {
        $key = $theme_default;
      }
      // Or fallback on the first available theme spec.
      else {
        $key = key($block_themes);
      }

      // Write block settings.
      $write = array_merge($block, $block_themes[$key]);
      drupal_write_record('block', $write, array('module', 'delta', 'theme'));
    }
    // Ensure global settings.
    _fe_block_settings_update_global_settings($block);

    // Set node type settings
    // (only if there were some defined, to avoid overwriting not yet exported
    // data).
    if (isset($block_node_types)) {
      _fe_block_settings_update_block_node_type_settings($block, $block_node_types);
    }

    // Apply role visibility settings.
    if (isset($block_roles)) {
      _fe_block_settings_update_block_roles($block, $block_roles);
    }

    // Update block CSS classes.
    if (isset($block_css_class) && module_exists('block_class')) {
      _fe_block_settings_update_block_css_class($block, $block_css_class);
    }

    // Set i18n_block languages.
    if (module_exists('i18n_block') && isset($block_i18n_block_language)) {
      _fe_block_settings_update_i18n_block_language($block, $block_i18n_block_language);
    }
  }

  // Clear block cache.
  cache_clear_all(NULL, 'cache_block');

  return TRUE;
}


/**
 * Implements hook_deployer_export_boxes().
 */
function deployer_export_blocks_deployer_export_boxes() {
  $export = array();

  $export['version'] = '2.0';

  $export['deployer-deployer_manage_links'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'deployer_manage_links',
    'module' => 'deployer',
    'node_types' => array(),
    'pages' => 'deploy-stats
deploy-stats/*',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'garland',
        'weight' => -9,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['deployer-deployer_status_messages'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'deployer_status_messages',
    'module' => 'deployer',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-help'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'help',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => 'help',
        'status' => 1,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'garland',
        'weight' => -9,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-management'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'management',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'garland',
        'weight' => 1,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-navigation'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'navigation',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-user-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'user-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'garland',
        'weight' => -9,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-new'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'new',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-online'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'online',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-environments-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'environments-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-projects-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'projects-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'garland',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-running_deploys-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'running_deploys-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'authenticated user' => 2,
    ),
    'themes' => array(
      'garland' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'garland',
        'weight' => -9,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['deployer-flightcontrol_bookmarks'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'flightcontrol_bookmarks',
    'module' => 'deployer',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'garland' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'garland',
        'weight' => -10,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
