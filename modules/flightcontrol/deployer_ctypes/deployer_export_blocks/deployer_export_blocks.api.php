<?php
/**
 * @file This file describes the hooks that are defined by this module.
 */

/**
 * This hook collects all exported blocks and adds them to the system.
 *
 * @return array
 *  A list of roles.
 */
function hook_deployer_export_boxes() {
  $export = array();

  $export['version'] = '2.0';

  $export['deployer-deployer_manage_links'] = array(
    'cache'      => 1,
    'custom'     => 0,
    'delta'      => 'deployer_manage_links',
    'module'     => 'deployer',
    'node_types' => array(),
    'pages'      => 'deploy-stats
deploy-stats/*',
    'roles'      => array(),
    'themes'     => array(
      'garland' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme'  => 'garland',
        'weight' => -9,
      ),
    ),
    'title'      => '',
    'visibility' => 0,
  );

  return $export;
}