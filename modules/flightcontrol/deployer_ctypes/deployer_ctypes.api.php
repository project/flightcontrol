<?php

/**
 * @file This file describes the hooks that are defined by this module.
 */

/**
 * Hook that collects all fields that need to be created from
 * the implementing module.
 *
 * @return array
 *   An indexed array of fields with at least field_name and type of
 *   field defined.
 */
function hook_deployer_ctypes_fields() {
  $fields = array();
  $fields[] = array(
    'field_name' => 'field_wiki',
    'type'       => 'link_field',
  );

  return $fields;
}

/**
 * Hook that collects info about content types that need to be created.
 * @return array
 * An indexed array of content type info that need to be created.
 */
function hook_deployer_ctypes_content_types() {
  $ctypes = array();
  $ctypes[] = array(
    'type'        => 'client',
    'name'        => t('Client'),
    'base'        => 'node_content',
    'description' => t("The client content type contains information about the client we do our projects for."),
    'custom'      => 1,
    'modified'    => 1,
    'locked'      => 0,
  );

  return $ctypes;
}

/**
 * Hook that collects info about field instances to be created.
 *
 * In this case "client" is the content type machine name.
 *
 * @return array
 *  An associative array where the keys are the machine names of the content
 *  type. And the values are an indexed array of field instance info to be
 *  added to the content type.
 */
function hook_deployer_ctypes_field_instances() {
  $instances = array();
  $instances['client'][] = array(
    'label'       => t('Wiki link'),
    'field_name'  => 'field_wiki',
    'entity_type' => 'node',
    'bundle'      => 'client',
    'cardinality' => 1,
    'locked'      => FALSE,
    'settings'    => array(
      'title'       => 'value',
      'title_value' => 'Wiki',
      'attributes'  => array('target' => '_blank')
    ),
  );

  return $instances;
}

/**
 * This hook collects all defined layout settings so that they can be
 * installed from one place.
 * For an example array @see hook_ds_layout_settings_info() in ds/ds.api.php
 */
function hook_deployer_ctypes_ds_layout_settings() {
  $dslayouts = array();
  return $dslayouts;
}

/**
 *This hook collects all defined ds view modes so that they can be
 * installed from one place.
 *
 * @return array
 *  A list of view modes including the entities they belong too.
 */
function hook_deployer_ctypes_ds_view_modes() {
  $view_modes = array();

  $view_modes['compact'] = array(
    'node',
  );

  return $view_modes;
}
