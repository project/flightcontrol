<?php
/**
 * @file This file describes the hooks that are defined by this module.
 */


/**
 *This hook collects all defined roles so that they can be
 * installed from one place.
 *
 * @return array
 *  A list of roles.
 */
function hook_deployer_permissions_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  return $roles;
}

/**
 *This hook collects all defined permissions so that they can be
 * installed from one place.
 *
 * @return array
 *  A list of permissions.
 */
function hook_deployer_permissions_perms() {
  $permissions = array();

  // Exported permission: 'access content'.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  return $permissions;
}