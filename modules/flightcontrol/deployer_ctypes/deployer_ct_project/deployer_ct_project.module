<?php

/**
 * @file
 * This file includes functionality to create the "project" content type.
 */

/**
 * Implements hook_deployer_ctypes_content_types().
 */
function deployer_ct_project_deployer_ctypes_content_types() {
  $ctypes = array();
  $ctypes['project'] = array(
    'type'        => 'project',
    'name'        => t('Project'),
    'base'        => 'node_content',
    'description' => t('The project content type contains information about the different project for the client nodes.'),
    'custom'      => 1,
    'modified'    => 1,
    'locked'      => 0,
    'has_title'   => '1',
    'title_label' => t('Titel'),
    'help'        => '',
  );
  drupal_alter('node_info', $ctypes);
  return $ctypes;
}

/**
 * Implements hook_ds_field_settings_info().
 */
function deployer_ct_project_ds_field_settings_info() {
  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|project|dashboard';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'project';
  $ds_fieldsetting->view_mode = 'dashboard';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_wiki_link' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_general_health_info' => array(
      'weight' => '16',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|project|dashboard'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|project|listview';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'project';
  $ds_fieldsetting->view_mode = 'listview';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_wiki_link' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_general_health_info' => array(
      'weight' => '16',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|project|listview'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_deployer_ctypes_fields().
 */
function deployer_ct_project_deployer_ctypes_fields() {
  $fields = array();

  // Entity reference fields.
  $fields[] = array(
    'field_name'  => 'field_client',
    'type'        => 'entityreference',
    'cardinality' => 1,
    'settings'    => array(
      'handler'          => 'base',
      'handler_settings' => array(
        'behaviors'      => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort'           => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'client' => 'client',
        ),
      ),
      'target_type'      => 'node',
    ),
  );

  // Select Drupal version.
  $fields[] = array(
    'field_name' => 'field_drupal_major_version',
    'settings' => array(
      'allowed_values' => array(
        '7' => '7.x',
        '8' => '8.x',
      ),
      'allowed_values_function' => '',
    ),
    'type' => 'list_text',
    'cardinality' => 1,
  );

  $fields[] = array(
    'field_name'  => 'field_composer_build',
    'type'        => 'list_boolean',
    'cardinality' => 1,
    'settings'    => array(
      'allowed_values'          => array(
        'no',
        'yes',
      ),
      'allowed_values_function' => '',
    ),
  );

  // Integer fields.
  $fields[] = array(
    'field_name'  => 'field_snapshot_limit',
    'type'        => 'number_integer',
    'cardinality' => 1,
    'settings'    => array(),
  );

  // Text fields.
  $fields[] = array(
    'field_name'  => 'field_code_project',
    'type'        => 'text',
    'cardinality' => 1,
    'settings'    => array(
      'max_length' => 255,
    ),
  );

  $fields[] = array(
    'field_name'  => 'field_repo_path',
    'type'        => 'text',
    'cardinality' => 1,
    'settings'    => array(
      'max_length' => 255,
    ),
  );

  $fields[] = array(
    'field_name'  => 'field_update_to_branch',
    'type'        => 'text',
    'cardinality' => 1,
    'settings'    => array(
      'max_length' => 255,
    ),
  );

  $fields[] = array(
    'field_name'  => 'field_hipchat_room',
    'type'        => 'text',
    'settings'    => array(
      'max_length' => 255,
    ),
    'cardinality' => 1,
  );

  // Boolean fields.
  $fields[] = array(
    'field_name'  => 'field_auto_updates',
    'type'        => 'list_boolean',
    'cardinality' => 1,
    'settings'    => array(
      'allowed_values' => array(
        '',
        '',
      ),
    ),
  );

  // Long text fields.
  $fields[] = array(
    'settings'    => array(),
    'field_name'  => 'field_locked_modules',
    'type'        => 'text_long',
    'cardinality' => 1,
  );

  return $fields;
}

/**
 * Implements hook_deployer_ctypes_field_instances().
 */
function deployer_ct_project_deployer_ctypes_field_instances() {
  $instances = array();
  $ctype = 'project';

  // Field field_client.
  $instances[$ctype][] = array(
    'default_value' => NULL,
    'description'   => '',
    'display'       => array(
      'dashboard' => array(
        'label'    => 'inline',
        'module'   => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type'     => 'entityreference_label',
        'weight'   => 1,
      ),
      'full'      => array(
        'label'    => 'inline',
        'module'   => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type'     => 'entityreference_label',
        'weight'   => 1,
      ),
      'listview'  => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 16,
      ),
      'teaser'    => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
    ),
    'label'         => 'Client',
    'required'      => 1,
    'settings'      => array(
      'user_register_form' => FALSE,
    ),
    'widget'        => array(
      'active'   => 1,
      'module'   => 'deployer_ct_project',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path'           => '',
        'size'           => 60,
      ),
      'type'     => 'deployer_ct_project_client_select',
      'weight'   => 2,
    ),
    'field_name'    => 'field_client',
    'entity_type'   => 'node',
    'bundle'        => 'project',
  );

  // Field field_code_project.
  $instances[$ctype][] = array(
    'default_value' => NULL,
    'description'   => 'Choose a version control repository for this project.',
    'display'     => array(
      'default'  => array(
        'label'    => 'inline',
        'module'   => 'deployer',
        'settings' => array(
          'format' => 'short',
        ),
        'type'     => 'deployer_repo_field_link',
        'weight'   => 2,
      ),
      'full'     => array(
        'label'    => 'inline',
        'module'   => 'deployer',
        'settings' => array(
          'format' => 'short',
        ),
        'type'     => 'deployer_repo_field_link',
        'weight'   => 2,
      ),
      'listview' => array(
        'label'    => 'inline',
        'module'   => 'deployer',
        'settings' => array(
          'format' => 'short',
        ),
        'type'     => 'deployer_repo_field_link',
        'weight'   => 1,
      ),
      'teaser'   => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
    ),
    'label'         => 'Version control repository',
    'required'      => 1,
    'settings'      => array(
      'text_processing'    => 0,
      'user_register_form' => FALSE,
    ),
    'widget'        => array(
      'active'   => 1,
      'module'   => 'deployer_ct_project',
      'settings' => array(),
      'type'     => 'deployer_ct_project_vcs_project_select',
      'weight'   => 2,
    ),
    'field_name'    => 'field_code_project',
    'entity_type'   => 'node',
    'bundle'        => 'project',
  );

  // Field field_drupal_major_version.
  $instances[$ctype][] = array(
    'default_value' => NULL,
    'description'   => "Select a major Drupal version. If you're still using Drupal 6, select '7.x'.",
    'display'     => array(
      'default'  => array(
        'label'    => 'inline',
        'module'   => 'list',
        'settings' => array(),
        'type'     => 'list_default',
        'weight'   => 2,
      ),
      'full'     => array(
        'label'    => 'inline',
        'module'   => 'list',
        'settings' => array(),
        'type'     => 'list_default',
        'weight'   => 2,
      ),
      'listview' => array(
        'label'    => 'inline',
        'module'   => 'list',
        'settings' => array(),
        'type'     => 'list_default',
        'weight'   => 2,
      ),
      'teaser'   => array(
        'label'    => 'inline',
        'module'   => 'list',
        'settings' => array(),
        'type'     => 'list_default',
        'weight'   => 2,
      ),
    ),
    'label'         => 'Major Drupal version',
    'required'      => 1,
    'settings'      => array(
      'text_processing'    => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 3,
    ),
    'field_name'    => 'field_drupal_major_version',
    'entity_type'   => 'node',
    'bundle'        => 'project',
  );

  // field_composer_build.
  $instances[$ctype][] = array(
    'default_value' => array(
      array(
        'value' => 1,
      ),
    ),
    'description'   => 'Build with composer if composer.json is present ?',
    'display'       => array(
      'default' => array(
        'label'    => 'inline',
        'module'   => 'list',
        'settings' => array(),
        'type'     => 'list_default',
        'weight'   => 12,
      ),
      'full'    => array(
        'label'    => 'inline',
        'module'   => 'list',
        'settings' => array(),
        'type'     => 'list_default',
        'weight'   => 7,
      ),
      'teaser'  => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
    ),
    'label'         => 'Composer build',
    'widget'        => array(
      'active'   => 1,
      'module'   => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type'     => 'options_onoff',
      'weight'   => 6,
    ),
    'field_name'    => 'field_composer_build',
    'entity_type'   => 'node',
    'bundle'        => 'project',
  );

  // Field field_snapshot_limit.
  $instances[$ctype][] = array(
    'default_value' => array(
      array(
        'value' => 2,
      ),
    ),
    'description'   => 'Max number of snapshots in this project.',
    'display'       => array(
      'default'  => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 18,
      ),
      'full'     => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 19,
      ),
      'listview' => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
      'teaser'   => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
    ),
    'label'         => 'Snapshot limit',
    'required'      => 0,
    'settings'      => array(
      'max'                => '',
      'min'                => '',
      'prefix'             => '',
      'suffix'             => ' snapshots per project',
      'user_register_form' => FALSE,
    ),
    'widget'        => array(
      'active'   => 0,
      'module'   => 'number',
      'settings' => array(),
      'type'     => 'number',
      'weight'   => 3,
    ),
    'field_name'    => 'field_snapshot_limit',
    'entity_type'   => 'node',
    'bundle'        => 'project',
  );

  // Field field_hipchat_room.
  $instances[$ctype][] = array(
    'default_value' => NULL,
    'description'   => "Room to send additional hipchat messages to.
\nMessages are also sent to global hipchat room used for status updates.",
    'display'       => array(
      'default'  => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 19,
      ),
      'full'     => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 20,
      ),
      'listview' => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
      'teaser'   => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
    ),
    'label'         => 'Hipchat  room',
    'required'      => 0,
    'settings'      => array(
      'text_processing'    => 0,
      'user_register_form' => FALSE,
    ),
    'widget'        => array(
      'active'   => 1,
      'module'   => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type'     => 'text_textfield',
      'weight'   => 4,
    ),
    'field_name'    => 'field_hipchat_room',
    'entity_type'   => 'node',
    'bundle'        => 'project',
  );

  // Field field_repo_path.
  $instances[$ctype][] = array(
    'default_value' => NULL,
    'description'   => '',
    'display'       => array(
      'default'  => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 20,
      ),
      'full'     => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 21,
      ),
      'listview' => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
      'teaser'   => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
    ),
    'label'         => 'repo_path',
    'required'      => 0,
    'settings'      => array(
      'text_processing'    => 0,
      'user_register_form' => FALSE,
    ),
    'widget'        => array(
      'active'   => 1,
      'module'   => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type'     => 'text_textfield',
      'weight'   => 8,
    ),
    'field_name'    => 'field_repo_path',
    'entity_type'   => 'node',
    'bundle'        => 'project',
  );

  // Field field_auto_updates.
  $instances[$ctype][] = array(
    'default_value' => array(
      array(
        'value' => 1,
      ),
    ),
    'description'   => "Enables auto-updates of drupal modules for this project.
\nUpdates are only performed if the global setting is enabled.",
    'display'       => array(
      'default'  => array(
        'label'    => 'above',
        'module'   => 'list',
        'settings' => array(),
        'type'     => 'list_default',
        'weight'   => 23,
      ),
      'listview' => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
      'teaser'   => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
      'full'     => array(
        'type'     => 'hidden',
        'label'    => 'above',
        'settings' => array(),
        'weight'   => 0,
      ),
    ),
    'label'         => 'Auto updates',
    'required'      => 0,
    'settings'      => array(
      'user_register_form' => FALSE,
    ),
    'widget'        => array(
      'active'   => 1,
      'module'   => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type'     => 'options_onoff',
      'weight'   => 10,
    ),
    'field_name'    => 'field_auto_updates',
    'entity_type'   => 'node',
    'bundle'        => 'project',
  );

  // Field field_update_to_branch.
  $instances[$ctype][] = array(
    'default_value' => NULL,
    'description'   => "Branch to push updates to.
\nLeave this empty to use the default branch, configured in the global settings

\nIf this branch does not exist, the default branch on the repository is used (master?)",
    'display'       => array(
      'default'  => array(
        'label'    => 'above',
        'module'   => 'text',
        'settings' => array(),
        'type'     => 'text_default',
        'weight'   => 24,
      ),
      'listview' => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
      'teaser'   => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
      'full'     => array(
        'type'     => 'hidden',
        'label'    => 'above',
        'settings' => array(),
        'weight'   => 0,
      ),
    ),
    'label'         => 'Branch',
    'required'      => 0,
    'settings'      => array(
      'text_processing'    => 0,
      'user_register_form' => FALSE,
    ),
    'widget'        => array(
      'active'   => 1,
      'module'   => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type'     => 'text_textfield',
      'weight'   => 11,
    ),
    'field_name'    => 'field_update_to_branch',
    'entity_type'   => 'node',
    'bundle'        => 'project',
  );

  // Field field_locked_modules.
  $instances[$ctype][] = array(
    'default_value' => NULL,
    'description'   => '',
    'display'       => array(
      'default'  => array(
        'label'    => 'above',
        'module'   => 'text',
        'settings' => array(),
        'type'     => 'text_default',
        'weight'   => 22,
      ),
      'listview' => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
      'teaser'   => array(
        'label'    => 'above',
        'settings' => array(),
        'type'     => 'hidden',
        'weight'   => 0,
      ),
      'full'     => array(
        'type'     => 'hidden',
        'label'    => 'above',
        'settings' => array(),
        'weight'   => 0,
      ),
    ),
    'label'         => 'Locked modules',
    'required'      => 0,
    'settings'      => array(
      'text_processing'    => 0,
      'user_register_form' => FALSE,
    ),
    'widget'        => array(
      'active'   => 1,
      'module'   => 'deployer_update',
      'settings' => array(
        'rows' => 5,
      ),
      'type'     => 'deployer_update_module_locks',
      'weight'   => 12,
    ),
    'field_name'    => 'field_locked_modules',
    'entity_type'   => 'node',
    'bundle'        => 'project',
  );

  return $instances;
}

/**
 * Implements hook_field_widget_info().
 */
function deployer_ct_project_field_widget_info() {
  return array(
    'deployer_ct_project_client_select'      => array(
      'label'       => t('Flightcontrol: (pre)select client'),
      'field types' => array('entityreference'),
      'behaviors'   => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      ),
    ),
    'deployer_ct_project_vcs_project_select' => array(
      'label'       => t('Flightcontrol: select project from version control'),
      'field types' => array('text'),
      'behaviors'   => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function deployer_ct_project_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  switch ($instance['widget']['type']) {
    case 'deployer_ct_project_client_select':
      // Call normal options-select widget.
      $instance['widget']['type'] = 'options_select';
      $element = options_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);

      // Pre-select client if it's valid..
      if (isset($_GET['client']) && !empty($_GET['client']) && is_numeric($_GET['client']) && $client = node_load($_GET['client'])) {
        // Set default client value.
        $element['#default_value'] = $_GET['client'];
      }
      break;

    case 'deployer_ct_project_vcs_project_select':
      // Create select widget with tags/commits for tag field.
      module_load_include('inc', 'deployer', 'deployer_fields_and_formatters');
      $element['value']['#type'] = 'select';
      $element['value']['#options'] = deployer_options_list($field);
      $element['value']['#default_value'] = isset($items[$delta]['value']) ? $items[$delta]['value'] : NULL;
      $element['value']['#title'] = $element['#title'];
      break;
  }

  return $element;
}

/**
 * Implements hook_deployer_variable_configs().
 */
function deployer_ct_project_flightcontrol_variable_configs() {

  $export = array();

  $strongarm = array(
    'view_modes'   => array(
      'teaser'        => array(
        'custom_settings' => FALSE,
      ),
      'full'          => array(
        'custom_settings' => TRUE,
      ),
      'rss'           => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token'         => array(
        'custom_settings' => FALSE,
      ),
      'revision'      => array(
        'custom_settings' => FALSE,
      ),
      'listview'      => array(
        'custom_settings' => TRUE,
      ),
      'compact'       => array(
        'custom_settings' => FALSE,
      ),
      'dashboard'     => array(
        'custom_settings' => TRUE,
      ),
    ),
    'extra_fields' => array(
      'form'    => array(
        'title' => array(
          'weight' => '1',
        ),
        'path'  => array(
          'weight' => '7',
        ),
        'flag'  => array(
          'weight' => '6',
        ),
      ),
      'display' => array(
        'flag_works'     => array(
          'default' => array(
            'weight'  => '18',
            'visible' => FALSE,
          ),
          'full'    => array(
            'weight'  => '18',
            'visible' => FALSE,
          ),
        ),
        'flag_bookmarks' => array(
          'default' => array(
            'weight'  => '0',
            'visible' => TRUE,
          ),
          'full'    => array(
            'weight'  => '0',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__project'] = $strongarm;

  $export['language_content_type_project'] = '0';

  $export['menu_options_project'] = array();

  $export['menu_parent_project'] = 'main-menu:0';

  $export['node_options_project'] = array(
    0 => 'status',
  );

  $export['node_preview_project'] = '0';

  $export['node_submitted_project'] = 0;

  return $export;
}

/**
 * Implements hook_views_api().
 */
function deployer_ct_project_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_views_default_views().
 */
function deployer_ct_project_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'projects';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Projects';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Projects';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'listview';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'reverse_field_project_node';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Connection */
  $handler->display->display_options['fields']['field_connection_type']['id'] = 'field_connection_type';
  $handler->display->display_options['fields']['field_connection_type']['table'] = 'field_data_field_connection_type';
  $handler->display->display_options['fields']['field_connection_type']['field'] = 'field_connection_type';
  $handler->display->display_options['fields']['field_connection_type']['relationship'] = 'reverse_field_project_node';
  /* Field: Content: Server type */
  $handler->display->display_options['fields']['field_server_type']['id'] = 'field_server_type';
  $handler->display->display_options['fields']['field_server_type']['table'] = 'field_data_field_server_type';
  $handler->display->display_options['fields']['field_server_type']['field'] = 'field_server_type';
  $handler->display->display_options['fields']['field_server_type']['relationship'] = 'reverse_field_project_node';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Contextual filter: Content: Client (field_client) */
  $handler->display->display_options['arguments']['field_client_target_id']['id'] = 'field_client_target_id';
  $handler->display->display_options['arguments']['field_client_target_id']['table'] = 'field_data_field_client';
  $handler->display->display_options['arguments']['field_client_target_id']['field'] = 'field_client_target_id';
  $handler->display->display_options['arguments']['field_client_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_client_target_id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['field_client_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_client_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_client_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_client_target_id']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['field_client_target_id']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['field_client_target_id']['validate_options']['types'] = array(
    'client' => 'client',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'project' => 'project',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['projects'] = $view;

  return $export;
}

/**
 * Implements hook_ds_fields_info().
 *
 * Add display suite field for the project content type.
 */
function deployer_ct_project_ds_fields_info($entity_type) {
  $node_fields = array();

  $node_fields['field_wiki_link'] = array(
    'title'      => t('Wiki'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'ui_limit'   => array(
      '*|*',
    ),
    'function'   => '_deployer_ct_project_ds_field_wiki_link',
  );

  $node_fields['field_general_health_info'] = array(
    'title'      => t('Health info'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'ui_limit'   => array(
      '*|*',
    ),
    'function'   => '_deployer_ct_project_ds_field_general_health_info',
  );

  $node_fields['field_connected_environments'] = array(
    'title'      => t('Connected environments'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'ui_limit'   => array(
      'project|*',
    ),
    'function'   => '_deployer_ct_project_ds_field_connected_environments',
  );

  return array(
    'node' => $node_fields,
  );
}

/**
 * Generate a wiki link.
 *
 * @param object $field
 *   field object (display suite).
 *
 * @return string
 *   Wiki link HTML.
 */
function _deployer_ct_project_ds_field_wiki_link($field) {
  $output = '';
  $node = $field['entity'];

  // Get client data.
  $client_data = field_get_items('node', $node, 'field_client');

  if (isset($client_data[0]['entity'])) {
    $client = $client_data[0]['entity'];
    $wiki_data = entity_metadata_wrapper('node', $client)->field_wiki->value();
    $url = entity_metadata_wrapper('node', $client)->field_wiki->value();

    if (!empty($url)) {
      $options = array(
        'attributes' => $wiki_data['attributes'],
        'html'       => FALSE,
      );

      $title = !empty($wiki_data['title']) ?: $field['title'];

      $variables = array(
        'text'    => $title,
        'path'    => $wiki_data['url'],
        'options' => $options,
      );

      $output = theme('link', $variables);
    }

  }

  return $output;
}

/**
 * Return general info about environments of a project.
 *
 * @param object $field
 *   Field object.
 *
 * @return string|bool
 *   HTML output or FALSE.
 */
function _deployer_ct_project_ds_field_general_health_info($field) {
  $html = '';
  $project = $field['entity'];

  $field_name = 'field_project';

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'environment')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->fieldCondition($field_name, 'target_id', $project->nid, '=');

  $result = $query->execute();

  if (isset($result['node'])) {
    $html = _deployer_ct_project_ds_field_general_health_info_html($result);
  }

  return $html;
}

/**
 * Return connected environments on a project.
 *
 * @param object $field
 *   Field object.
 *
 * @return string|bool
 *   HTML output or FALSE.
 */
function _deployer_ct_project_ds_field_connected_environments($field) {
  return views_embed_view('environments', 'block');
}

/**
 * Render html.
 *
 * @param array $result
 *   A list of node environments.
 *
 * @return string
 *   The html.
 */
function _deployer_ct_project_ds_field_general_health_info_html($result) {
  $html = '';
  $data = array();
  $server_type_list = array(
    'develop',
    'test',
    'acceptance',
    'production',
  );
  $environmet_nids = array_keys($result['node']);
  $environmet_items = entity_load('node', $environmet_nids);

  foreach ($environmet_items as $nid => $environmet_item) {
    $env = entity_metadata_wrapper('node', $environmet_item);
    $info = $env->field_health_info->value();
    $info_list = explode('|', $info);
    $server_type = $env->field_server_type->value();

    $data[$server_type_list[$server_type]]['health'] = isset($info_list[0]) ? $info_list[0] : '';
    $data[$server_type_list[$server_type]]['updates'] = isset($info_list[1]) ? $info_list[1] : '';
    $data[$server_type_list[$server_type]]['version'] = isset($info_list[2]) ? $info_list[2] : '';
    $data[$server_type_list[$server_type]]['health_info'] = $info;
  }

  // The order is very important e.g the production server is checked first.
  // Servers health?
  // 1: ok.
  // 2: warning, server in cluster down.
  // 3: critical, whole cluster down.
  $server_condition_bad = array(2, 3);
  $health = array('alive' => 1);
  $html .= _deployer_ct_project_render_health_status($data, 'health', $server_condition_bad, $health);

  // Updates level:
  // -1: unknown.
  // 0: no updates.
  // 1: regular updates.
  // 2: security updates.
  $health = array('updates' => 1);
  $application_updates = array(2, -1);
  $html .= _deployer_ct_project_render_health_status($data, 'updates', $application_updates, $health);

  // Incase all environments are okay, show status of one of the environments.
  if (empty($html)) {
    $html .= _deployer_ct_project_render_health_status($data);
    return $html;
  }

  return $html;
}

/**
 * Render health status html.
 *
 * @param array $data
 *   A list of data about an environment.
 *
 * @param string $value_to_check
 *   The value to check in the $list_data.
 *
 * @param array $list_data
 *   The options to look out for.
 *
 * @param array $info
 *   all, alive, updates or version.
 *   Makes function return proper parts.
 *
 * @return string
 *   Html string.
 */
function _deployer_ct_project_render_health_status($data, $value_to_check = '', $list_data = array(), $info = array('all' => 1)) {
  $html = '';

  foreach ($data as $server_name => $value) {
    switch ($server_name) {
      case 'production':

        $html = _deployer_ct_project_render_health_status_helper($value, $value_to_check, $list_data, $info);

        break;

      case 'acceptance':

        $html = _deployer_ct_project_render_health_status_helper($value, $value_to_check, $list_data, $info);

        break;

      case 'test':

        $html = _deployer_ct_project_render_health_status_helper($value, $value_to_check, $list_data, $info);

        break;

      case 'develop':

        $html = _deployer_ct_project_render_health_status_helper($value, $value_to_check, $list_data, $info);

        break;
    }
  }

  return $html;
}

/**
 * Render health status html helper.
 *
 * @param $value_to_check
 * @param $list_data
 * @param $info
 * @param $value
 * @return string
 */
function _deployer_ct_project_render_health_status_helper($value, $value_to_check = '', $list_data = array(), $info = array('all' => 1)) {
  module_load_include('inc', 'deployer', 'deployer_helper_functions.inc');
  $html = '';

  // Fill the info list with defaults
  $info += array(
    'all'     => NULL,
    'updates' => NULL,
    'alive'   => NULL,
  );

  if (!empty($list_data) && !empty($value_to_check)) {
    if (in_array($value[$value_to_check], $list_data)) {
      $html = _deployer_interpret_health_status($value['health_info'], $info);
    }
  }
  else {
    $html = _deployer_interpret_health_status($value['health_info'], $info);
  }

  return $html;
}
