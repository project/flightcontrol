<?php

/**
 * @file This file stores code for the view: history_of_deploys.
 */

/**
 * View: deploy_stats.
 */
function deployer_views_deploy_stats(&$export) {

  $view = new view();
  $view->name = 'deploy_stats';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Deploy Stats';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Deploy Stats';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Zoeken';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'X';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_environment_id' => 'field_environment_id',
    'title_1' => 'title_1',
    'title_2' => 'title_2',
    'field_server_type' => 'field_server_type',
    'created' => 'created',
    'field_project_id' => 'field_project_id',
    'field_customer_id' => 'field_customer_id',
    'field_commit_tag' => 'field_commit_tag',
  );
  $handler->display->display_options['style_options']['default'] = 'created';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_environment_id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_2' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_server_type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_project_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_customer_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_commit_tag' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_environment_id_target_id']['id'] = 'field_environment_id_target_id';
  $handler->display->display_options['relationships']['field_environment_id_target_id']['table'] = 'field_data_field_environment_id';
  $handler->display->display_options['relationships']['field_environment_id_target_id']['field'] = 'field_environment_id_target_id';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_project_target_id']['id'] = 'field_project_target_id';
  $handler->display->display_options['relationships']['field_project_target_id']['table'] = 'field_data_field_project';
  $handler->display->display_options['relationships']['field_project_target_id']['field'] = 'field_project_target_id';
  $handler->display->display_options['relationships']['field_project_target_id']['relationship'] = 'field_environment_id_target_id';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_client_target_id']['id'] = 'field_client_target_id';
  $handler->display->display_options['relationships']['field_client_target_id']['table'] = 'field_data_field_client';
  $handler->display->display_options['relationships']['field_client_target_id']['field'] = 'field_client_target_id';
  $handler->display->display_options['relationships']['field_client_target_id']['relationship'] = 'field_project_target_id';
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Field: Content: Deploystatus */
  $handler->display->display_options['fields']['field_deploystatus']['id'] = 'field_deploystatus';
  $handler->display->display_options['fields']['field_deploystatus']['table'] = 'field_data_field_deploystatus';
  $handler->display->display_options['fields']['field_deploystatus']['field'] = 'field_deploystatus';
  $handler->display->display_options['fields']['field_deploystatus']['label'] = 'Status';
  $handler->display->display_options['fields']['field_deploystatus']['type'] = 'deployer_status_formatter';
  $handler->display->display_options['fields']['field_deploystatus']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Type */
  $handler->display->display_options['fields']['field_commit_tag']['id'] = 'field_commit_tag';
  $handler->display->display_options['fields']['field_commit_tag']['table'] = 'field_data_field_commit_tag';
  $handler->display->display_options['fields']['field_commit_tag']['field'] = 'field_commit_tag';
  $handler->display->display_options['fields']['field_commit_tag']['ui_name'] = 'Type';
  $handler->display->display_options['fields']['field_commit_tag']['label'] = 'Type';
  $handler->display->display_options['fields']['field_commit_tag']['click_sort_column'] = 'format';
  $handler->display->display_options['fields']['field_commit_tag']['type'] = 'deployer_deploy_type';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'Operator';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'node';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['relationship'] = 'field_client_target_id';
  $handler->display->display_options['fields']['title_1']['label'] = 'Klant';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title_2']['id'] = 'title_2';
  $handler->display->display_options['fields']['title_2']['table'] = 'node';
  $handler->display->display_options['fields']['title_2']['field'] = 'title';
  $handler->display->display_options['fields']['title_2']['relationship'] = 'field_project_target_id';
  $handler->display->display_options['fields']['title_2']['label'] = 'Project';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Node';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Environment ID */
  $handler->display->display_options['fields']['field_environment_id']['id'] = 'field_environment_id';
  $handler->display->display_options['fields']['field_environment_id']['table'] = 'field_data_field_environment_id';
  $handler->display->display_options['fields']['field_environment_id']['field'] = 'field_environment_id';
  $handler->display->display_options['fields']['field_environment_id']['settings'] = array(
    'link' => 1,
  );
  /* Field: Content: Server type */
  $handler->display->display_options['fields']['field_server_type']['id'] = 'field_server_type';
  $handler->display->display_options['fields']['field_server_type']['table'] = 'field_data_field_server_type';
  $handler->display->display_options['fields']['field_server_type']['field'] = 'field_server_type';
  $handler->display->display_options['fields']['field_server_type']['relationship'] = 'field_environment_id_target_id';
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Datum';
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  /* Field: Content: Projectnummer */
  $handler->display->display_options['fields']['field_project_id']['id'] = 'field_project_id';
  $handler->display->display_options['fields']['field_project_id']['table'] = 'field_data_field_project_id';
  $handler->display->display_options['fields']['field_project_id']['field'] = 'field_project_id';
  $handler->display->display_options['fields']['field_project_id']['relationship'] = 'field_project_target_id';
  $handler->display->display_options['fields']['field_project_id']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_project_id']['alter']['path'] = 'https://www.intranet.finalist.com/FacturenOnline/beheerProjecten/ProjectInitAction.do?projectId=[field_project_id-value]';
  $handler->display->display_options['fields']['field_project_id']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Klantnummer */
  $handler->display->display_options['fields']['field_customer_id']['id'] = 'field_customer_id';
  $handler->display->display_options['fields']['field_customer_id']['table'] = 'field_data_field_customer_id';
  $handler->display->display_options['fields']['field_customer_id']['field'] = 'field_customer_id';
  $handler->display->display_options['fields']['field_customer_id']['relationship'] = 'field_client_target_id';
  $handler->display->display_options['fields']['field_customer_id']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_customer_id']['alter']['path'] = 'https://www.intranet.finalist.com/FacturenOnline/beheerKlanten/KlantInitAction.do?klantId=[field_customer_id-value]';
  $handler->display->display_options['fields']['field_customer_id']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Tag link */
  $handler->display->display_options['fields']['field_tag_link']['id'] = 'field_tag_link';
  $handler->display->display_options['fields']['field_tag_link']['table'] = 'field_data_field_tag_link';
  $handler->display->display_options['fields']['field_tag_link']['field'] = 'field_tag_link';
  $handler->display->display_options['fields']['field_tag_link']['label'] = 'Tag';
  $handler->display->display_options['fields']['field_tag_link']['alter']['max_length'] = '20';
  $handler->display->display_options['fields']['field_tag_link']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['field_tag_link']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['field_tag_link']['alter']['html'] = TRUE;
  $handler->display->display_options['fields']['field_tag_link']['click_sort_column'] = 'url';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'deploy' => 'deploy',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['relationship'] = 'field_client_target_id';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['group'] = 1;
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Klant';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'klant';
  $handler->display->display_options['filters']['title']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title_1']['id'] = 'title_1';
  $handler->display->display_options['filters']['title_1']['table'] = 'node';
  $handler->display->display_options['filters']['title_1']['field'] = 'title';
  $handler->display->display_options['filters']['title_1']['relationship'] = 'field_project_target_id';
  $handler->display->display_options['filters']['title_1']['operator'] = 'contains';
  $handler->display->display_options['filters']['title_1']['group'] = 1;
  $handler->display->display_options['filters']['title_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title_1']['expose']['operator_id'] = 'title_1_op';
  $handler->display->display_options['filters']['title_1']['expose']['label'] = 'Project';
  $handler->display->display_options['filters']['title_1']['expose']['operator'] = 'title_1_op';
  $handler->display->display_options['filters']['title_1']['expose']['identifier'] = 'project';
  $handler->display->display_options['filters']['title_1']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['title_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );
  /* Filter criterion: Content: Deploystatus (field_deploystatus) */
  $handler->display->display_options['filters']['field_deploystatus_value']['id'] = 'field_deploystatus_value';
  $handler->display->display_options['filters']['field_deploystatus_value']['table'] = 'field_data_field_deploystatus';
  $handler->display->display_options['filters']['field_deploystatus_value']['field'] = 'field_deploystatus_value';
  $handler->display->display_options['filters']['field_deploystatus_value']['group'] = 1;
  $handler->display->display_options['filters']['field_deploystatus_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_deploystatus_value']['expose']['operator_id'] = 'field_deploystatus_value_op';
  $handler->display->display_options['filters']['field_deploystatus_value']['expose']['label'] = 'Deploystatus (field_deploystatus)';
  $handler->display->display_options['filters']['field_deploystatus_value']['expose']['operator'] = 'field_deploystatus_value_op';
  $handler->display->display_options['filters']['field_deploystatus_value']['expose']['identifier'] = 'field_deploystatus_value';
  $handler->display->display_options['filters']['field_deploystatus_value']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['field_deploystatus_value']['group_info']['label'] = 'Status';
  $handler->display->display_options['filters']['field_deploystatus_value']['group_info']['identifier'] = 'field_deploystatus_value';
  $handler->display->display_options['filters']['field_deploystatus_value']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'OK',
      'operator' => '=',
      'value' => array(
        'value' => '1',
        'min' => '',
        'max' => '',
      ),
    ),
    2 => array(
      'title' => 'Niet OK',
      'operator' => '=',
      'value' => array(
        'value' => '2',
        'min' => '',
        'max' => '',
      ),
    ),
    3 => array(
      'title' => '',
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
    ),
  );
  /* Filter criterion: Type */
  $handler->display->display_options['filters']['field_commit_tag_value']['id'] = 'field_commit_tag_value';
  $handler->display->display_options['filters']['field_commit_tag_value']['table'] = 'field_data_field_commit_tag';
  $handler->display->display_options['filters']['field_commit_tag_value']['field'] = 'field_commit_tag_value';
  $handler->display->display_options['filters']['field_commit_tag_value']['ui_name'] = 'Type';
  $handler->display->display_options['filters']['field_commit_tag_value']['group'] = 1;
  $handler->display->display_options['filters']['field_commit_tag_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_commit_tag_value']['expose']['operator_id'] = 'field_commit_tag_value_op';
  $handler->display->display_options['filters']['field_commit_tag_value']['expose']['label'] = 'Commit tag / hash (field_commit_tag)';
  $handler->display->display_options['filters']['field_commit_tag_value']['expose']['operator'] = 'field_commit_tag_value_op';
  $handler->display->display_options['filters']['field_commit_tag_value']['expose']['identifier'] = 'field_commit_tag_value';
  $handler->display->display_options['filters']['field_commit_tag_value']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['field_commit_tag_value']['group_info']['label'] = 'Type';
  $handler->display->display_options['filters']['field_commit_tag_value']['group_info']['identifier'] = 'field_commit_tag_value';
  $handler->display->display_options['filters']['field_commit_tag_value']['group_info']['remember'] = 1;
  $handler->display->display_options['filters']['field_commit_tag_value']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'Deploy',
      'operator' => 'not_starts',
      'value' => '--',
    ),
    2 => array(
      'title' => 'Backup',
      'operator' => '=',
      'value' => '--dummy--',
    ),
    3 => array(
      'title' => 'Restore',
      'operator' => '=',
      'value' => '--restore--',
    ),
  );
  /* Filter criterion: Content: Server type (field_server_type) */
  $handler->display->display_options['filters']['field_server_type_value']['id'] = 'field_server_type_value';
  $handler->display->display_options['filters']['field_server_type_value']['table'] = 'field_data_field_server_type';
  $handler->display->display_options['filters']['field_server_type_value']['field'] = 'field_server_type_value';
  $handler->display->display_options['filters']['field_server_type_value']['relationship'] = 'field_environment_id_target_id';
  $handler->display->display_options['filters']['field_server_type_value']['value'] = array(
    1 => '1',
    2 => '2',
    3 => '3',
  );
  $handler->display->display_options['filters']['field_server_type_value']['group'] = 1;
  $handler->display->display_options['filters']['field_server_type_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_server_type_value']['expose']['operator_id'] = 'field_server_type_value_op';
  $handler->display->display_options['filters']['field_server_type_value']['expose']['label'] = 'OTAP';
  $handler->display->display_options['filters']['field_server_type_value']['expose']['operator'] = 'field_server_type_value_op';
  $handler->display->display_options['filters']['field_server_type_value']['expose']['identifier'] = 'field_server_type_value';
  $handler->display->display_options['filters']['field_server_type_value']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['field_server_type_value']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_server_type_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_server_type_value']['expose']['reduce'] = TRUE;
  /* Filter criterion: Date: Date (node) */
  $handler->display->display_options['filters']['date_filter']['id'] = 'date_filter';
  $handler->display->display_options['filters']['date_filter']['table'] = 'node';
  $handler->display->display_options['filters']['date_filter']['field'] = 'date_filter';
  $handler->display->display_options['filters']['date_filter']['operator'] = 'between';
  $handler->display->display_options['filters']['date_filter']['group'] = 1;
  $handler->display->display_options['filters']['date_filter']['exposed'] = TRUE;
  $handler->display->display_options['filters']['date_filter']['expose']['operator_id'] = 'date_filter_op';
  $handler->display->display_options['filters']['date_filter']['expose']['operator'] = 'date_filter_op';
  $handler->display->display_options['filters']['date_filter']['expose']['identifier'] = 'date_filter';
  $handler->display->display_options['filters']['date_filter']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['date_filter']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['date_filter']['default_date'] = 'first day of previous month';
  $handler->display->display_options['filters']['date_filter']['default_to_date'] = 'today';
  $handler->display->display_options['filters']['date_filter']['year_range'] = '-3:+0';
  $handler->display->display_options['filters']['date_filter']['date_fields'] = array(
    'node.created' => 'node.created',
  );
  /* Filter criterion: Content: Klantnummer (field_customer_id) */
  $handler->display->display_options['filters']['field_customer_id_value']['id'] = 'field_customer_id_value';
  $handler->display->display_options['filters']['field_customer_id_value']['table'] = 'field_data_field_customer_id';
  $handler->display->display_options['filters']['field_customer_id_value']['field'] = 'field_customer_id_value';
  $handler->display->display_options['filters']['field_customer_id_value']['relationship'] = 'field_client_target_id';
  $handler->display->display_options['filters']['field_customer_id_value']['group'] = 1;
  $handler->display->display_options['filters']['field_customer_id_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_customer_id_value']['expose']['operator_id'] = 'field_customer_id_value_op';
  $handler->display->display_options['filters']['field_customer_id_value']['expose']['label'] = 'Klantnummer (FUO)';
  $handler->display->display_options['filters']['field_customer_id_value']['expose']['operator'] = 'field_customer_id_value_op';
  $handler->display->display_options['filters']['field_customer_id_value']['expose']['identifier'] = 'field_customer_id_value';
  $handler->display->display_options['filters']['field_customer_id_value']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['field_customer_id_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );
  /* Filter criterion: Content: Projectnummer (field_project_id) */
  $handler->display->display_options['filters']['field_project_id_value']['id'] = 'field_project_id_value';
  $handler->display->display_options['filters']['field_project_id_value']['table'] = 'field_data_field_project_id';
  $handler->display->display_options['filters']['field_project_id_value']['field'] = 'field_project_id_value';
  $handler->display->display_options['filters']['field_project_id_value']['relationship'] = 'field_project_target_id';
  $handler->display->display_options['filters']['field_project_id_value']['group'] = 1;
  $handler->display->display_options['filters']['field_project_id_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_project_id_value']['expose']['operator_id'] = 'field_project_id_value_op';
  $handler->display->display_options['filters']['field_project_id_value']['expose']['label'] = 'Projectnummer (FUO)';
  $handler->display->display_options['filters']['field_project_id_value']['expose']['operator'] = 'field_project_id_value_op';
  $handler->display->display_options['filters']['field_project_id_value']['expose']['identifier'] = 'field_project_id_value';
  $handler->display->display_options['filters']['field_project_id_value']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['field_project_id_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'deploy-stats';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Statistics';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Data export CSV */
  $handler = $view->new_display('views_data_export', 'Data export CSV', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['style_options']['provide_file'] = 0;
  $handler->display->display_options['style_options']['parent_sort'] = 0;
  $handler->display->display_options['style_options']['quote'] = 1;
  $handler->display->display_options['style_options']['trim'] = 0;
  $handler->display->display_options['style_options']['replace_newlines'] = 0;
  $handler->display->display_options['style_options']['header'] = 1;
  $handler->display->display_options['style_options']['keep_html'] = 0;
  $handler->display->display_options['path'] = 'deploy-stats/export/csv';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );

  /* Display: Data export XLS */
  $handler = $view->new_display('views_data_export', 'Data export XLS', 'views_data_export_2');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_xls';
  $handler->display->display_options['style_options']['provide_file'] = 1;
  $handler->display->display_options['style_options']['parent_sort'] = 0;
  $handler->display->display_options['path'] = 'deploy-stats/export/xls';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );

  $export['deploy_stats'] = $view;
}
