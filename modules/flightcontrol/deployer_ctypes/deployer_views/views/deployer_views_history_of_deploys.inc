<?php

/**
 * @file This file stores code for the view: history_of_deploys.
 */

/**
 * View: history_of_deploys.
 */
function deployer_views_history_of_deploys(&$export) {

  $view = new view();
  $view->name = 'history_of_deploys';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'History of deploys';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Deploy history';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'title_1' => 'title_1',
    'title_2' => 'title_2',
    'title_3' => 'title_3',
    'changed' => 'changed',
    'name' => 'name',
  );
  $handler->display->display_options['style_options']['default'] = 'changed';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_2' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_3' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'changed' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'No related deploys.';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_environment_id_target_id']['id'] = 'field_environment_id_target_id';
  $handler->display->display_options['relationships']['field_environment_id_target_id']['table'] = 'field_data_field_environment_id';
  $handler->display->display_options['relationships']['field_environment_id_target_id']['field'] = 'field_environment_id_target_id';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_project_target_id']['id'] = 'field_project_target_id';
  $handler->display->display_options['relationships']['field_project_target_id']['table'] = 'field_data_field_project';
  $handler->display->display_options['relationships']['field_project_target_id']['field'] = 'field_project_target_id';
  $handler->display->display_options['relationships']['field_project_target_id']['relationship'] = 'field_environment_id_target_id';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_client_target_id']['id'] = 'field_client_target_id';
  $handler->display->display_options['relationships']['field_client_target_id']['table'] = 'field_data_field_client';
  $handler->display->display_options['relationships']['field_client_target_id']['field'] = 'field_client_target_id';
  $handler->display->display_options['relationships']['field_client_target_id']['relationship'] = 'field_project_target_id';
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_project_target_id_1']['id'] = 'field_project_target_id_1';
  $handler->display->display_options['relationships']['field_project_target_id_1']['table'] = 'field_data_field_project';
  $handler->display->display_options['relationships']['field_project_target_id_1']['field'] = 'field_project_target_id';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Deploy';
  $handler->display->display_options['fields']['title']['alter']['max_length'] = '10';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['trim'] = TRUE;
  /* Field: Environment title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'node';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['relationship'] = 'field_environment_id_target_id';
  $handler->display->display_options['fields']['title_1']['ui_name'] = 'Environment title';
  $handler->display->display_options['fields']['title_1']['label'] = 'Environment';
  $handler->display->display_options['fields']['title_1']['alter']['max_length'] = '15';
  $handler->display->display_options['fields']['title_1']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title_1']['alter']['trim'] = TRUE;
  /* Field: Project title */
  $handler->display->display_options['fields']['title_2']['id'] = 'title_2';
  $handler->display->display_options['fields']['title_2']['table'] = 'node';
  $handler->display->display_options['fields']['title_2']['field'] = 'title';
  $handler->display->display_options['fields']['title_2']['relationship'] = 'field_project_target_id';
  $handler->display->display_options['fields']['title_2']['ui_name'] = 'Project title';
  $handler->display->display_options['fields']['title_2']['label'] = 'Project';
  $handler->display->display_options['fields']['title_2']['alter']['max_length'] = '15';
  $handler->display->display_options['fields']['title_2']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title_2']['alter']['trim'] = TRUE;
  /* Field: Client title */
  $handler->display->display_options['fields']['title_3']['id'] = 'title_3';
  $handler->display->display_options['fields']['title_3']['table'] = 'node';
  $handler->display->display_options['fields']['title_3']['field'] = 'title';
  $handler->display->display_options['fields']['title_3']['relationship'] = 'field_client_target_id';
  $handler->display->display_options['fields']['title_3']['ui_name'] = 'Client title';
  $handler->display->display_options['fields']['title_3']['label'] = 'Klant';
  /* Field: Content: Tag link */
  $handler->display->display_options['fields']['field_tag_link']['id'] = 'field_tag_link';
  $handler->display->display_options['fields']['field_tag_link']['table'] = 'field_data_field_tag_link';
  $handler->display->display_options['fields']['field_tag_link']['field'] = 'field_tag_link';
  $handler->display->display_options['fields']['field_tag_link']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_tag_link']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_tag_link']['alter']['path'] = '[field_tag_link-url]';
  $handler->display->display_options['fields']['field_tag_link']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['field_tag_link']['alter']['alt'] = '[field_tag_link-title]';
  $handler->display->display_options['fields']['field_tag_link']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_tag_link']['alter']['max_length'] = '15';
  $handler->display->display_options['fields']['field_tag_link']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_tag_link']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_tag_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_tag_link']['type'] = 'link_separate';
  /* Field: Content: Updated date */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'node';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['label'] = 'Date';
  $handler->display->display_options['fields']['changed']['date_format'] = 'short';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'Author';
  /* Field: Content: Deploystatus */
  $handler->display->display_options['fields']['field_deploystatus']['id'] = 'field_deploystatus';
  $handler->display->display_options['fields']['field_deploystatus']['table'] = 'field_data_field_deploystatus';
  $handler->display->display_options['fields']['field_deploystatus']['field'] = 'field_deploystatus';
  $handler->display->display_options['fields']['field_deploystatus']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_deploystatus']['type'] = 'deployer_status_formatter';
  $handler->display->display_options['fields']['field_deploystatus']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Environment ID (field_environment_id) */
  $handler->display->display_options['arguments']['field_environment_id_target_id']['id'] = 'field_environment_id_target_id';
  $handler->display->display_options['arguments']['field_environment_id_target_id']['table'] = 'field_data_field_environment_id';
  $handler->display->display_options['arguments']['field_environment_id_target_id']['field'] = 'field_environment_id_target_id';
  $handler->display->display_options['arguments']['field_environment_id_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_environment_id_target_id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['field_environment_id_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_environment_id_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_environment_id_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'deploy' => 'deploy',
  );
  /* Filter criterion: Content: Deploystatus (field_deploystatus) */
  $handler->display->display_options['filters']['field_deploystatus_value']['id'] = 'field_deploystatus_value';
  $handler->display->display_options['filters']['field_deploystatus_value']['table'] = 'field_data_field_deploystatus';
  $handler->display->display_options['filters']['field_deploystatus_value']['field'] = 'field_deploystatus_value';
  $handler->display->display_options['filters']['field_deploystatus_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_deploystatus_value']['value']['min'] = '0.1';
  $handler->display->display_options['filters']['field_deploystatus_value']['value']['value'] = '1';
  /* Filter criterion: Klant-naam-filter */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['relationship'] = 'field_client_target_id';
  $handler->display->display_options['filters']['title']['ui_name'] = 'Klant-naam-filter';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Klant';
  $handler->display->display_options['filters']['title']['expose']['description'] = 'Filter op klantnaam of een deel daarvan.';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'client';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: User: Name (raw) */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'users';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['relationship'] = 'uid';
  $handler->display->display_options['filters']['name']['operator'] = 'contains';
  $handler->display->display_options['filters']['name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['label'] = 'Author';
  $handler->display->display_options['filters']['name']['expose']['description'] = 'Filter op author of een deel daarvan.';
  $handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
  $handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_env');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Deploy history per environment';
  $handler->display->display_options['defaults']['use_more'] = FALSE;
  $handler->display->display_options['defaults']['use_more_always'] = FALSE;
  $handler->display->display_options['defaults']['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_always'] = TRUE;
  $handler->display->display_options['defaults']['use_more_text'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'Full list';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'title_1' => 'title_1',
    'title_2' => 'title_2',
    'title_3' => 'title_3',
    'changed' => 'changed',
    'name' => 'name',
  );
  $handler->display->display_options['style_options']['default'] = 'changed';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_2' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_3' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'changed' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_environment_id_target_id']['id'] = 'field_environment_id_target_id';
  $handler->display->display_options['relationships']['field_environment_id_target_id']['table'] = 'field_data_field_environment_id';
  $handler->display->display_options['relationships']['field_environment_id_target_id']['field'] = 'field_environment_id_target_id';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_project_target_id']['id'] = 'field_project_target_id';
  $handler->display->display_options['relationships']['field_project_target_id']['table'] = 'field_data_field_project';
  $handler->display->display_options['relationships']['field_project_target_id']['field'] = 'field_project_target_id';
  $handler->display->display_options['relationships']['field_project_target_id']['relationship'] = 'field_environment_id_target_id';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_client_target_id']['id'] = 'field_client_target_id';
  $handler->display->display_options['relationships']['field_client_target_id']['table'] = 'field_data_field_client';
  $handler->display->display_options['relationships']['field_client_target_id']['field'] = 'field_client_target_id';
  $handler->display->display_options['relationships']['field_client_target_id']['relationship'] = 'field_project_target_id';
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Deploy';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Environment title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'node';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['relationship'] = 'field_environment_id_target_id';
  $handler->display->display_options['fields']['title_1']['ui_name'] = 'Environment title';
  $handler->display->display_options['fields']['title_1']['label'] = 'Environment';
  $handler->display->display_options['fields']['title_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title_1']['alter']['max_length'] = '40';
  $handler->display->display_options['fields']['title_1']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title_1']['alter']['trim'] = TRUE;
  /* Field: Project title */
  $handler->display->display_options['fields']['title_2']['id'] = 'title_2';
  $handler->display->display_options['fields']['title_2']['table'] = 'node';
  $handler->display->display_options['fields']['title_2']['field'] = 'title';
  $handler->display->display_options['fields']['title_2']['relationship'] = 'field_project_target_id';
  $handler->display->display_options['fields']['title_2']['ui_name'] = 'Project title';
  $handler->display->display_options['fields']['title_2']['label'] = 'Project';
  $handler->display->display_options['fields']['title_2']['exclude'] = TRUE;
  /* Field: Client title */
  $handler->display->display_options['fields']['title_3']['id'] = 'title_3';
  $handler->display->display_options['fields']['title_3']['table'] = 'node';
  $handler->display->display_options['fields']['title_3']['field'] = 'title';
  $handler->display->display_options['fields']['title_3']['relationship'] = 'field_client_target_id';
  $handler->display->display_options['fields']['title_3']['ui_name'] = 'Client title';
  $handler->display->display_options['fields']['title_3']['label'] = 'Klant';
  $handler->display->display_options['fields']['title_3']['exclude'] = TRUE;
  /* Field: Content: Updated date */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'node';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['label'] = 'Date';
  $handler->display->display_options['fields']['changed']['date_format'] = 'long';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'Author';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'deploy' => 'deploy',
  );
  /* Filter criterion: Content: Deploystatus (field_deploystatus) */
  $handler->display->display_options['filters']['field_deploystatus_value']['id'] = 'field_deploystatus_value';
  $handler->display->display_options['filters']['field_deploystatus_value']['table'] = 'field_data_field_deploystatus';
  $handler->display->display_options['filters']['field_deploystatus_value']['field'] = 'field_deploystatus_value';
  $handler->display->display_options['filters']['field_deploystatus_value']['value']['min'] = '0.1';
  $handler->display->display_options['filters']['field_deploystatus_value']['value']['value'] = '1';
  $handler->display->display_options['block_description'] = 'Deploy history';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_project');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Deploy history per project';
  $handler->display->display_options['defaults']['use_more'] = FALSE;
  $handler->display->display_options['use_more'] = TRUE;
  $handler->display->display_options['defaults']['use_more_always'] = FALSE;
  $handler->display->display_options['defaults']['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_always'] = TRUE;
  $handler->display->display_options['defaults']['use_more_text'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'Full list';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'title_1' => 'title_1',
    'title_2' => 'title_2',
    'title_3' => 'title_3',
    'changed' => 'changed',
  );
  $handler->display->display_options['style_options']['default'] = 'changed';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_2' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_3' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'changed' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_environment_id_target_id']['id'] = 'field_environment_id_target_id';
  $handler->display->display_options['relationships']['field_environment_id_target_id']['table'] = 'field_data_field_environment_id';
  $handler->display->display_options['relationships']['field_environment_id_target_id']['field'] = 'field_environment_id_target_id';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_project_target_id']['id'] = 'field_project_target_id';
  $handler->display->display_options['relationships']['field_project_target_id']['table'] = 'field_data_field_project';
  $handler->display->display_options['relationships']['field_project_target_id']['field'] = 'field_project_target_id';
  $handler->display->display_options['relationships']['field_project_target_id']['relationship'] = 'field_environment_id_target_id';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_client_target_id']['id'] = 'field_client_target_id';
  $handler->display->display_options['relationships']['field_client_target_id']['table'] = 'field_data_field_client';
  $handler->display->display_options['relationships']['field_client_target_id']['field'] = 'field_client_target_id';
  $handler->display->display_options['relationships']['field_client_target_id']['relationship'] = 'field_project_target_id';
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Relationship: environments linking to project */
  $handler->display->display_options['relationships']['reverse_field_project_node']['id'] = 'reverse_field_project_node';
  $handler->display->display_options['relationships']['reverse_field_project_node']['table'] = 'node';
  $handler->display->display_options['relationships']['reverse_field_project_node']['field'] = 'reverse_field_project_node';
  $handler->display->display_options['relationships']['reverse_field_project_node']['ui_name'] = 'environments linking to project';
  $handler->display->display_options['relationships']['reverse_field_project_node']['label'] = 'Content referencing Content from field_project (Environments)';
  /* Relationship: Deploys referencing environment */
  $handler->display->display_options['relationships']['reverse_field_environment_id_node']['id'] = 'reverse_field_environment_id_node';
  $handler->display->display_options['relationships']['reverse_field_environment_id_node']['table'] = 'node';
  $handler->display->display_options['relationships']['reverse_field_environment_id_node']['field'] = 'reverse_field_environment_id_node';
  $handler->display->display_options['relationships']['reverse_field_environment_id_node']['relationship'] = 'reverse_field_project_node';
  $handler->display->display_options['relationships']['reverse_field_environment_id_node']['ui_name'] = 'Deploys referencing environment';
  $handler->display->display_options['relationships']['reverse_field_environment_id_node']['label'] = 'Content referencing Content from field_environment_id (Deploys)';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Deploy';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Environment title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'node';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['relationship'] = 'field_environment_id_target_id';
  $handler->display->display_options['fields']['title_1']['ui_name'] = 'Environment title';
  $handler->display->display_options['fields']['title_1']['label'] = 'Environment';
  $handler->display->display_options['fields']['title_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title_1']['alter']['max_length'] = '40';
  $handler->display->display_options['fields']['title_1']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title_1']['alter']['trim'] = TRUE;
  /* Field: Project title */
  $handler->display->display_options['fields']['title_2']['id'] = 'title_2';
  $handler->display->display_options['fields']['title_2']['table'] = 'node';
  $handler->display->display_options['fields']['title_2']['field'] = 'title';
  $handler->display->display_options['fields']['title_2']['relationship'] = 'field_project_target_id';
  $handler->display->display_options['fields']['title_2']['ui_name'] = 'Project title';
  $handler->display->display_options['fields']['title_2']['label'] = 'Project';
  $handler->display->display_options['fields']['title_2']['exclude'] = TRUE;
  /* Field: Client title */
  $handler->display->display_options['fields']['title_3']['id'] = 'title_3';
  $handler->display->display_options['fields']['title_3']['table'] = 'node';
  $handler->display->display_options['fields']['title_3']['field'] = 'title';
  $handler->display->display_options['fields']['title_3']['relationship'] = 'field_client_target_id';
  $handler->display->display_options['fields']['title_3']['ui_name'] = 'Client title';
  $handler->display->display_options['fields']['title_3']['label'] = 'Klant';
  $handler->display->display_options['fields']['title_3']['exclude'] = TRUE;
  /* Field: Content: Updated date */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'node';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['label'] = 'Date';
  $handler->display->display_options['fields']['changed']['exclude'] = TRUE;
  $handler->display->display_options['fields']['changed']['date_format'] = 'long';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Project (field_project) */
  $handler->display->display_options['arguments']['field_project_target_id']['id'] = 'field_project_target_id';
  $handler->display->display_options['arguments']['field_project_target_id']['table'] = 'field_data_field_project';
  $handler->display->display_options['arguments']['field_project_target_id']['field'] = 'field_project_target_id';
  $handler->display->display_options['arguments']['field_project_target_id']['relationship'] = 'reverse_field_project_node';
  $handler->display->display_options['arguments']['field_project_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_project_target_id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['field_project_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_project_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_project_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'deploy' => 'deploy',
  );
  /* Filter criterion: Content: Deploystatus (field_deploystatus) */
  $handler->display->display_options['filters']['field_deploystatus_value']['id'] = 'field_deploystatus_value';
  $handler->display->display_options['filters']['field_deploystatus_value']['table'] = 'field_data_field_deploystatus';
  $handler->display->display_options['filters']['field_deploystatus_value']['field'] = 'field_deploystatus_value';
  $handler->display->display_options['filters']['field_deploystatus_value']['value']['min'] = '0.1';
  $handler->display->display_options['filters']['field_deploystatus_value']['value']['value'] = '1';
  $handler->display->display_options['block_description'] = 'Deploy history';

  $export['history_of_deploys'] = $view;
}
