(function ($, Drupal, window, document, undefined) {

    var debugOn = 0;
    var oldData = '';

    // Add extra functionality.
    Drupal.behaviors.getAjaxLog = {
        attach: function (context, settings) {
            var nid = Drupal.settings.deployer.deploy_id;
            var container = $('pre#deploy-log');

            // Add 'show/hide debug' button.
            $('.field-name-field-deployer-log').children(":first").before('<input class="show_anyway" type="submit" id="show_debug" value="Show debug"/>');
            $('input#show_debug').bind('click', function () {
                if (debugOn == 1) {
                    $(this).val('Show debug');
                    debugOn = 0;

                    // Filter debug lines.
                    oldData = $(container).html();
                    data = filterDebug(oldData);

                    // Insert data.
                    $(container).html(data);
                }
                else {
                    $(this).val('Hide debug');
                    debugOn = 1;

                    // Show unfiltered data.
                    $(container).html(oldData);
                }
            });

            updateLogContentFromCallback(nid, container);
        }
    };

    // Filter debug- and newlines.
    function filterDebug(data) {
        if (debugOn == 0) {
            data = data.replace(/^.*DEBUG.*\n/gm,"");
            data = data.replace(/^\n/gm,"");
            return data;
        }
        else {
            return data;
        }
    }

    function updateLogContentFromCallback(nid, container) {
        $.ajax({
            url: '/ajax/getlog/' + nid,
            success:function(data){
                if (data == 'FINISHED'){
                    location.reload(true);
                }
                else if(data != 'FALSE'){
                    var scroll;

                    var scroll = ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) ? 1 : 0;

                    // Filter debug lines.
                    oldData = data;
                    data = filterDebug(data);

                    // Insert data.
                    $(container).html(data);

                    console.log( scroll === true, scroll == true );
                    if (scroll == 1) {
                        // you're at the bottom of the page
                        window.scrollTo(0, document.body.scrollHeight);
                    }

                    // Start new update in 3000ms
                    setTimeout(function() { updateLogContentFromCallback(nid, container) }, 1000);
                }
            }
        });
    }

})(jQuery, Drupal, this, this.document);