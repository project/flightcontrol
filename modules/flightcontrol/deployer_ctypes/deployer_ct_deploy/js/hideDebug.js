(function ($, Drupal, window, document, undefined) {

    var debugOn = 0;
    var oldData = '';

    // Add extra functionality.
    Drupal.behaviors.hideDebug = {
        attach: function (context, settings) {
            var container = $('pre#deploy-log');

            // Save old data, filter debug.
            oldData = $(container).html();
            data = filterDebug(oldData);
            // Insert data.
            $(container).html(data);

            // Add 'show/hide debug' button.
            $('.field-name-field-deployer-log').children(":first").before('<input class="show_anyway" type="submit" id="show_debug" value="Show debug"/>');
            $('input#show_debug').bind('click', function () {
                if (debugOn == 1) {

                    $(this).val('Show debug');
                    debugOn = 0;

                    // Filter debug lines.
                    oldData = $(container).html();
                    data = filterDebug(oldData);

                    // Insert data.
                    $(container).html(data);
                }
                else {
                    $(this).val('Hide debug');
                    debugOn = 1;

                    // Show unfiltered data.
                    $(container).html(oldData);
                }
            });
        }
    };

    // Filter debug- and newlines.
    function filterDebug(data) {
        if (debugOn == 0) {
            data = data.replace(/^.*DEBUG.*\n/gm,"");
            data = data.replace(/^\n/gm,"");
            return data;
        }
        else {
            return data;
        }
    }

})(jQuery, Drupal, this, this.document);