<?php
/**
 * @file This file describes the hooks that are defined by this module.
 */

/**
 * Hook to set variable configurations.
 */
function hook_deployer_variable_configs() {
  $export = array();
  // Publish node.
  $export['node_options_client'] = array(
    0 => 'status',
  );
  return $export;
}
