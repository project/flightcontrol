<?php
/**
 * @file
 * Updates Drush versions, used to make remote executions.
 */

const DEPLOYER_DRUSH_GIT_RELEASES_URL = 'https://api.github.com/repos/drush-ops/drush/releases';
const DEPLOYER_DRUSH_MAJOR_VERSION = '8';

/**
 * Get available drush versions from packagist.
 *
 * @return array|bool
 *   Array containing versions within major
 *   version DEPLOYER_DRUSH_MAJOR_VERSION, or FALSE on failure.
 */
function deployer_drush_manager_fetch_drush_versions() {
  $request = drupal_http_request(DEPLOYER_DRUSH_GIT_RELEASES_URL);
  if ($request->code == 200) {
    $data = json_decode($request->data, TRUE);

    $valid_versions = array();
    foreach ($data as $version_info) {
      $version = $version_info['tag_name'];
      if (strpos($version, DEPLOYER_DRUSH_MAJOR_VERSION) === 0) {
        $valid_versions[$version] = $version_info;
      }
    }

    uksort($valid_versions, 'version_compare');
    $valid_versions = array_reverse($valid_versions);
    return $valid_versions;
  }

  return FALSE;
}

/**
 * Download drush version.
 *
 * @param array $version_info
 *   Version-info array from packagist.
 *
 * @return bool
 *   TRUE on success, FALSE on failure.
 */
function deployer_drush_manager_download($version_info) {
  $drush_base_dir = variable_get('deployer_drush_versions_path', '');
  if (!empty($drush_base_dir)) {
    $drush_base_dir = drupal_realpath($drush_base_dir);

    // Get URL / version from version info.
    $version = $version_info['tag_name'];
    if (isset($version_info['assets'])) {

      foreach ($version_info['assets'] as $asset) {
        if ($asset['name'] == 'drush.phar' && isset($asset['browser_download_url'])) {
          $url = $asset['browser_download_url'];
          break;
        }
      }

      if (isset($url)) {
        // See if drush basefolder exists.
        // Create if not.
        $drush_dir = $drush_base_dir . '/' . $version;
        $drush = $drush_dir . '/drush';
        if (!is_dir($drush_base_dir)) {
          if (!mkdir($drush_base_dir, 0755, TRUE)) {
            return FALSE;
          }
        }

        // Fetch.
        $request = drupal_http_request($url);
        if ($request->code == 200) {
          // Put in temp file.
          $temp_file = 'temporary://' . uniqid() . '.drush';
          if (file_unmanaged_save_data($request->data, $temp_file)) {
            // Remove old drush version if any.
            if (is_dir($drush_dir)) {
              exec('rm -rf "' . $drush_dir . '"');
            }
            mkdir($drush_dir, 0755);
            rename(drupal_realpath($temp_file), $drush);
            chmod($drush, 0755);
          }
        }
      }
    }
  }

  // OK ?
  if (isset($drush_dir) && file_exists($drush_dir . '/drush')) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Fetch latest drush version.
 *
 * @return bool
 *   TRUE: ok, FALSE on failure.
 */
function deployer_drush_manager_fetch_latest() {
  if ($versions = deployer_drush_manager_fetch_drush_versions()) {
    $latest = reset($versions);
    if (deployer_drush_manager_download($latest)) {
      return TRUE;
    }
  }

  drupal_set_message(t('Unable to download latest Drush version.'), 'error');
  return FALSE;
}
