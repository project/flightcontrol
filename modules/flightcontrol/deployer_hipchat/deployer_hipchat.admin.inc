<?php

/**
 * @file
 * The administrative function for the deployer_profit_counter module.
 */

/**
 * Admin form used to congure the deployer_profit_counter module.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array
 *
 * @return array
 *   A renderable array of the form.
 */
function deployer_hipchat_admin($form, &$form_state) {
  $form['deployer_hipchat_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Hipchat API key'),
    '#default_value' => variable_get('deployer_hipchat_key', ''),
    '#required' => TRUE,
  );

  $form['deployer_hipchat_room_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Hipchat room id or name'),
    '#default_value' => variable_get('deployer_hipchat_room_id', ''),
    '#required' => TRUE,
  );

  $form['test_link'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Send hipchat test message'), 'admin/deployer/hipchat/test'),
  );

  return system_settings_form($form);
}
