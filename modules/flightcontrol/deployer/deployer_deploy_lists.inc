<?php
/**
 * @file
 * Deploy lists for restoring backups etc.
 */

/**
 * Provide deploys in a list overview with backups attached.
 *
 * @param object $field
 *   Contains node.
 *   If provided, this works as filter.
 *
 * @return array
 *   Build array.
 */
function deployer_list_snapshots($field = NULL) {
  $header = array(
    array('data' => t('Date'), 'field' => 'changed', 'sort' => 'desc'),
  );

  $build = array();
  $query = db_select('node', 'n');

  // Deploystatus, db-backup, webroot-backup, backup-status.
  $query->join('field_data_field_deploystatus', 'dstat', 'n.nid = dstat.entity_id');
  $query->join('field_data_field_backup_database', 'backup_db', 'n.nid = backup_db.entity_id');
  $query->join('field_data_field_backup_webroot', 'backup_webroot', 'n.nid = backup_db.entity_id');
  $query->join('field_data_field_backups_ok', 'backups_ok', 'n.nid = backups_ok.entity_id');

  // Environment referenced from deploy.
  $query->join('field_data_field_environment_id', 'env', 'n.nid = env.entity_id');
  $query->join('node', 'env_node', 'env_node.nid = env.field_environment_id_target_id');

  // Project referenced from environment.
  $query->join('field_data_field_project', 'project', 'project.entity_id = env_node.nid');
  $query->join('node', 'project_node', 'project_node.nid = project.field_project_target_id');

  // Client referenced from project.
  $query->join('field_data_field_client', 'client', 'client.entity_id = project_node.nid');
  $query->join('node', 'client_node', 'client_node.nid = client.field_client_target_id');

  // Node type: deploy.
  // Deploystatus: 1.
  // Webroot OR DB backup attached.
  // Backup status: 1.
  $query->condition('n.type', 'deploy');

  // Filter for specific project if provided.
  if (!empty($field)) {
    $node = $field['entity'];
    if ($node->type == 'project') {
      $project_id = $node->nid;
    }
    elseif ($node->type == 'environment') {
      $proj_field = field_get_items('node', $node, 'field_project');
      $project_id = $proj_field[0]['target_id'];
    }

    if (isset($project_id)) {
      $query->fields('project', array('field_project_target_id'));
      $query->condition('project.field_project_target_id', $project_id);
    }
  }

  $query->condition('dstat.field_deploystatus_value', 1, '>=');
  $or_cond = db_or()->condition('backup_webroot.field_backup_webroot_value', 1)->condition('backup_db.field_backup_database_value', 1);
  $query->condition($or_cond);
  $query->condition('backups_ok.field_backups_ok_value', 1);

  $query->fields('n', array('nid', 'changed', 'title'));
  $query->fields('backup_db', array('field_backup_database_value'));
  $query->fields('backup_webroot', array('field_backup_webroot_value'));
  $query->fields('env_node', array('nid', 'title'));
  $query->fields('client_node', array('nid', 'title'));
  $query->fields('dstat', array('field_deploystatus_value'));

  // Group by deploy-nid.
  $query->groupBy('n.nid');

  // Add pager.
  $query = $query->extend('PagerDefault');
  // Page limit.
  $query->limit(10);

  // Add table sort extender.
  $query = $query->extend('TableSort');
  // Add order by headers.
  $query->orderByHeader($header);

  $results = $query->execute();

  // Define tabular data.
  $variables['header'] = array(
    'Deploy',
    array(
      'data' => 'Environment',
      'field' => 'env_node_title',
    ),
    'OTAP',
    array(
      'data' => 'Client',
      'field' => 'client_node_title',
    ),
    'Tag',
    array(
      'data' => 'Date',
      'field' => 'changed',
      'sort' => 'desc',
    ),
    'Deploy status',
    'Database / Webroot',
    'Restore',
    'Download',
  );
  $variables['attributes'] = array();
  $variables['caption'] = NULL;
  $variables['colgroups'] = array();
  $variables['sticky'] = NULL;
  $variables['empty'] = 'No results';
  $variables['rows'] = array();

  // Process results.
  foreach ($results as $row) {

    $what_in_backup = array();
    $tag = '';

    // Look up meta file, created at time of deploy.
    // This has information on drupal sites, databases and webroots.
    $file = drupal_realpath('private://deploy/backups/' . $row->nid . '.meta');
    if (file_exists($file)) {
      $content = file_get_contents($file);
      $backup_meta = drupal_json_decode($content);

      // Webroot backups.
      if (isset($backup_meta[0]['webroot_backup']) && !empty($backup_meta[0]['webroot_backup'])) {
        $file = drupal_realpath($backup_meta[0]['webroot_backup']);
        if (file_exists($file)) {
          $what_in_backup[] = 'webroot';
        }
      }

      if (isset($backup_meta[0]['tag_str']) && !empty($backup_meta[0]['tag_str'])) {
        $tag = _deployer_interpret_deployed_tag($backup_meta[0]['tag_str']);
        $tag = l(truncate_utf8($tag['tag'], 15, FALSE, '...'), $tag['url']);
      }
    }

    // Database backups.
    if (isset($backup_meta[0]['database_backup'][0]) && !empty($backup_meta[0]['database_backup'][0])) {
      $file = drupal_realpath($backup_meta[0]['database_backup'][0]['localfile']);
      if (file_exists($file)) {
        $what_in_backup[] = 'database';
      }
    }

    // OTAP type.
    $otap_readable = '';
    $env = node_load($row->env_node_nid);
    if ($env) {
      $otap = field_get_items('node', $env, 'field_server_type');
      if (isset($otap[0]['value'])) {
        $otap_value = $otap[0]['value'];
        $otap_available_values = array(
          0 => 'D',
          1 => 'T',
          2 => 'A',
          3 => 'P',
        );
        $otap_readable = isset($otap_available_values[$otap_value]) ? $otap_available_values[$otap_value] : '';
      }
    }

    // Rest of results.
    // Only list snapshot, if there's something to restore.
    if (!empty($what_in_backup)) {
      $variables['rows'][] = array(
        l(truncate_utf8($row->title, 15, FALSE, '...'), 'node/' . $row->nid),
        l(truncate_utf8($row->env_node_title, 8, FALSE, '...'), 'node/' . $row->env_node_nid),
        $otap_readable,
        l(truncate_utf8($row->client_node_title, 15, FALSE, '...'), 'node/' . $row->client_node_nid),
        $tag,
        format_date($row->changed, 'short'),
        _deployer_interpret_deploystatus($row->field_deploystatus_value),
        implode(', ', $what_in_backup),
        l(t('Restore'), 'node/add/deploy', array('query' => array('id' => $row->nid, 'restore' => 1))),
        l(t('Download'), 'snapshots/download', array('query' => array('id' => $row->nid))),
      );
    }
  }

  // Add the content.
  $build['content'] = array('#markup' => theme_table($variables));
  // Add the pager.
  $build['pager'] = array(
    '#theme' => 'pager',
    '#weight' => 5,
  );

  if (isset($project_id)) {
    // Return rendered array for DS field.
    $build['#prefix'] = theme('html_tag', array(
      'element' => array(
        '#tag' => 'h2',
        '#value' => t('Snapshots for this project'),
      ),
    ));

    return drupal_render($build);
  }
  else {

    $output = drupal_render($build);

    // Wrapper.
    $output = theme_html_tag(array(
      'element' => array(
        '#tag' => 'div',
        '#attributes' => array(
          'class' => 'list-snapshots',
        ),
        '#value' => $output,
      ),
    ));

    // Return the renderable array.
    $ret_build = array(
      'content' => array(
        '#markup' => $output,
      ),
    );
    return $ret_build;
  }
}

/**
 * Show downloadable content.
 *
 * Callback function for presenting downloadable archives for  a deploy.
 *
 * @return array
 *   Renderable build array.
 */
function deployer_download_snapshot() {

  if (isset($_GET['id']) && !empty($_GET['id']) && is_numeric($_GET['id']) && $node = node_load($_GET['id'])) {

    // Look up meta file, created at time of deploy.
    // This has information on drupal sites, databases and webroots.
    $file = drupal_realpath('private://deploy/backups/' . $_GET['id'] . '.meta');
    if (file_exists($file)) {
      $content = file_get_contents($file);
      $backup_meta = drupal_json_decode($content);

      $variables['title'] = 'Files';
      $variables['type'] = 'ul';
      $variables['attributes'] = array();

      $variables['items'] = array();

      // Webroot backups.
      if (isset($backup_meta[0]['webroot_backup']) && !empty($backup_meta[0]['webroot_backup'])) {
        $file = drupal_realpath($backup_meta[0]['webroot_backup']);
        if (file_exists($file)) {
          // Create file entity if file is unmanaged.
          $filesize = filesize(drupal_realpath($backup_meta[0]['webroot_backup']));
          $modtime = filemtime(drupal_realpath($backup_meta[0]['webroot_backup']));
          _deployer_create_save_file_entity($backup_meta[0]['webroot_backup']);
          $variables['items'][0]['data'] = t('Webroot');
          $variables['items'][0]['children'][] = t('File: ') . l(basename($backup_meta[0]['webroot_backup']), file_create_url($backup_meta[0]['webroot_backup']));

          if (isset($backup_meta[0]['tag_str']) && !empty($backup_meta[0]['tag_str'])) {
            $tag = _deployer_interpret_deployed_tag($backup_meta[0]['tag_str']);
            $variables['items'][0]['children'][] = t('Tag / commit: ') . l($tag['tag'], $tag['url']);
          }

          $variables['items'][0]['children'][] = t('Size: ') . format_size($filesize);
          $variables['items'][0]['children'][] = t('Date: ') . format_date($modtime);
        }
        else {
          $variables['items'][0] = 'Webroot (not found on disk)';
        }
      }

      // Database backups.
      if (isset($backup_meta[0]['database_backup'][0]) && !empty($backup_meta[0]['database_backup'][0])) {
        $item_count = 1;
        foreach ($backup_meta[0]['database_backup'] as $db) {
          $file = drupal_realpath($db['localfile']);
          if (file_exists($file)) {
            // Create file entity if file is unmanaged.
            $filesize = filesize(drupal_realpath($db['localfile']));
            $modtime = filemtime(drupal_realpath($db['localfile']));
            _deployer_create_save_file_entity($db['localfile']);
            $variables['items'][$item_count]['data'] = t('Database');
            $variables['items'][$item_count]['children'][] = t('File: ') . l(basename($db['localfile']), file_create_url($db['localfile']));
            $variables['items'][$item_count]['children'][] = t('Site: ') . $db['site'];
            $variables['items'][$item_count]['children'][] = t('Size: ') . format_size($filesize);
            $variables['items'][$item_count]['children'][] = t('Date: ') . format_date($modtime);
          }
          else {
            $variables['items'][$item_count] = 'Database [' . $db['site'] . '] (not found on disk)';
          }
          $item_count++;
        }
      }

      if (!empty($variables['items'])) {
        $build['content'] = array('#markup' => '<fieldset>' . theme_item_list($variables) . '</fieldset>');
      }
      else {
        $build['content'] = array('#markup' => 'Backupfile contains no proper data.');
      }
    }
    else {
      $build['content'] = array('#markup' => 'No backup-files found on disk.');
    }

    // Set prefix with snapshot information.
    $build['content']['#prefix'] = '<fieldset>' . deployer_show_deploy_info($_GET['id'], t('Snapshot information')) . '</fieldset>';

    return $build;
  }
  else {
    drupal_not_found();
  }
}

/**
 * Function to return html with deploy information.
 *
 * Only usable for deploys which are finished.
 *
 * @param int $id
 *   Deploy node id.
 *
 * @param string $title
 *   Optional title for item list.
 *
 * @return string
 *   html for theme_item_list.
 */
function deployer_show_deploy_info($id, $title = NULL) {
  // Set prefix with snapshot information.
  $deploynode = node_load($id);

  if ($title == NULL) {
    $variables_prefix['title'] = t('Basic deploy information');
  }
  else {
    $variables_prefix['title'] = $title;
  }

  $variables_prefix['type'] = 'ul';
  $variables_prefix['attributes'] = array();

  $variables_prefix['items'] = array();
  $variables_prefix['items'][] = t('Deploy:') . ' ' . l($deploynode->title, 'node/' . $deploynode->nid);
  $variables_prefix['items'][] = t('Date:') . ' ' . format_date($deploynode->changed);

  $backups_ok = field_get_items('node', $deploynode, 'field_backups_ok');
  $backups_ok = $backups_ok[0]['value'];

  if ($backups_ok) {
    $variables_prefix['items'][] = l(t('Restore'), 'node/add/deploy', array('query' => array('id' => $deploynode->nid, 'restore' => 1)));
    $variables_prefix['items'][] = l(t('Download'), 'snapshots/download', array('query' => array('id' => $deploynode->nid)));
  }
  return theme_item_list($variables_prefix);
}
