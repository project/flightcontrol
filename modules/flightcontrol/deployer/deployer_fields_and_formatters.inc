<?php
/**
 * @file
 * Code project field / formatter.
 * Deploystatus formatter.
 * Contextual link block (add project, add env etc).
 */

/**
 * Function to provide repos as options.
 *
 * Use hook_deployer_code_repo_option_provider();
 * to provide option-providers for code_repo field.
 * Expected return value: array(
 *   'type' => 'option_provider',
 * )
 * type: for example 'github' or 'company-subversion'
 * option_provider: function name providing options for this type,
 *
 * @param array $field
 *   Field from content form.
 *
 * @return array
 *   Options.
 */
function deployer_options_list($field) {
  $option_providers = module_invoke_all('deployer_code_repo_option_provider');
  foreach ($option_providers as $type => $provider) {
    if (function_exists($provider)) {
      if ($result = call_user_func_array($provider, array())) {
        foreach ($result as $option) {
          $options[$type . '|' . $option] = $option . ' (' . $type . ')';
        }
      }
    }
  }

  if (empty($options)) {
    drupal_set_message(t('No repositories found. Maybe you did not configure a connector for GitHub, Stash, Gitlab etc ?<br/>
        This message might occur in case of invalid SSL handshakes with git, see !gitconfig.<br/>
        Also, check !dblog for messages.', array(
          '!dblog' => l(t('dblog'), 'admin/reports/dblog'),
          '!gitconfig' => l(t('Git API settings'), 'admin/deployer/versioncontrol/git_api'),
        )));
    return array();
  }

  ksort($options, SORT_FLAG_CASE | SORT_STRING);
  return $options;
}

/**
 * Implements hook_field_widget_info_alter().
 */
function deployer_field_widget_info_alter(&$info) {
  $widgets = array(
    'options_select' => array('deployer_code_project'),
  );
  foreach ($widgets as $widget => $field_types) {
    $info[$widget]['field types'] = array_merge($info[$widget]['field types'], $field_types);
  }
}


/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
//function deployer_field_info() {
//  return array(
//    // Code repository field.
//    'deployer_code_project' => array(
//      'label' => t('Deployer code project'),
//      'description' => t('Code project / repository for customer.'),
//      'settings' => array('max_length' => 255),
//      'instance_settings' => array(
//        'text_processing' => 0,
//      ),
//      'default_widget' => 'options_select',
//      'default_formatter' => 'deployer_simple_text',
//    ),
//  );
//}

/**
 * Implements hook_field_is_empty().
 */
function deployer_field_is_empty($item, $field) {
  return empty($item['code_project']);
}

/**
 * Implements hook_field_formatter_info().
 */
function deployer_field_formatter_info() {
  return array(
    'deployer_status_formatter' => array(
      'label' => t('Deploy status'),
      'field types' => array('number_decimal'),
    ),
    'deployer_healt_status_formatter' => array(
      'label' => t('Health status interpreter'),
      'field types' => array('text_long'),
      'settings' => array(
        'all' => 'All health items',
      ),
    ),
    'deployer_healt_status_age' => array(
      'label' => t('Heath age interpreter'),
      'field types' => array('number_integer'),
    ),
    'deployer_repo_field_link' => array(
      'label' => t('Convert repo to link'),
      'field types' => array('text'),
      'settings' => array(
        'format' => 'full',
      ),
    ),
    'deployer_deploy_type' => array(
      'label' => t('Convert hash_field to deploy type'),
      'field types' => array('text'),
    ),
    'deployer_first_n_characters' => array(
      'label' => t('Only show first N characters'),
      'field types' => array('text'),
    ),
    'deployer_first_n_characters_list' => array(
      'label' => t('Only show first N characters'),
      'field types' => array('list_integer'),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function deployer_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  if ($field['type'] == 'deployer_code_project') {
    $element = array();
    $element['format'] = array(
      '#type'           => 'select',
      '#title'          => t('Link format'),
      '#description'    => t('Select the link format.'),
      '#default_value'  => $settings['format'],
      '#options'        => array(
        'short'  => 'Short - link only',
        'full' => 'Full - "Visit on github: [repo]"',
      ),
    );
    return $element;
  }

  elseif ($field['type'] == 'text_long') {
    $element = array();
    $element['format'] = array(
      '#type'           => 'checkboxes',
      '#title'          => t('Health status format'),
      '#description'    => t('Select the status format.'),
      '#default_value'  => $settings['format'],
      '#options'        => array(
        'all' => 'All health items',
        'alive' => 'Alive status',
        'updates' => 'Update status',
        'version' => 'Deployed version running',
      ),
    );
    return $element;
  }

  elseif ($display['type'] == 'deployer_first_n_characters' || $display['type'] == 'deployer_first_n_characters_list') {
    $element = array(
      'number' => array(
        '#type' => 'textfield',
        '#description' => t('Provide number of characters to show, from beginning of the field'),
        '#default_value' => $settings['number'],
      ),
    );
    return $element;
  }
  return FALSE;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function deployer_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = t('Showing specific items');
  return $summary;
}


/**
 * Implements hook_field_formatter_view().
 */
function deployer_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    // This formatter simply outputs the field as text and with a color.
    case 'deployer_simple_text':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          // We create a render array to produce the desired markup,
          // "<p style="color: #hexcolor">The color code ... #hexcolor</p>".
          // See theme_html_tag().
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $item['value'],
        );
      }
      break;

    case 'deployer_status_formatter':
      foreach ($items as $delta => $item) {

        $item['value'] = _deployer_interpret_deploystatus($item['value']);
        $element[$delta] = array(
          '#markup' => $item['value'],
        );
      }
      break;

    case 'deployer_healt_status_formatter':
      foreach ($items as $delta => $item) {
        $settings = $display['settings'];
        if (!isset($settings['format'])) {
          $format = array('all' => 1);
        }
        else {
          $format = $settings['format'];
        }
        $item['value'] = _deployer_interpret_health_status($item['value'], $format);
        $element[$delta] = array(
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $item['value'],
        );
      }
      break;

    case 'deployer_healt_status_age':
      foreach ($items as $delta => $item) {

        $item['value'] = _deployer_interpret_health_age($item['value']);
        $element[$delta] = array(
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $item['value'],
        );
      }
      break;

    case 'deployer_repo_field_link':
      foreach ($items as $delta => $item) {

        $settings = $display['settings'];
        $format = $settings['format'];

        if (isset($item['value'])) {
          $item['value'] = deployer_repo_link($item['value'], $format);
          $element[$delta] = array(
            '#type' => 'html_tag',
            '#tag' => 'span',
            '#value' => $item['value'],
          );
        }
      }
      break;

    case 'deployer_deploy_type':
      foreach ($items as $delta => $item) {
        $version = _deployer_interpret_deployed_tag($item['value']);
        switch ($version['tag_type']) {
          case 'hash':
            $val = 'deploy';
            break;

          case 'tag':
            $val = 'deploy';
            break;

          case 'backup':
            $val = 'backup';
            break;

          case 'restore':
            $val = 'restore';
            break;

          default:
            $val = '';
            break;
        }
        $element[$delta] = array(
          '#markup' => $val,
        );
      }
      break;

    case 'deployer_deploy_tag':
      foreach ($items as $delta => $item) {
        $version = _deployer_interpret_deployed_tag($item['value']);
        switch ($version['tag_type']) {
          case 'hash':
            $val = $version['tag'];
            break;

          case 'tag':
            $val = $version['tag'];
            break;

          default:
            $val = '';
            break;
        }
        $element[$delta] = array(
          '#markup' => $val,
        );
      }
      break;

    case 'deployer_first_n_characters':
      foreach ($items as $delta => $item) {
        // Trim length.
        $output = $item['value'];
        $settings = $display['settings'];
        $number = $settings['number'];
        $output = (strlen($output) > $number) ? substr($output, 0, $number) : $output;
        $element[$delta] = array('#markup' => $output);
      }
      break;

    case 'deployer_first_n_characters_list':
      $allowed_values = list_allowed_values($field, $instance, $entity_type, $entity);
      foreach ($items as $delta => $item) {
        if (isset($allowed_values[$item['value']])) {
          $output = field_filter_xss($allowed_values[$item['value']]);
        }
        else {
          // If no match was found in allowed values, fall back to the key.
          $output = field_filter_xss($item['value']);
        }

        // Trim length.
        $settings = $display['settings'];
        $number = $settings['number'];
        $output = (strlen($output) > $number) ? substr($output, 0, $number) : $output;
        $element[$delta] = array('#markup' => $output);
      }
      break;
  }
  return $element;
}

/**
 * Implements hook_ds_fields_info().
 *
 * Add display suite field for server health info.
 */
function deployer_ds_fields_info($entity_type) {
  $fc_fields = array();
  $node_fields = array();

  $fc_fields['field_deployer_server_health'] = array(
    'title' => t('Server health information'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'ui_limit' => array(
      '*|*',
    ),
    'function' => 'deployer_show_server_health_full',
  );
  $node_fields['field_deployer_show_snapshots'] = array(
    'title' => t('List snapshots'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'ui_limit' => array(
      '*|*',
    ),
    'function' => 'deployer_list_snapshots',
  );
  $node_fields['field_deployer_show_backup_link'] = array(
    'title' => t('Backup link'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'ui_limit' => array(
      '*|*',
    ),
    'function' => 'deployer_env_backup_link',
  );
  $node_fields['field_deployer_show_deploy_link'] = array(
    'title' => t('Deploy link'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'ui_limit' => array(
      '*|*',
    ),
    'function' => 'deployer_env_deploy_link',
  );
  $node_fields['field_deployer_client_connected_projects'] = array(
    'title' => t('Connected projects'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'ui_limit' => array(
      '*|*',
    ),
    'function' => 'deployer_client_connected_projects',
  );

  return array(
    'field_collection_item' => $fc_fields,
    'node' => $node_fields,
  );

}

/**
 * Generate contextual info for env, project, client.
 *
 * @param mixed $node
 *   Node object.
 *   If empty, it's fetched from the menu router.
 *
 * @return string
 *   HTML.
 */
function _deployer_contextual_info_block_content($node = NULL) {
  if (is_null($node)) {
    $node = menu_get_object('node');
  }

  if (!$node) {
    // Snapshot download.
    if (arg(0) == 'snapshots' && arg(1) == 'download') {
      if (isset($_GET['id']) && is_numeric($_GET['id'])) {
        $node = node_load($_GET['id']);
      }
    }
    // Snapshot restore or backup.
    elseif (arg(0) == 'node' && arg(1) == 'add' && arg(2) == 'deploy') {
      if (isset($_GET['id']) && is_numeric($_GET['id'])) {
        $node = node_load($_GET['id']);
      }
      elseif (isset($_GET['env']) && is_numeric($_GET['env'])) {
        $node = node_load($_GET['env']);
      }
    }
  }

  if ($node) {
    $tree = _deployer_get_node_context_tree($node);

    // Compose markup for context links.
    $markup = _deployer_contextual_tree_render($tree);

    // Flag link.
    if (function_exists('flag_create_link')) {
      $markup .= flag_create_link('flightcontrol_bookmarks', $node->nid);
    }

    // Add flag link, if any.
    if (function_exists('flag_create_link')) {
      $flag_link = flag_create_link('bookmarks', $node->nid);
      if ($flag_link) {
        $markup .= PHP_EOL . '<div class="flag">' . $flag_link . '</div>';
      }
    }

    // Render.
    if (!empty($markup)) {
      return theme('html_tag', array(
        'element' => array(
          '#tag' => 'div',
          '#attributes' => array('class' => 'deployer-context'),
          '#value' => $markup,
        ),
      ));
    }
  }
  return '';
}

/**
 * Provide dashboard management links.
 *
 * Links are contextual.
 *
 * @return string
 *   HTML item list.
 */
function _deployer_manage_links_block_content() {

  $arg[0] = arg(0);
  $arg[1] = arg(1);

  $links = array();
  $links[] = l(t('Add client'), 'node/add/client');

  // Change links wrt context.
  if (!empty($arg[0]) && $arg[0] == 'node' && !empty($arg[1]) && is_numeric($arg[1]) && $node = node_load($arg[1])) {
    switch ($node->type) {
      case 'project':
        $add_env_opts = array('query' => array('project' => $node->nid));
        $links[] = l(t('Add environment'), 'node/add/environment', $add_env_opts);
        break;

      case 'client':
        $add_project_opts = array('query' => array('client' => $node->nid));
        $links[] = l(t('Add project'), 'node/add/project', $add_project_opts);
        break;

    }
  }

  $items['title'] = '';
  $items['type'] = 'ul';
  $items['attributes'] = array();
  $items['items'] = $links;

  return theme_item_list($items);
}

/**
 * Render current notifications.
 *
 * @return bool|string
 *   Markup or FALSE.
 */
function _deployer_show_status_notifications() {
  $messages = variable_get('deployer_status_messages', array());
  $messages_unset = FALSE;

  $show_messages = array();
  if (!empty($messages)) {
    foreach ($messages as $key => $message) {
      // Message still valid to show ?
      if ($message['timestamp'] > (time() - (DEPLOYER_STATUS_MESSAGE_TTL * 60))) {

        // Get contextual data (project / client).
        $project_link = '';
        $client_link = '';
        $node = node_load($message['nid']);
        if ($node) {
          // Get project and client, if any.
          $project = field_get_items('node', $node, 'field_project');
          if (!empty($project)) {
            $project = node_load($project[0]['target_id']);
            $project_link = l($project->title, 'node/' . $project->nid);

            if ($project) {
              $client = field_get_items('node', $project, 'field_client');
              if (!empty($client)) {
                $client = node_load($client[0]['target_id']);
                $client_link = l($client->title, 'node/' . $client->nid);
              }
            }
          }

          // Prepare data to render.
          $status_map = deployer_status_message_map($message['status']);

          // Render proper message.
          if (!empty($message['message'])) {
            $msg = $message['message'];
          }
          else {
            $msg = $status_map['message'];
          }

          // Add renderable message to array.
          $show_messages[] = array(
            'class' => $status_map['class'],
            'link' => l($node->title, 'node/' . $message['nid']),
            'message' => $msg,
            'project' => $project_link,
            'client' => $client_link,
          );
        }
      }
      else {
        unset($messages[$key]);
        $messages_unset = TRUE;
      }
    }

    // Clean up old messages if needed.
    if ($messages_unset) {
      variable_set('deployer_status_messages', $messages);
    }

    // Render messages.
    if (!empty($show_messages)) {
      return theme('deployer_status_message', array('messages' => $show_messages));
    }
  }

  return FALSE;
}

/**
 * Render bookmarks block.
 */
function _deployer_bookmarks_block() {
  $markup = '';

  global $user;
  if ($user->uid) {
    // Authenticated user.
    if (function_exists('flag_get_user_flags')) {
      $flagged = flag_get_user_flags('node');
      if (isset($flagged['flightcontrol_bookmarks'])) {
        $nids = array_keys($flagged['flightcontrol_bookmarks']);
        $nodes = node_load_multiple($nids);

        $results_tree = array();
        foreach ($nodes as $node) {
          // Get contextual tree.
          if ($contextual_tree = _deployer_get_node_context_tree($node)) {
            $results_tree = _deployer_merge_recursive_children($results_tree, $contextual_tree);
          }
        }

        $markup = _deployer_contextual_tree_render($results_tree);
      }
    }
  }

  return $markup;
}

/**
 * Deep-merge arrays while keeping numerical keys.
 *
 * @param array $main_tree
 *   Main array to merge into.
 * @param array $extra_tree
 *   Array to merge into main array.
 *
 * @return array
 *   Main array.
 */
function _deployer_merge_recursive_children($main_tree, $extra_tree) {
  foreach ($extra_tree as $key => $item) {
    if (!isset($main_tree[$key])) {
      $main_tree[$key] = $item;
    }
    elseif (is_array($main_tree[$key]) && is_array($item)) {
      $main_tree[$key] = _deployer_merge_recursive_children($main_tree[$key], $item);
    }
    else {
      $main_tree[$key] = $item;
    }
  }

  return $main_tree;
}

/**
 * Get contextual tree for project, environment or client node.
 *
 * @param object $node
 *   Node object.
 *
 * @return array|bool
 *   Array containing hierarchical info, or FALSE if no upper client was found.
 */
function _deployer_get_node_context_tree($node) {
  switch ($node->type) {
    // Just link to node.
    case 'client':
      $client['nid'] = $node->nid;
      $client['link'] = l($node->title, 'node/' . $node->nid);
      $wrapper = entity_metadata_wrapper('node', $node);
      $wiki_link = $wrapper->field_wiki->value();
      if (isset($wiki_link['url'])) {
        $client['link'] .= ' (' . l(t('wiki'), $wiki_link['url'], array(
            'attributes' => array('target' => '_blank'),
          )) . ')';
      }
      break;

    // Link to project, client.
    case 'project':
      $wrapper = entity_metadata_wrapper('node', $node);
      $client_nid = $wrapper->field_client->getIdentifier();
      $client_title = $wrapper->field_client->label();
      if (!empty($client_nid)) {
        $client['nid'] = $client_nid;
        $client['link'] = l($client_title, 'node/' . $client_nid);
        $wiki_link = $wrapper->field_client->field_wiki->value();
        if (isset($wiki_link['url'])) {
          $client['link'] .= ' (' . l(t('wiki'), $wiki_link['url'], array(
              'attributes' => array('target' => '_blank'),
            )) . ')';
        }
      }

      // Project.
      $client['children'][$node->nid]['link'] = l($node->title, 'node/' . $node->nid);
      break;

    // Link to environment, project, client.
    case 'environment':
    case 'deploy':
      // In case of deploy, fetch environment node.
      if ($node->type == 'deploy') {
        $wrapper = entity_metadata_wrapper('node', $node);
        $node = $wrapper->field_environment_id->value();
      }

      $wrapper = entity_metadata_wrapper('node', $node);
      $project_nid = $wrapper->field_project->getIdentifier();
      $project_title = $wrapper->field_project->label();
      if (!empty($project_nid)) {
        $client_nid = $wrapper->field_project->field_client->getIdentifier();
        $client_title = $wrapper->field_project->field_client->label();
        if (!empty($client_nid)) {
          $client['nid'] = $client_nid;
          $client['link'] = l($client_title, 'node/' . $client_nid);
          $wiki_link = $wrapper->field_project->field_client->field_wiki->value();
          if (isset($wiki_link['url'])) {
            $client['link'] .= ' (' . l(t('wiki'), $wiki_link['url'], array(
                'attributes' => array('target' => '_blank'),
              )) . ')';
          }

          // Project.
          $client['children'][$project_nid]['link'] = l($project_title, 'node/' . $project_nid);

          // Environment.
          $server_type = field_view_field('node', $node, 'field_server_type');
          $client['children'][$project_nid]['children'][$node->nid]['link'] = l($node->title, 'node/' . $node->nid) . ' (' . substr($server_type[0]['#markup'], 0, 1) . ')';
        }
      }
      break;
  }

  if (isset($client)) {
    $tree[$client['nid']] = $client;
    // Cleanup, because of consistency.
    unset($tree[$client['nid']]['nid']);
    return $tree;
  }
  return FALSE;
}

/**
 * Render contextual tree.
 *
 * @param array $tree
 *   Tree array.
 *
 * @return string
 *   HTML item list.
 */
function _deployer_contextual_tree_render($tree) {
  $variables = array();
  $variables['items'] = array();
  $variables['title'] = '';
  $variables['type'] = 'ul';
  $variables['attributes'] = array();

  // Compose markup for context links.
  // Put clients as top-level items.
  $items = array();
  if ($tree) {
    foreach ($tree as $client_nid => $client) {
      $client_item = array(
        'data' => '<span class="label">' . t('Client:') . '</span>' . $client['link'],
        'class' => array('client'),
      );

      // Select projects as 2nd level items.
      if (isset($client['children'])) {
        foreach ($client['children'] as $project_nid => $project) {
          $project_item = array(
            'data' => '<span class="label">' . t('Project:') . '</span>' . $project['link'],
            'class' => array('project'),
          );

          // Select environments as 3rd level items.
          if (isset($project['children'])) {
            foreach ($project['children'] as $env_nid => $env) {
              $env_item = array(
                'data' => '<span class="label">' . t('Environment:') . '</span>' . $env['link'],
                'class' => array('environment'),
              );
              $project_item['children'][] = $env_item;
            }
          }
          $client_item['children'][] = $project_item;
        }
      }
      $items[] = $client_item;
    }
  }

  // Render item list.
  if ($items) {
    $variables['items'] = $items;
    return theme('item_list', $variables);
  }
  return FALSE;
}
