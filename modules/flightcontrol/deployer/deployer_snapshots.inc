<?php
/**
 * @file
 * Snapshot functions like cleanup etc.
 */

/**
 * Function to check snapshots and cleanup for project.
 */
function deployer_snapshot_cleanup() {
  // Only run if DEPLOYER_SNAPSHOT_CLEANUP_INTERVAL seconds have passed.
  $last_run = variable_get('deployer_snapshot_last_run', 0);
  if ($last_run < (time() - DEPLOYER_SNAPSHOT_CLEANUP_INTERVAL)) {
    // Init queue.
    $queue = DrupalQueue::get('deployer_snapshot_cleanup_queue');

    // Only add items if queue is empty.
    $count = $queue->numberOfItems();
    // There is no harm in trying to recreate existing.
    $queue->createQueue();

    if (!$count) {
      // Get all project nid's.
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'project');
      $result = $query->execute();

      if (isset($result['node'])) {
        // Add projects to queue.
        foreach ($result['node'] as $node) {
          $queue->createItem($node->nid);
        }
      }
    }

    variable_set('deployer_snapshot_last_run', time());
  }
}

/**
 * Clean up old snapshots.
 *
 * @param int $project_id
 *   Project node ID.
 * @param bool $remove_all
 *   TRUE: remove all snapshots for project.
 */
function deployer_cleanup_snapshots($project_id = 0, $remove_all = FALSE) {

  // Track number of snapshots per project,
  // and snapshot limits.
  $counters = array();

  $query = db_select('node', 'n');

  // Deploystatus, db-backup, webroot-backup, backup-status.
  $query->leftjoin('field_data_field_deploystatus', 'dstat', 'n.nid = dstat.entity_id');
  $query->leftjoin('field_data_field_backup_database', 'backup_db', 'n.nid = backup_db.entity_id');
  $query->leftjoin('field_data_field_backup_webroot', 'backup_webroot', 'n.nid = backup_db.entity_id');
  $query->leftjoin('field_data_field_backups_ok', 'backups_ok', 'n.nid = backups_ok.entity_id');
  $query->leftjoin('field_data_field_backups_deleted', 'backups_deleted', 'n.nid = backups_deleted.entity_id');

  // Environment referenced from deploy.
  $query->leftjoin('field_data_field_environment_id', 'env', 'n.nid = env.entity_id');
  $query->leftjoin('field_data_field_project', 'project_field', 'env.field_environment_id_target_id = project_field.entity_id');

  // Snapshot limit.
  $query->leftjoin('field_data_field_snapshot_limit', 'limit_tab', 'project_field.field_project_target_id = limit_tab.entity_id');

  // Project name.
  $query->leftjoin('node', 'project', 'project.nid = project_field.field_project_target_id');

  // Node type: deploy.
  // Deploystatus: 1.
  // Webroot OR DB backup attached.
  $query->condition('n.type', 'deploy');

  // Only finished deploys, and with some kind of backup options checked.
  $or_cond = db_or()->condition('dstat.field_deploystatus_value', DEPLOY_STATUS_FINISHED)
    ->condition('dstat.field_deploystatus_value', DEPLOY_STATUS_FAILED);
  $query->condition($or_cond);
  // Backup opts.
  $or_cond = db_or()->condition('backup_webroot.field_backup_webroot_value', 1)->condition('backup_db.field_backup_database_value', 1);
  $query->condition($or_cond);

  // Only deploys that are not already cleaned up.
  $cleaned_up_or = db_or()->condition('backups_deleted.field_backups_deleted_value', 1, '!=')->condition('backups_deleted.field_backups_deleted_value', NULL);
  $query->condition($cleaned_up_or);

  $query->fields('n', array('nid', 'changed', 'title'));
  $query->fields('backup_db', array('field_backup_database_value'));
  $query->fields('backup_webroot', array('field_backup_webroot_value'));
  $query->fields('dstat', array('field_deploystatus_value'));
  $query->fields('backups_ok', array('field_backups_ok_value'));
  $query->fields('env', array('field_environment_id_target_id'));
  $query->fields('project_field', array('field_project_target_id'));
  $query->fields('limit_tab', array('entity_id', 'field_snapshot_limit_value'));
  $query->fields('project', array('nid', 'title'));

  // Filter for project id, if provided.
  // Removes all snapshots for a project.
  if ($project_id > 0) {
    $query->condition('project.nid', $project_id);
  }

  // Group by deploy-nid.
  $query->groupBy('n.nid');

  // Order by changed datestamp.
  $query->orderBy("n.changed", "DESC");

  // Notify.
  !empty($project) ? drupal_set_message(t('Cleaning up snapshots for project @p', array('@p' => $project_id))) : NULL;

  $snapshots = $query->execute();

  foreach ($snapshots as $snapshot) {
    $proj_id = $snapshot->field_project_target_id;
    if (!isset($counters[$proj_id])) {

      // Set default snapshot limit.
      if (!isset($snapshot->field_snapshot_limit_value) || empty($snapshot->field_snapshot_limit_value)) {
        $counters[$proj_id]['limit'] = 2;
      }
      else {
        $counters[$proj_id]['limit'] = $snapshot->field_snapshot_limit_value;
      }

      $counters[$proj_id]['count'] = 0;
    }

    // Remove snapshot files if
    // there's more than the snapshot limit, OR
    // the snapshot is not ok anyway, OR
    // if we need to remove all snapshot for provided project_id.
    if ($counters[$proj_id]['count'] >= $counters[$proj_id]['limit'] || !$snapshot->field_backups_ok_value || ($project_id > 0 && $remove_all)) {

      // Look up meta file, created at time of deploy.
      // This has information on drupal sites, databases and webroots.
      $meta_file_uri = 'private://deploy/backups/' . $snapshot->nid . '.meta';
      $meta_file = drupal_realpath($meta_file_uri);

      if (file_exists($meta_file)) {
        watchdog('deployer', 'Removing snapshots for project @p', array('@p' => $snapshot->project_title), WATCHDOG_INFO, $link = NULL);
        $content = file_get_contents($meta_file);
        $backup_meta = drupal_json_decode($content);

        if (!empty($backup_meta)) {
          foreach ($backup_meta as $servercounter => $server_backup) {

            // Webroot backupfile.
            // Should only be one per deploy (meta file),
            // unless a backup was made on every server in an environment.
            if (isset($server_backup['webroot_backup']) && !empty($server_backup['webroot_backup'])) {
              $file_uri = $server_backup['webroot_backup'];
              $file = drupal_realpath($file_uri);
              if (file_exists($file)) {
                // Get file id and remove it.
                $query = db_select('file_managed', 'file');
                $query->condition('file.uri', $file_uri);
                $query->fields('file', array('fid'));
                $query->groupBy('file.fid');

                $fids = $query->execute();
                foreach ($fids as $fid) {
                  $file = file_load($fid->fid);
                  file_delete($file);
                }
              }
            }

            // Database backups.
            // Each server in environment can potentially contain
            // a database backup for any drupal site folder.
            if (!empty($server_backup['database_backup'])) {
              foreach ($server_backup['database_backup'] as $backup_counter => $site_backup) {
                if (isset($site_backup['localfile'])) {
                  $file_uri = $site_backup['localfile'];
                  $file = drupal_realpath($file_uri);
                  if (file_exists($file)) {
                    // Get file id and remove it.
                    $query = db_select('file_managed', 'file');
                    $query->condition('file.uri', $file_uri);
                    $query->fields('file', array('fid'));
                    $query->groupBy('file.fid');

                    $fids = $query->execute();
                    foreach ($fids as $fid) {
                      $file = file_load($fid->fid);
                      file_delete($file);
                    }
                  }
                }
              }
            }
          }
        }

        // Remove meta file.
        if (file_exists(drupal_realpath($meta_file_uri))) {
          drupal_unlink($meta_file_uri);
        }
      }

      // Set backup-ok status to 0.
      // This will prevent the snapshot from being listed.
      $deploy = node_load($snapshot->nid);
      $deploy->field_backups_ok['und'][0]['value'] = 0;

      // Mark node as already cleaned up.
      $deploy->field_backups_deleted['und'][0]['value'] = 1;
      field_attach_update('node', $deploy);
    }

    // Only increment if snapshot is ok.
    // Don't want to remove good snapshots too soon.
    if ($snapshot->field_backups_ok_value) {
      $counters[$proj_id]['count']++;
    }
  }

}
