<?php
/**
 * @file
 * Server commands and functions.
 */

/**
 * Basic checks for drupal folder, drush etc.
 *
 * @param object $server
 *   Server object.
 *
 * @return array
 *   array(TRUE|FALSE, (string) $msg).
 */
function _deployer_server_basic_check_ssh(&$server) {
  $path = $server->application_path;
  $msg = '';

  $unsafe_path = TRUE;
  $safe_paths = _deployer_get_safe_paths();

  if (!empty($safe_paths)) {
    foreach ($safe_paths as $safe_path) {
      if (strpos($path, $safe_path) === 0) {
        $unsafe_path = FALSE;
        break;
      }
    }

    if ($unsafe_path) {
      $msg .= t('No safe path to act on: %p%', array('%p%' => $path)) . "\n";
      $msg .= t('Safe paths are: @safe_paths', array('@safe_paths' => print_r($safe_paths, 1)));
      return array(FALSE, $msg);
    }
  }
  else {
    $msg .= t('There are no safe paths specified.');
    return array(FALSE, $msg);
  }

  // Check for webroot.
  $server->exec('stat ' . $path);
  $status = $server->getExitStatus();
  if ($status !== 0) {
    $msg = t('No valid path: %p%', array('%p' => $path));
    return array(FALSE, $msg);
  }

  // Check for drush.
  $server->exec('which drush');
  if ($server->getExitStatus() !== 0) {
    $msg .= t('No drush installed on server!');
    return array(FALSE, $msg);
  }

  $msg .= t("Basic check ok.");
  return array(TRUE, $msg);
}

/**
 * Check if webroot contains drupal installation..
 *
 * @param object $server
 *   Server object.
 *
 * @return bool
 *   Return false / true.
 */
function _deployer_server_is_drupal_ssh(&$server) {

  // Check if valid drupal install.
  $output = $server->exec('cd ' . $server->application_path . '; drush ev "echo VERSION"');
  if ($server->getExitStatus() !== 0) {
    return FALSE;
  }
  elseif (trim($output) == "VERSION") {
    return FALSE;
  }
  return TRUE;
}

/**
 * Get drupal version.
 *
 * @param object $server
 *   Net_SFTP / Net_SSH2 server object.
 *
 * @return bool|mixed
 *   Return string.
 */
function _deployer_get_drupal_version_ssh(&$server) {

  $version = trim($server->exec('cd ' . $server->application_path . '; drush ev "echo VERSION" 2> /dev/null'));

  if ($server->getExitStatus() !== 0) {
    // Update module probably missing.
    return 'DRUSH ERROR';
  }

  if ($version == "VERSION") {
    return '';
  }
  return $version;
}

/**
 * Get deployed version.
 *
 * We echo the '.deployed_tag' file in the webroot.
 * Expected format:
 *  [tagtype]|[tag/commit_hash]$$[url_to_tag_info],  OR
 *  [tagtype]|[commit_hash]|[tag]$$[url_to_tag_info],  OR
 *
 * So the third element 'tag' is optional.
 * If it's empty, 'tag/commit_hash' is interpreted as tag.
 *
 * @param object $server
 *   Net_SFTP / Net_SSH2 server object.
 *
 * @return bool|mixed
 *   Return array with keys 'type', 'tag', 'url'.
 */
function _deployer_get_deployed_version_ssh(&$server) {
  $version = trim($server->exec('cd ' . $server->application_path . '; [ -f .deployed_tag ] && cat .deployed_tag'));
  return _deployer_interpret_deployed_tag($version);
}

/**
 * Get drupal updates.
 *
 * @param object $server
 *   Net_SFTP / Net_SSH2 server object.
 *
 * @return bool|mixed
 *   Return array.
 */
function _deployer_get_drupal_updates_ssh(&$server) {

  $updates = array();

  $output = trim($server->exec('cd ' . $server->application_path . '; drush --no --pipe --simulate pm-update 2>/dev/null'));
  if ($server->getExitStatus() !== 0) {
    // Update module probably missing.
    return FALSE;
  }

  if (empty($output)) {
    // No updates available.
    return array();
  }

  $lines = explode("\n", $output);
  foreach ($lines as $line) {
    $lineparts = preg_split('/\s+/', $line);

    if (isset($lineparts[2])) {
      $module = $lineparts[0];
      $version = $lineparts[1];
      $new_version = $lineparts[2];

      if (strpos($lineparts[3], 'SECURITY-UPDATE') !== FALSE) {
        $type = 'security';
      }
      else {
        $type = 'update';
      }

      $update = array(
        'module' => $module,
        'version' => $version,
        'new_version' => $new_version,
        'type' => $type,
      );

      $updates[] = $update;
    }
  }

  return $updates;
}

/**
 * Function to create health checks queues.
 *
 * Each queue is handled by separate spawned php process.
 * Only add environments to queue if health-check has expired.
 */
function deployer_create_checkup_ssh_queue() {

  // Initiate queues.
  // One for each concurrent process.
  $queues = array();
  $queues_empty = TRUE;
  for ($index = 0; $index < DEPLOYER_HEALTH_CHECK_PARALLEL; $index++) {
    $queues[$index] = DrupalQueue::get('deployer_server_checkup_ssh_queue_' . $index, $reliable = TRUE);
    // There is no harm in trying to recreate existing.
    $queues[$index]->createQueue();

    $count = $queues[$index]->numberOfItems();
    if ($count) {
      $queues_empty = FALSE;
    }
  }

  // Get all environments.
  // Only if all queues are empty.
  if ($queues_empty) {
    $query = db_select('node', 'n');
    $query->leftjoin('field_data_field_last_checked', 'last', 'n.nid = last.entity_id');
    $query->leftJoin('field_data_field_connection_type', 'type', 'type.entity_id = n.nid');

    // Node type: environment.
    $query->condition('n.type', 'environment');
    $query->condition('n.status', 1);
    $query->condition('type.field_connection_type_value', 'ssh');

    // Fields.
    $query->fields('n', array('nid'));
    $query->fields('last', array('field_last_checked_value'));

    // Group by deploy-nid.
    $query->groupBy('n.nid');

    $results = $query->execute();

    // Add servers to queue.
    $queue_index = 0;
    foreach ($results as $row) {
      $check_interval = variable_get('deployer_health_check_interval', 600);
      if (empty($row->field_last_checked_value) || $row->field_last_checked_value < (time() - $check_interval)) {
        $queues[$queue_index]->createItem($row->nid);

        // Cycle queues round robin.
        if ($queue_index == (count($queues) - 1)) {
          $queue_index = 0;
        }
        else {
          $queue_index++;
        }
      }
    }
  }
}

/**
 * Handle health check for all environments.
 *
 * Invokes nohup processes to perform checks in parallel.
 */
function deployer_health_check_cron() {
  // Detect debug mode.
  // Stuff is printed with 'dd()'.
  $debug = variable_get('deployer_health_check_debug', FALSE);
  if ($debug && !function_exists('dd')) {
    $debug = FALSE;
  }

  module_load_include('module', 'deployer', 'deployer');

  // Check drush binary.
  if (!deployer_check_local_drush()) {
    watchdog('deployer', 'Cannot perform health checks. Local drush binary not found or executable!', array(), WATCHDOG_WARNING, $link = NULL);
    return;
  }

  // Update queue (create new, or add environments).
  deployer_create_checkup_ssh_queue();

  // Spawn new process for each queue.
  for ($index = 0; $index < DEPLOYER_HEALTH_CHECK_PARALLEL; $index++) {
    $queue = DrupalQueue::get('deployer_server_checkup_ssh_queue_' . $index, $reliable = TRUE);
    // There is no harm in trying to recreate existing.
    $queue->createQueue();

    // Items in queue? Spawn.
    $count = $queue->numberOfItems();
    if ($count) {
      $debug ? dd(t('Invoking process for healthcheck queue @index (@num items)', array('@index' => $index, '@num' => $count))) : NULL;
      deployer_server_checkup_ssh_queue_invoke($index);
    }
  }
}

/**
 * Spawn new drush process to perform health checks.
 *
 * @param int $index
 *   Queue index.
 */
function deployer_server_checkup_ssh_queue_invoke($index) {
  exec('nohup drush deployer_process_checkup_queue ' . $index . ' > /dev/null 2>&1 &');
}

/**
 * Process health check queue.
 *
 * Invoked by drush.
 *
 * @param int $index
 *   Queue index.
 */
function deployer_process_checkup_queue($index) {
  if ($index >= 0 && $index <= DEPLOYER_HEALTH_CHECK_PARALLEL) {

    // Detect debug mode.
    // Stuff is printed with 'dd()'.
    $debug = variable_get('deployer_health_check_debug', FALSE);
    if ($debug && !function_exists('dd')) {
      $debug = FALSE;
    }

    $start = time();

    $queue = DrupalQueue::get('deployer_server_checkup_ssh_queue_' . $index, $reliable = TRUE);
    $count = $queue->numberOfItems();
    if (!$count) {
      $debug ? dd(t('No items in queue @index', array('@index' => $index))) : NULL;
      return;
    }

    $debug ? dd(t('Health check queue @index: Starting. Need to perform @num health-checks', array('@num' => $count))) : NULL;

    // Claim new item 30s.
    while ($item = $queue->claimItem(30)) {
      $env_id = $item->data;
      // Perform health check on environment.
      _deployer_server_checkup_ssh_act($env_id);
      $queue->deleteItem($item);

      // Stop if time's up.
      if (time() > $start + DEPLOYER_HEALTH_CHECK_QUEUE_RUN_TIME) {
        $debug ? dd(t('Health check queue @index: Time is up! will process rest next time.')) : NULL;
        break;
      }
    }

  }
}

/**
 * Server checkup.
 *
 * @param int $env_id
 *   Node ID of project.
 */
function _deployer_server_checkup_ssh_act($env_id) {
  // Detect debug mode.
  // Stuff is printed with 'dd()'.
  $debug = variable_get('deployer_health_check_debug', FALSE);
  if ($debug && !function_exists('dd')) {
    $debug = FALSE;
  }

  $start = time();
  $env = node_load($env_id);
  if ($env && $env->type == 'environment') {
    $debug ? (t('[@num] Performing health check on environment @e', array('@num' => $env->nid, '@e' => $env->title))) : NULL;

    $env_drupal_version = '';
    $env_deployed_version = '';
    $env_alive = 0;

    // Updates level:
    // -1: unknown.
    // 0: no updates.
    // 1: regular updates.
    // 2: security updates.
    $env_updates = -1;

    $server_instance = field_get_items('node', $env, 'field_server_instance');
    foreach ($server_instance as $server_field_collection) {
      $collection = entity_load('field_collection_item', array($server_field_collection['value']));

      $hostname = field_get_items('field_collection_item', $collection[$server_field_collection['value']], 'field_hostname');
      $hostname = $hostname[0]['value'];

      $port = field_get_items('field_collection_item', $collection[$server_field_collection['value']], 'field_port');
      $port = $port[0]['value'];

      $username = field_get_items('field_collection_item', $collection[$server_field_collection['value']], 'field_username');
      $username = $username[0]['value'];

      $application_path = field_get_items('field_collection_item', $collection[$server_field_collection['value']], 'field_path');
      $application_path = $application_path[0]['value'];

      $webroot_path = field_get_items('field_collection_item', $collection[$server_field_collection['value']], 'field_path_webroot');
      $webroot_path = (isset($webroot_path[0]['value']) && !empty($webroot_path[0]['value'])) ? $webroot_path[0]['value'] : '';

      $server = deployer_new_server_ssh($hostname, $port, $username, $application_path, $webroot_path);
      $msg = $server[1];
      $server = $server[0];

      if ($server) {
        $server->application_path = $application_path;
        $server->hostname = $hostname;
      }
      else {
        $duration = time() - $start;
        $msg = t('[@num] (t+@durs) Unable to connect server %s% with message: %m%', array(
          '@num' => $env->nid,
          '@dur' => $duration,
          '%s%' => $username . '@' . $hostname . ':' . $application_path,
          '%m%' => $msg,
        ));
        watchdog('deployer', $msg, array(), WATCHDOG_WARNING, $link = NULL);
        $debug ? dd($msg) : NULL;
      }

      $is_drupal = 0;
      $version = '';
      $deployed_version = '';
      $updates = '';

      if ($server) {
        $env_alive++;
        $alive = 1;
        if (_deployer_server_is_drupal_ssh($server)) {
          $is_drupal = 1;
        }

        $version = _deployer_get_drupal_version_ssh($server);

        // Shorten string to field size.
        // Drush sometimes doesn't seperate errors into error output...
        $version = substr($version, 0, 255);

        $env_drupal_version = $version;

        $deployed_version = _deployer_get_deployed_version_ssh($server);
        if (!empty($deployed_version['tag'])) {
          $env_deployed_version = serialize($deployed_version);
        }
        $deployed_version = serialize($deployed_version);

        $updates = _deployer_get_drupal_updates_ssh($server);
        if ($updates && empty($updates)) {
          $updates = array();
        }
        elseif ($updates) {
          foreach ($updates as $update) {
            if ($update['type'] == 'security') {
              $env_updates = 2;
            }
            else {
              if ($env_updates <= 0) {
                $env_updates = 1;
              }
            }
          }
        }
      }
      else {
        $alive = 0;
      }

      $cid = $server_field_collection['value'];
      $data = array(
        'cid' => $cid,
        'alive' => $alive,
        'is_drupal' => $is_drupal,
        'version' => $version,
        // Avoid db_query mess up syntax with serialized data.
        'deployed_version' => base64_encode($deployed_version),
        'updates' => serialize($updates),
      );

      $query = db_select('deployer_health_info', 's');
      $query->condition('s.cid', $cid);
      $query->fields('s', array('cid'));
      $results = $query->execute();

      $rowcount = $results->rowCount();
      if (empty($rowcount)) {
        $write = db_insert('deployer_health_info');
      }
      else {
        $write = db_update('deployer_health_info');
        $write->condition('cid', $cid);
      }

      $write->fields($data);
      $write->execute();

    }

    // Servers alive?
    // 1: ok.
    // 2: warning, server in cluster down.
    // 3: critical, whole cluster down.
    if ($env_alive == count($server_instance)) {
      $env_alive = 1;
    }
    elseif ($env_alive == 0) {
      $env_alive = 3;
      deployer_set_status_message($env, DEPLOYER_ENV_DEAD);
    }
    else {
      $env_alive = 2;
      deployer_set_status_message($env, DEPLOYER_ENV_DEAD_PARTIALLY);
    }

    // Set status message for security updates.
    if ($env_updates == 2) {
      deployer_set_status_message($env, DEPLOYER_ENV_SECURITY_UPDATES, NULL, $env->nid . '_updates');
    }

    // Save environment data.
    $env->field_drupal_version[LANGUAGE_NONE][0]['value'] = $env_drupal_version;

    $env->field_health_info[LANGUAGE_NONE][0]['value'] = $env_alive . '|' . $env_updates . '|' . $env_deployed_version;
    $env->field_last_checked[LANGUAGE_NONE][0]['value'] = time();
    node_save($env);

    $debug ? dd(t('[@num] Done', array('@num' => $env->nid))) : NULL;
  }
}
