<?php
/**
 * @file
 * Server commands and functions.
 */

/**
 * Basic checks for webroot folder..
 *
 * @param object $server
 *   Server object.
 *
 * @return array
 *   array(TRUE|FALSE, (string) $msg).
 */
function _deployer_server_basic_check_ftp(&$server) {
  $result = ftp_chdir($server->ftp, $server->application_path);
  if ($result) {
    return array(TRUE, t('Webroot exists.'));
  }
  else {
    return array(
      FALSE,
      t('Webroot does not exist (@r)!', array('@r' => $server->application_path)),
    );
  }
}

/**
 * Get deployed version.
 *
 * We echo the '.deployed_tag' file in the webroot.
 * Expected format:
 *  [tagtype]|[tag/commit_hash]$$[url_to_tag_info],  OR
 *  [tagtype]|[commit_hash]|[tag]$$[url_to_tag_info],  OR
 *
 * So the third element 'tag' is optional.
 * If it's empty, 'tag/commit_hash' is interpreted as tag.
 *
 * @param object $server
 *   Net_SFTP / Net_SSH2 server object.
 *
 * @return bool|mixed
 *   Return array with keys 'type', 'tag', 'url'.
 */
function _deployer_get_deployed_version_ftp(&$server) {
  $version = trim($server->exec('cd ' . $server->application_path . '; [ -f .deployed_tag ] && cat .deployed_tag'));
  return _deployer_interpret_deployed_tag($version);
}

/**
 * Get drupal updates.
 *
 * @param object $server
 *   Net_SFTP / Net_SSH2 server object.
 *
 * @return bool|mixed
 *   Return array.
 */
function _deployer_get_drupal_updates_ftp(&$server) {

  $updates = array();

  $output = trim($server->exec('cd ' . $server->application_path . '; drush --no --pipe --simulate pm-update 2>/dev/null'));
  if ($server->getExitStatus() !== 0) {
    // Update module probably missing.
    return FALSE;
  }

  if (empty($output)) {
    // No updates available.
    return array();
  }

  $lines = explode("\n", $output);
  foreach ($lines as $line) {
    $lineparts = preg_split('/\s+/', $line);
    $module = $lineparts[0];
    $version = $lineparts[1];
    $new_version = $lineparts[2];

    if (strpos($lineparts[3], 'SECURITY-UPDATE') !== FALSE) {
      $type = 'security';
    }
    else {
      $type = 'update';
    }

    $update = array(
      'module' => $module,
      'version' => $version,
      'new_version' => $new_version,
      'type' => $type,
    );

    $updates[] = $update;
  }

  return $updates;
}

/**
 * Function to check health and basic info for server.
 *
 * Stores data in table deployer_health_info.
 */
function deployer_server_checkup_ftp() {

  // Get all environments.
  $query = db_select('node', 'n');
  $query->leftjoin('field_data_field_last_checked', 'last', 'n.nid = last.entity_id');

  // Node type: environment.
  $query->condition('n.type', 'environment');
  $query->condition('n.status', 1);

  // Fields.
  $query->fields('n', array('nid'));
  $query->fields('last', array('field_last_checked_value'));

  // Group by deploy-nid.
  $query->groupBy('n.nid');

  $results = $query->execute();

  // Create servers.
  $environments = array();
  foreach ($results as $row) {
    $env = node_load($row->nid);
    $servers = array();

    $check_interval = variable_get('deployer_health_check_interval', 600);
    if (empty($row->field_last_checked_value) || $row->field_last_checked_value < (time() - $check_interval)) {
      $server_instance = field_get_items('node', $env, 'field_server_instance');
      foreach ($server_instance as $server_field_collection) {
        $collection = entity_load('field_collection_item', array($server_field_collection['value']));

        $hostname = field_get_items('field_collection_item', $collection[$server_field_collection['value']], 'field_hostname');
        $hostname = $hostname[0]['value'];

        $port = field_get_items('field_collection_item', $collection[$server_field_collection['value']], 'field_port');
        $port = $port[0]['value'];

        $username = field_get_items('field_collection_item', $collection[$server_field_collection['value']], 'field_username');
        $username = $username[0]['value'];

        $webroot = field_get_items('field_collection_item', $collection[$server_field_collection['value']], 'field_path');
        $webroot = $webroot[0]['value'];

        $server = deployer_new_server_ftp($hostname, $port, $username);
        $msg = $server[1];
        $server = $server[0];

        if ($server) {
          $server->application_path = $webroot;
          $server->hostname = $hostname;
          $servers[$server_field_collection['value']] = $server;
        }
        else {
          watchdog('deployer', 'Unable to connect server %s% with message: %m%', array('%s%' => $username . '@' . $hostname . ':' . $webroot, '%m%' => $msg), WATCHDOG_ALERT, $link = NULL);
          $servers[$server_field_collection['value']] = 'dead';
        }

        $environments[$row->nid] = $servers;
      }
    }
  }

  // Process commands.
  foreach ($environments as $env_id => &$servers) {

    $env_drupal_version = '';
    $env_deployed_version = '';
    $env_alive = 0;

    // Updates level:
    // -1: unknown.
    // 0: no updates.
    // 1: regular updates.
    // 2: security updates.
    $env_updates = -1;

    foreach ($servers as $cid => &$server) {
      $is_drupal = 0;
      $version = '';
      $deployed_version = '';
      $updates = '';

      if ($server != 'dead') {
        $env_alive++;
        $alive = 1;
        if (_deployer_server_is_drupal_ftp($server)) {
          $is_drupal = 1;
        }

        $version = _deployer_get_drupal_version_ftp($server);
        $env_drupal_version = $version;

        $deployed_version = _deployer_get_deployed_version_ftp($server);
        if (!empty($deployed_version['tag'])) {
          $env_deployed_version = serialize($deployed_version);
        }
        $deployed_version = serialize($deployed_version);

        $updates = _deployer_get_drupal_updates_ftp($server);
        if ($updates && empty($updates)) {
          $updates = array();
        }
        elseif ($updates) {
          foreach ($updates as $update) {
            if ($update['type'] == 'security') {
              $env_updates = 2;
            }
            else {
              if ($env_updates <= 0) {
                $env_updates = 1;
              }
            }
          }
        }
      }
      else {
        $alive = 0;
      }

      $data = array(
        'cid' => $cid,
        'alive' => $alive,
        'is_drupal' => $is_drupal,
        'version' => $version,
        // Avoid db_query mess up syntax with serialized data.
        'deployed_version' => base64_encode($deployed_version),
        'updates' => serialize($updates),
      );

      $query = db_select('deployer_health_info', 's');
      $query->condition('s.cid', $cid);
      $query->fields('s', array('cid'));
      $results = $query->execute();

      $rowcount = $results->rowCount();
      if (empty($rowcount)) {
        $write = db_insert('deployer_health_info');
      }
      else {
        $write = db_update('deployer_health_info');
        $write->condition('cid', $cid);
      }

      $write->fields($data);
      $write->execute();

    }

    // Servers alive?
    // 1: ok.
    // 2: warning, server in cluster down.
    // 3: critical, whole cluster down.
    if ($env_alive == count($servers)) {
      $env_alive = 1;
    }
    elseif ($env_alive == 0) {
      $env_alive = 3;
    }
    else {
      $env_alive = 2;
    }

    // Save environment data.
    $env = node_load($env_id);
    $env->field_drupal_version[LANGUAGE_NONE][0]['value'] = $env_drupal_version;

    $env->field_health_info[LANGUAGE_NONE][0]['value'] = $env_alive . '|' . $env_updates . '|' . $env_deployed_version;
    $env->field_last_checked[LANGUAGE_NONE][0]['value'] = time();
    node_save($env);

  }
}
