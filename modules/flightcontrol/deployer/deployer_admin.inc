<?php
/**
 * @file
 * Admin settings for deployer.
 */

/**
 * Deploy admin configurables.
 *
 * Note that 'variable name' MUST start with 'deployer_'.
 */
function deployer_admin($form, &$form_array) {
  $form['default'] = array(
    '#type' => 'fieldset',
    '#title' => t('Standard settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['default']['deployer_notification_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Send notifications to this address, if any'),
    '#default_value' => variable_get('deployer_notification_email', ''),
    '#required' => FALSE,
    '#description' => t('Leave empty to suppress email notifications'),
  );

  $form['default']['deployer_rsa_keyfile'] = array(
    '#type' => 'textfield',
    '#title' => t('RSA Private keyfile'),
    '#default_value' => variable_get('deployer_rsa_keyfile', ''),
    '#required' => TRUE,
    '#description' => t("Path to ssh private key. Flightcontrol uses this key to authenticate on your hosting systems.<br/>If you supply a path in the private filesystem, I'll try to generate it myself."),
  );

  $form['default']['deployer_health_age_valid'] = array(
    '#type' => 'textfield',
    '#title' => t('Max number of seconds health is considered actual.'),
    '#default_value' => variable_get('deployer_health_age_valid', 3600),
    '#required' => TRUE,
  );

  $form['default']['deployer_health_check_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Interval in seconds to check health for systems'),
    '#default_value' => variable_get('deployer_health_check_interval', 600),
    '#required' => TRUE,
    '#description' => t('Runs with each cron.'),
  );

  $form['default']['deployer_safe_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Safe paths to deploy to'),
    '#default_value' => variable_get('deployer_safe_paths', DEPLOYER_SAFE_PATHS_DEFAULT),
    '#required' => TRUE,
    '#description' => t('Enter each path on a new line.'),
  );

  $form['ftp'] = array(
    '#type' => 'fieldset',
    '#title' => t('FTP settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['ftp']['deployer_ftp_max_retries'] = array(
    '#type' => 'textfield',
    '#title' => t('Max command retries'),
    '#default_value' => variable_get('deployer_ftp_max_retries', 10),
    '#required' => TRUE,
  );

  $form['ftp']['deployer_ftp_reconnect_after'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of retries after which to reconnect'),
    '#default_value' => variable_get('deployer_ftp_reconnect_after', 3),
    '#required' => TRUE,
  );

  $form['debug'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debug settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['debug']['deployer_health_check_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Health check debug mode'),
    '#description' => t('Debug is printed in /tmp/drupal_debug.txt for ssh health checks.<br/>Note that devel-module must be turned on!'),
    '#default_value' => variable_get('deployer_health_check_debug', FALSE),
    '#required' => FALSE,
  );

  $form['#submit'][] = "deployer_admin_submit";

  return system_settings_form($form);
}

/**
 * Admin form validation callback.
 */
function deployer_admin_validate($form, &$form_state) {
  // Check if user did not provide very unsafe paths to deploy in.
  $unsafe_paths = array(
    '/',
    '/usr',
    '/etc',
    '/bin',
    '/boot',
    '/cdrom',
    '/dev',
    '/sbin',
    '/tmp',
  );
  $paths = _deployer_get_safe_paths($form_state['values']['deployer_safe_paths']);
  $entered_unsafe = array();
  foreach ($paths as $path) {
    if (in_array($path, $unsafe_paths)) {
      $entered_unsafe[] = $path;
    }
  }
  if (!empty($entered_unsafe)) {
    form_set_error('deployer_safe_paths', t('You have the following unsafe paths: "@paths". Please fix this error and try again.', array('@paths' => implode(', ', $entered_unsafe))));
  }
}

/**
 * Admin form submit handler.
 */
function deployer_admin_submit($form, &$form_state) {
  // Create SSH-key if needed and possible.
  if (isset($form_state['input']['deployer_rsa_keyfile']) && !empty($form_state['input']['deployer_rsa_keyfile'])) {
    deployer_create_ssh_key($form_state['input']['deployer_rsa_keyfile']);
  }
}
