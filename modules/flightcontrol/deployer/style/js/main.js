/**
 * @file
 * JS stuff.
 *
 * Functionality:
 *  - Apply chosen to certain elements.
 *  - Auto-refresh bookmarks when clicked on bookmark link.
 */

(function ($) {
  // Apply chosen to certain elements.
  Drupal.behaviors.deployer_chosen = {
    attach: function (context, settings) {
      // List of all selectors to apply chosen on.
      var elements = ['select[id^="edit-field-client"].form-select', 'select[id^="edit-field-code-project"].form-select'];

      $(elements).each(function (index, selector) {
        // Because chosen is always a tiny bit too small,
        // let's give it a min width + margin.
        // Little bit nattevingerwerk.
        var width = $(selector).width();
        var min = 100;
        var margin = 30;

        // Apply min width.
        if (width < min) {
          width = min;
        }
        // Add margin.
        width += margin;

        // Choose.
        $(selector).chosen({
          disable_search_threshold: 10,
          max_selected_options: 1,
          search_contains: true,
          max_shown_results: 15,
          width: width + "px"
        });
      });
    }
  };

  // Auto-refresh bookmarks when clicked on bookmark link.
  Drupal.behaviors.deployer_flag = {
    attach: function (context, settings) {
      // Click on manual block-refresh-link.
      if (typeof context['selector'] == 'string') {
        if (~context['selector'].indexOf(".flag-wrapper")) {
          $('#block-deployer-flightcontrol-bookmarks .block-refresh-button').click();
        }
      }
    }
  };
})(jQuery);
