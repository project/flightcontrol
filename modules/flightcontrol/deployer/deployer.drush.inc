<?php
/**
 * @file
 * Drush commands:
 *  - deployer_process_checkup_queue [queue index]
 *    Process health checkup queue.
 */

/**
 * Implements hook_drush_command().
 */
function deployer_drush_command() {
  $items = array();

  // Health check per environment.
  $items['deployer_process_checkup_queue'] = array(
    'description' => 'Check environments in queue',
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUPAL_FULL',
    'callback' => 'deployer_process_checkup_queue',
    'arguments' => array(
      'index' => 'The index of the queue, 0 to ' . DEPLOYER_HEALTH_CHECK_PARALLEL,
    ),
  );

  return $items;
}
