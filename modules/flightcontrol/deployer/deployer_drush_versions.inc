<?php
/**
 * @file
 * Drush functions.
 */

/**
 * Provide drush installation folder.
 *
 * Checks if the wanted drush version is available in the drush folder,
 * and returns the location if it is so.
 * If there's no match, it will return the drush folder of the newest version.
 * If there are no drush versions at all, OR 'remote' is wanted,
 * it will return FALSE.
 *
 * @param string $drush_wanted
 *   drush-folder, without path. Could be "drush-6.2.".
 *
 * @return string
 *   Returns FALSE or drush installation folder.
 */
function _deployer_get_drush($drush_wanted = 'default') {

  // If 'remote' is wanted, we will return FALSE so deployer uses remote drush.
  if ($drush_wanted == 'remote') {
    return FALSE;
  }

  if (!$drush_dir = variable_get('deployer_drush_versions_path')) {
    watchdog('deployer', 'Var deployer_drush_versions_path not set, can only use remote drush.', array(), WATCHDOG_WARNING, $link = NULL);
    return FALSE;
  }

  // List drush options.
  if (!is_dir($drush_dir)) {
    return FALSE;
  }
  $drush_options = scandir($drush_dir, 1);
  if (empty($drush_options)) {
    return FALSE;
  }

  // Return match, or set default.
  foreach ($drush_options as $drush) {
    $drush_install = $drush_dir . '/' . $drush;
    if ($drush != '.' && $drush != '..' && is_dir($drush_dir . '/' . $drush) && file_exists($drush_install . '/drush')) {
      if ($drush_wanted == $drush) {
        return $drush_install;
      }
      if (!isset($drush_default)) {
        $drush_default = $drush_install;
      }
    }
  }

  if (!isset($drush_default)) {
    return FALSE;
  }
  else {
    return $drush_default;
  }
}

/**
 * Provide drush options / versions.
 *
 * Checks what versions are available in the drush folder,
 * If there are no drush versions at all, it will only return default values.
 */
function _deployer_get_drush_options() {
  // Default options.
  $drush_array = array();
  $drush_array['default'] = 'default (latest version available)';
  $drush_array['remote'] = 'remote (Use remote drush)';

  if (!$drush_dir = variable_get('deployer_drush_versions_path')) {
    watchdog('deployer', 'Var deployer_drush_versions_path not set, can only use remote drush.', array(), WATCHDOG_ALERT, $link = NULL);
    return $drush_array;
  }

  // List drush options.
  if (is_dir($drush_dir)) {
    $drush_options = scandir($drush_dir, 1);
    if (!empty($drush_options)) {
      // Get proper options.
      foreach ($drush_options as $drush) {
        $drush_exec = $drush_dir . '/' . $drush . '/drush';
        if ($drush != '.' && $drush != '..' && is_dir($drush_dir . '/' . $drush) && file_exists($drush_exec)) {
          $drush_array[$drush] = $drush;
        }
      }
    }
  }

  return $drush_array;
}

/**
 * Perform check and patch to drush.
 *
 *  - Drush 8.x somehow uses .my.cnf, which may introduce credentials
 *    conflicts in a LOT of infrastructures.
 *    See https://github.com/drush-ops/drush/pull/2387 .
 *
 * @param string $drush
 *   Patch to drush.
 */
function _deployer_check_patch_drush($drush) {
  // Check / patch drush for portability.
//  if (is_dir($drush)) {
//
//    // Patch drush phar to omit defaults-extra-file .
//    // See https://github.com/drush-ops/drush/pull/2387 .
//    $drush_bin = $drush . '/drush';
//    if (file_exists($drush_bin)) {
//      $content = file_get_contents($drush_bin);
//      $wrong_string = '$parameters[\'defaults-extra-file\']';
//      $replace = '$parameters[\'defaults-file\']';
//      if (strpos($content, $wrong_string)) {
//        _deployer_log(t('Patching drush: Fix .my.cnf bug'));
//        $content_patched = str_replace($wrong_string, $replace, $content);
//        $result = file_put_contents($drush_bin, $content_patched);
//        if ($result == FALSE) {
//          _deployer_log(t('Cannot write to drush folder @f. Please make me owner!', array('@f' => $drush)));
//        }
//      }
//      else {
//        _deployer_log(t('No need to patch drush.'));
//      }
//    }
//  }
}
