<?php
/**
 * @file
 * Commonly used functions during deploys.
 */

/**
 * Function to append message to logfile.
 *
 * @param string $message
 *   String containing message
 * @param object $node
 *   Node object to attach logfile to.
 * @param bool $debug
 *   This is debug, or not.
 * @param bool $replace
 *   TRUE: create (replace if it somehow exists) a new logfile and attach
 *         to node.
 *   FALSE: Assume the file, just append messages to the existing file.
 */
function _deployer_log($message, $node = NULL, $debug = TRUE, $replace = TRUE) {
  global $_deployer_logfile, $_deployer_logfile_raw;

  if (empty($_deployer_logfile) && !empty($node)) {
    // Create logfile.
    $_deployer_logfile = 'private://deploy/logs/' . $node->nid . '.log';
    $_deployer_logfile_raw = $_deployer_logfile . '.raw';
    $logpath = drupal_realpath('private://deploy/logs');
    if (!file_prepare_directory($logpath)) {
      drupal_mkdir('private://deploy/logs', NULL, TRUE);
    }
    if ($replace) {
      // Standard replacement mode.
      // Will create new log file.
      if ($logfile_obj = file_save_data('', $_deployer_logfile, FILE_EXISTS_REPLACE)) {
        if (!empty($node)) {
          // Attach logfile to node.
          $nid = $node->nid;
          $node = node_load($nid);
          $logfile_obj->display = 1;
          $logfile_obj->description = "";
          $node->field_logfile[LANGUAGE_NONE][0] = (array) $logfile_obj;
          node_save($node);
        }
      }
      else {
        watchdog('deployer', 'Unable to create logfile %f%', array('%f%' => $_deployer_logfile), WATCHDOG_ALERT, $link = NULL);
      }
    }
  }

  if (!empty($_deployer_logfile)) {
    // Put message into log.
    $logfile_real = drupal_realpath($_deployer_logfile_raw);
    if ($message == "\n") {
      file_put_contents($logfile_real, $message, FILE_APPEND);
    }
    else {
      if (strpos($message, "\n") === 0) {
        $message = preg_replace('/^\n$/i', '', trim($message));
        _deployer_log("\n");
      }

      // Set debug flag.
      if ($debug) {
        $message = '[DEBUG] ' . $message;
      }
      file_put_contents($logfile_real, date("Y-m-d H:i:s") . ' => ' . $message . PHP_EOL, FILE_APPEND);
    }

    // Render logfile.
    _deployer_render_log();

  }
}

/**
 * Set value to placeholder for logfile.
 *
 * @param string $placeholder
 *   Placeholder, surrounded by "&&".
 *
 * @param string $value
 *   Value.
 */
function _deployer_set_placeholder($placeholder, $value) {
  global $_deployer_placeholders;
  $_deployer_placeholders[$placeholder] = $value;

  // Update logfile.
  _deployer_render_log();
}

/**
 * Function to render logfile and replace placeholders.
 */
function _deployer_render_log() {
  global $_deployer_logfile, $_deployer_logfile_raw, $_deployer_placeholders;

  if (!empty($_deployer_logfile) && !empty($_deployer_logfile_raw)) {
    $log = file_get_contents(drupal_realpath($_deployer_logfile_raw));
    if (!empty($_deployer_placeholders)) {
      foreach ($_deployer_placeholders as $placeholder => $value) {
        $log = str_replace($placeholder, $value, $log);
      }
    }

    $rendered_log_real = drupal_realpath($_deployer_logfile);
    file_put_contents($rendered_log_real, $log);
  }
}

/**
 * Include common used libraries for deployer.
 *
 * @param array $deploy_context
 *   Context array.
 *
 * @return bool
 *   TRUE of FALSE.
 */
function _deployer_get_libraries(&$deploy_context = NULL) {
  // Tar library.
  $libraries = libraries_get_libraries();
  if (!isset($libraries['archive_tar'])) {
    watchdog('deployer', 'archive_tar LIBRARY NOT FOUND!', array(), WATCHDOG_ALERT, $link = NULL);
    drupal_set_message(t('archive_tar library not found!'));
    $context['results']['proceed'] = 0;

    if (!empty($deploy_context)) {
      $deploy_context['failed'] = 1;
    }
  }
  else {
    require_once $libraries['archive_tar'] . '/Archive/Tar.php';
  }

  // Patch for phpseclib.
  set_include_path(get_include_path() . PATH_SEPARATOR . constant('DRUPAL_ROOT') . '/' . libraries_get_path('phpseclib'));

  // Check for phpsec library and include.
  $libraries = libraries_get_libraries();
  if (!isset($libraries['phpseclib'])) {
    watchdog('deployer', 'phpseclib LIBRARY NOT FOUND!', array(), WATCHDOG_ALERT, $link = NULL);
    drupal_set_message(t('phpseclib LIBRARY NOT FOUND!'));
    if (!empty($deploy_context)) {
      $deploy_context['failed'] = 1;
    }
  }
  else {
    require_once $libraries['phpseclib'] . '/Net/SSH2.php';
    require_once $libraries['phpseclib'] . '/Net/SFTP.php';
    require_once $libraries['phpseclib'] . '/Crypt/RSA.php';
  }
}

/**
 * Retrieve code from repository.
 *
 * @param object $deploynode
 *   Deploy node object.
 *
 * @param array $deploy_context
 *   Deploy context.
 */
function deployer_batch_action_get_code_from_repo($deploynode, &$deploy_context) {
  // Check if routine failed.
  if (!$deploy_context['failed']) {
    if ($deploy_context['mode'] == 'deploy') {
      _deployer_log("\n" . t('Retrieving code from version control.'), NULL, DEPLOYER_NO_DEBUG);

      // Tag or hash from node/add/deploy form.
      // Usually in form 'repo-type|commit-hash'.
      $tag = field_get_items('node', $deploynode, 'field_commit_tag');
      $tag = $tag[0]['value'];

      $env = node_load($deploy_context['env_id']);
      $field_project = field_get_items('node', $env, 'field_project');
      $deploy_context['project_id'] = $field_project[0]['target_id'];

      // Set tag string to mark webroot.
      $tag_url = deployer_tag_link($deploy_context['project_id'], $deploy_context['env_id'], $tag, 'plain');
      $tag_url = $tag_url['url'];

      $deploy_context['tag_str'] = $tag . '$$' . $tag_url;

      // Retrieve it.
      _deployer_log(t('Retrieving codebase archive'));
      $filename = deployer_get_tarball_filename($deploy_context['project_id'], $tag);
      if ($filename) {
        $deploy_context['archive'] = $filename;
        _deployer_log(t('Tarball received'));
      }
      else {
        $deploy_context['failed'] = 1;
        _deployer_log(t('(CRITICAL) No proper tarball received!'));
      }
    }
  }
}

/**
 * Check code-archive if it's a proper archive.
 *
 * @param object $deploynode
 *   Deploy node object.
 *
 * @param array $deploy_context
 *   Deploy context.
 */
function deployer_batch_action_check_code($deploynode, &$deploy_context) {
  // Check if routine failed.
  if (!$deploy_context['failed'] && $deploy_context['mode'] == 'deploy') {
    // Check for valid tarball.
    _deployer_log(t('Checking code from repo now'));
    $tar_object = new Archive_Tar($deploy_context['archive']);
    $content = $tar_object->listContent();
    if ($content) {
      $size = filesize($deploy_context['archive']);
      _deployer_log(t('Tarball seems valid (%s%)', array('%s%' => format_size($size))));
    }
    else {
      _deployer_log(t('(CRITICAL) Tarball unvalid: %f%', array('%f%' => $deploy_context['archive'])));
      $deploy_context['failed'] = 1;
    }
  }
}

/**
 * Trigger hooks for predeploy.
 *
 * @param object $deploynode
 *   Deploy node object.
 *
 * @param array $deploy_context
 *   Current deploy context.
 */
function deployer_batch_action_trigger_predeploy($deploynode, $deploy_context) {
  // Trigger hooks, pre-deploy.
  module_invoke_all('deployer_predeploy', $deploynode, $deploy_context);
}

/**
 * Set restore properties if in restore mode.
 *
 * @param object $deploynode
 *   Deploy node object.
 * @param array $deploy_context
 *   Deploy context.
 * @param array $servers
 *   Array containing server objects.
 */
function deployer_batch_action_set_restore_props($deploynode, &$deploy_context, &$servers) {
  // Check if routine failed.
  if (!$deploy_context['failed'] && $deploy_context['mode'] == 'restore') {

    _deployer_log("\n" . t('Setting restore properties'));

    $restore_id = field_get_items('node', $deploynode, 'field_restore_id');
    $restore_id = $restore_id[0]['value'];

    // Look up meta file, created at time of deploy.
    // This has information on drupal sites, databases and webroots.
    $file = drupal_realpath('private://deploy/backups/' . $restore_id . '.meta');
    if (file_exists($file)) {
      $content = file_get_contents($file);
      $deploy_context['restore_meta'] = drupal_json_decode($content);
      if (!$deploy_context['restore_meta'] || empty($deploy_context['restore_meta'])) {
        // No meta data.
        $deploy_context['failed'] = 1;
        _deployer_log(t('No proper metadata in %f%. (CRITICAL)', array('%f%' => $file)));
      }
    }
    else {
      // Cannot restore if there's nothing to restore.
      _deployer_log(t('Metafile %f% does not exist. (CRITICAL)', array('%f%' => $file)));
      $deploy_context['failed'] = 1;
    }

    // Check restore properties and snapshot integrity.
    if (!$deploy_context['failed']) {

      _deployer_log(t('Checking snapshot integrity.'));
      $deploy_context['restores'] = array();

      // Databases properties / integrity.
      $restore_databases = field_get_items('node', $deploynode, 'field_restore_databases');
      $deploy_context['restore_databases'] = $restore_databases[0]['value'];
      if ($deploy_context['restore_databases']) {
        if (!empty($deploy_context['restore_meta'][0]['database_backup'])) {

          foreach ($deploy_context['restore_meta'][0]['database_backup'] as $backup) {
            if (is_file(drupal_realpath($backup['localfile']))) {
              _deployer_log(t('Database found for site @s', array('@s' => $backup['site'])));
              $backup['remote_file'] = '/tmp/.deploy_' . uniqid() . '.sql.gz';
              $deploy_context['restores']['database_backup'][] = $backup;

              // Preset remote file location to use, if needed.
              foreach ($servers as &$server) {
                $server->temp_db_archive[$backup['site']] = $server->temp_folder . '/.deploy_' . uniqid() . '.sql.gz';
              }
            }
            else {
              _deployer_log(t('File %f% does not exist.', array('%f%' => $backup['localfile'])));
              $deploy_context['failed'] = 1;
            }
          }
        }
        else {
          $deploy_context['failed'] = 1;
          _deployer_log(t('Unable to restore databases. Not in snapshot. (CRITICAL)'));
        }
      }

      // Webroot properties / integrity.
      $restore_webroot = field_get_items('node', $deploynode, 'field_restore_webroot');
      $deploy_context['restore_webroot'] = $restore_webroot[0]['value'];

      $restore_files = field_get_items('node', $deploynode, 'field_restore_files');
      $deploy_context['restore_files'] = $restore_files[0]['value'];

      $restore_settings_php = field_get_items('node', $deploynode, 'field_restore_settings_php');
      $deploy_context['restore_settings_php'] = $restore_settings_php[0]['value'];

      if ($deploy_context['restore_webroot'] || $deploy_context['restore_files'] || $deploy_context['restore_settings_php']) {
        if (!empty($deploy_context['restore_meta'][0]['webroot_backup'])) {
          if (is_file(drupal_realpath($deploy_context['restore_meta'][0]['webroot_backup']))) {
            $deploy_context['restores']['webroot_backup'][0] = $deploy_context['restore_meta'][0]['webroot_backup'];
            // Set tag string to mark webroot.
            if (isset($deploy_context['restore_meta'][0]['tag_str']) && !empty($deploy_context['restore_meta'][0]['tag_str'])) {
              $deploy_context['tag_str'] = $deploy_context['restore_meta'][0]['tag_str'];
            }
            else {
              $deploy_context['tag_str'] = '';
            }
          }
          else {
            $deploy_context['failed'] = 1;
            _deployer_log(t('Unable to restore webroot. File %f% not present on disk. (CRITICAL)', array('%f%' => $deploy_context['restore_meta'][0]['webroot_backup'])));
          }
        }
        else {
          $deploy_context['failed'] = 1;
          _deployer_log(t('Unable to restore webroot. Not in snapshot. (CRITICAL)'));
        }
      }

      // Failure if nothing is chosen to restore.
      if (empty($deploy_context['restores'])) {
        _deployer_log(t('You choose nothing to restore!'));
        $deploy_context['failed'] = 1;
      }

    }
    else {
      _deployer_log(t('Errors in backup. Cannot restore snapshot.'));
    }
  }
}

/**
 * See if deploy is running.
 *
 * @param object $node
 *   Node object containing field field_deploystatus.
 *
 * @return bool
 *   TRUE if deploy seems running, FALSE if not (idle of finished).
 */
function deployer_deploy_is_running($node) {
  $deploy_status = field_get_items('node', $node, 'field_deploystatus');
  if (!empty($deploy_status)) {
    $deploy_status = $deploy_status[0]['value'];
    if ($deploy_status == DEPLOY_STATUS_RUNNING) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Class lockNode
 *
 * Create a lockfile for a node, and remove if process stops.
 */
class DeployerLockNode {

  /**
   * Constructor.
   *
   * @param int $nid
   *   Node ID.
   */
  public function __construct($nid) {
    $this->nid = $nid;
    $this->lockFile = drupal_realpath(DEPLOYER_LOCKS_PATH . '/' . $this->nid . '.lock');
    $this->locked = FALSE;
  }

  /**
   * Destructor.
   *
   * Unlock this node only if it was locked on purpose.
   * So instances of this class that were created to only check
   * if node was locked, won't unlock it on shutdown.
   */
  public function __destruct() {
    if ($this->locked) {
      $this->unlock();
    }
  }

  /**
   * Lock current process.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function lock() {
    // Mark this object as locked-on-purpose.
    $this->locked = TRUE;

    // Get current PID.
    $pid = getmypid();

    // Prepare backup location on deployer server.
    $create_path = drupal_realpath(DEPLOYER_LOCKS_PATH);
    if (!$create_path) {
      return FALSE;
    }
    if (!file_prepare_directory($create_path)) {
      $dir = drupal_mkdir(DEPLOYER_LOCKS_PATH, NULL, TRUE);
      if (!$dir) {
        return FALSE;
      }
    }

    if (!file_put_contents(drupal_realpath(DEPLOYER_LOCKS_PATH . '/' . $this->nid . '.lock'), $pid)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Remove lock file.
   */
  public function unlock() {
    if (is_file($this->lockFile)) {
      unlink($this->lockFile);
    }
  }

  /**
   * Check if node is locked.
   *
   * @return bool
   *   TRUE if lockfile exists, FALSE of it doesn't.
   */
  public function hasLock() {
    if (is_file($this->lockFile)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * See if pid in lockfile is still running.
   */
  public function lockProcAlive() {
    if (function_exists('posix_getpgid') && is_file($this->lockFile)) {
      $old_pid = trim(file_get_contents($this->lockFile));
      if (posix_getpgid($old_pid)) {
        return TRUE;
      }
      return FALSE;
    }

    // No lockfile.
    // Assume we are ok.
    return TRUE;
  }
}
