<?php

/**
 * @file
 * Helper functions for deployer.
 *
 * _deployer_create_save_file_entity($uri)
 * _deployer_interpret_deploystatus($status)
 */

/**
 * Create file object and save it.
 *
 * If file does not exist as file entity, we create one and return it.
 * Else, the already existing file is returned.
 * This is used for downloading backup archives etc.
 *
 * If file does not exist on disk, FALSE is returned.
 *
 * @param string $uri
 *   Drupal file uri.
 *
 * @return object
 *   file|bool.
 */
function _deployer_create_save_file_entity($uri) {

  // Check for presence on disk.
  if (!file_exists(drupal_realpath($uri))) {
    // File does not exist on disk.
    return FALSE;
  }

  // Check if file already exists.
  $query = db_select('file_managed', 'f');
  $query->condition('f.uri', $uri);

  $query->fields('f', array('fid'));
  $results = $query->execute();

  foreach ($results as $row) {
    $file = file_load($row->fid);
    return $file;
  }

  // Create new file.
  global $user;
  $file = new stdClass();
  $file->fid = NULL;
  $file->uri = $uri;
  $file->filename = drupal_basename($uri);
  $file->filemime = file_get_mimetype($file->uri);
  $file->uid = $user->uid;
  $file->status = FILE_STATUS_PERMANENT;
  $file->filesize = filesize(drupal_realpath($uri));

  file_save($file);

  return $file;
}

/**
 * Function to interpret deploy status.
 *
 * @param int $status
 *   Deploy status.
 *
 * @return string
 *   Human readable deploy status.
 */
function _deployer_interpret_deploystatus($status) {
  switch ($status) {
    case DEPLOY_STATUS_IDLE:
      return t('Not started');

    case DEPLOY_STATUS_INITIATING:
      return t('Initiating');

    case DEPLOY_STATUS_RUNNING:
      return t('Running');

    case DEPLOY_STATUS_FINISHED:
      return t('Finished');

    case DEPLOY_STATUS_FAILED:
      return t('Failed');

    default:
      return t('Unknown');
  }
}

/**
 * Interpret health status for an environment.
 *
 * @param string $status
 *   Digits seperated by '|'.
 *   First digit: Environment alive status.
 *   Second digit: Updates status.
 *
 * @param array $item
 *   all, alive, updates or version.
 *   Makes function return proper parts.
 *
 * @return string
 *   html string containing span's.
 */
function _deployer_interpret_health_status($status, $item = array('all' => 1)) {

  $parts = explode('|', $status);
  $health = '';

  // Servers alive?
  if (isset($parts[0]) && ($item['all'] || $item['alive'])) {
    $alive = $parts[0];

    if ($alive == 1) {
      $health .= '<span class="health health-alive ok" title="' . t('Healthy') . '"><span>ok</span></span>';
    }
    elseif ($alive == 2) {
      $health .= '<span class="health health-alive warning" title="' . t('Warning: node in environment down') . '"><span>warning</span> </span>';
    }
    elseif ($alive == 3) {
      $health .= '<span class="health health-alive critical" title="' . t('Whole environment down!') . '"><span>critical</span></span>';
    }
  }

  // Updates.
  if (isset($parts[1]) && ($item['all'] || $item['updates'])) {
    $updates = $parts[1];

    if ($updates == -1) {
      $health .= '<span class="health updates dunno" title="' . t('Could not determine updates') . '"><span>updates</span></span>';
    }
    elseif ($updates == 0) {
      $health .= '<span class="health updates ok" title="' . t('Up to date') . '"><span>updates</span></span>';
    }
    elseif ($updates == 1) {
      $health .= '<span class="health updates regular" title="' . t('Updates available') . '"><span>updates</span></span>';
    }
    elseif ($updates == 2) {
      $health .= '<span class="health updates security" title="' . t('Security updates available') . '"><span>security updates</span></span>';
    }
  }

  // Deployed version.
  if (isset($parts[2]) && !empty($parts[2]) && ($item['all'] || $item['version'])) {
    $deployed_version = unserialize($parts[2]);
    $health .= '<span class="deployed_version"><a href="' . $deployed_version['url'] . '" target="_blank">' . truncate_utf8($deployed_version['tag'], 10, FALSE, '...') . '</a></span>';
  }

  return $health;
}

/**
 * Interpret health age for an environment.
 *
 * @param int $time
 *   Epoch time.
 *
 * @return string
 *   html string containing span's.
 */
function _deployer_interpret_health_age($time) {

  $html = '';
  if (!empty($time)) {
    $max_age = variable_get('deployer_health_age_valid', 3600);
    if ($time < (time() - $max_age)) {
      // Expired health.
      $class = 'expired';
    }
    else {
      $class = 'ok';
    }

    $html = '<span class="health_date ' . $class . '" title="' . t('Health age: @class', array('@class' => $class)) . '">' . format_date($time, 'short') . '</span>';
  }

  return $html;
}

/**
 * Interpret deployed_tag string.
 *
 * String is printed into meta file during deploy into the webroot.
 * Expected format:
 *  [tagtype]|[tag/commit_hash]$$[url_to_tag_info],  OR
 *  [tagtype]|[commit_hash]|[tag]$$[url_to_tag_info],  OR
 *
 * So the third element 'tag' is optional.
 * If it's empty, 'tag/commit_hash' is interpreted as tag.
 *
 * @param string $version
 *   String from meta file on webroot.
 *
 * @return array
 *   Array containing keys 'url', 'tag_type', 'tag' and 'hash'.
 */
function _deployer_interpret_deployed_tag($version = '') {
  $url = '';
  $tag_type = '';
  $tag = '';
  $hash = '';

  $parts = explode('$$', $version);
  if (isset($parts[0])) {
    $tag_parts = explode('|', $parts[0]);

    $tag_type = $tag_parts[0];

    // Tag is last item in array.
    // If there are 3 items, hash is the second.
    if (isset($tag_parts[2])) {
      $tag = $tag_parts[2];
      $hash = $tag_parts[1];
    }
    elseif (isset($tag_parts[1])) {
      $tag = $tag_parts[1];
    }
    else {
      $tag = NULL;
    }

  }
  if (isset($parts[1])) {
    $url = $parts[1];
  }

  return array(
    'tag' => $tag,
    'tag_type' => $tag_type,
    'url' => $url,
    'hash' => $hash,
  );
}

/**
 * Generates UUID.
 *
 * @return string
 *   UUID string.
 */
function _deployer_gen_uuid() {
  return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
    // 32 bits for "time_low"
    mt_rand(0, 0xffff), mt_rand(0, 0xffff),
    // 16 bits for "time_mid"
    mt_rand(0, 0xffff),
    // 16 bits for "time_hi_and_version",
    // four most significant bits holds version number 4
    mt_rand(0, 0x0fff) | 0x4000,
    // 16 bits, 8 bits for "clk_seq_hi_res",
    // 8 bits for "clk_seq_low",
    // two most significant bits holds zero and one for variant DCE1.1
    mt_rand(0, 0x3fff) | 0x8000,
    // 48 bits for "node"
    mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
  );
}

/**
 * Register new environment status.
 *
 * @param object $node
 *   Node object.
 * @param int $status
 *   Status for env.
 * @param string $message
 *   Optional message.
 * @param string $id
 *   Optional ID for message. If empty, node-id is used.
 *   Note: A new message with same id overrides a previous message.
 */
function deployer_set_status_message($node, $status, $message = NULL, $id = NULL) {
  $messages = variable_get('deployer_status_messages', array());

  // Use node id if $id is empty.
  empty($id) ? $id = $node->nid : NULL;

  // Add status message.
  $messages[$id] = array(
    'timestamp' => time(),
    'type' => $node->type,
    'status' => $status,
    'nid' => $node->nid,
    'message' => $message,
  );

  variable_set('deployer_status_messages', $messages);
}

/**
 * Get defined status messages / classes for environment status.
 *
 * @param int $status
 *   Deployer environment status.
 *
 * @return bool|array
 *   Status message array.
 */
function deployer_status_message_map($status) {
  // Status messages / classes.
  $status_map = array(
    DEPLOYER_ENV_DEAD => array(
      'class' => 'down',
      'message' => t('Whole environment down!'),
    ),
    DEPLOYER_ENV_RUNNING => array(
      'class' => 'ok',
      'message' => t('Environment seems fine.'),
    ),
    DEPLOYER_ENV_DEAD_PARTIALLY => array(
      'class' => 'partially-down',
      'message' => t('Environment partially down!'),
    ),
    DEPLOYER_ENV_UPDATES => array(
      'class' => 'updates-pending',
      'message' => t('Environment / codebase has updates available.'),
    ),
    DEPLOYER_ENV_SECURITY_UPDATES => array(
      'class' => 'security-updates-pending',
      'message' => t('Environment / codebase needs security updates!'),
    ),
  );

  if (isset($status_map[$status])) {
    return $status_map[$status];
  }
  return array(
    'class' => 'undefined',
    'message' => '',
  );
}

/**
 * Check local drush binary.
 *
 * @return bool
 *   TRUE: ok, FALSE on failure.
 */
function deployer_check_local_drush() {
  $output = array();
  $status = 1;
  exec('which drush', $output, $status);

  if ($status === 0) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Function to retrieve the function that called yours.
 *
 * @return array
 *   Backtrace array in format
 *   array (
 *     'file' => '/var/www/sites/all/modules/devel/devel.module',
 *     'line' => 1415,
 *     'function' => 'eval',
 *   )
 */
function deployer_get_function_caller() {
  $backtrace = debug_backtrace(NULL, 2);
  $return = $backtrace[1];

  // Remove args. It might reveal passwords.
  if (isset($return['args'])) {
    unset($return['args']);
  }
  return $return;
}

/**
 * Create SSH-key if it's in the private filesystem.
 *
 * @param string $keyfile
 *   Filename of private key.
 */
function deployer_create_ssh_key($keyfile) {

  $dir = dirname($keyfile);
  $filename = basename($keyfile);
  if (file_prepare_directory($dir, FILE_CREATE_DIRECTORY)) {
    drupal_chmod($dir, 0700);
    $dir = drupal_realpath($dir);

    $keyfile = $dir . '/' . $filename;
    if (!file_exists($keyfile)) {
      $private_fs = drupal_realpath(variable_get('file_private_path'));
      if (!empty($private_fs)) {
        // Check if keyfile location is inside private filesystem.
        if (strpos($keyfile, $private_fs) === 0) {

          // Check containing folder.
          if (is_dir(dirname($keyfile)) || mkdir(dirname($keyfile), 0700, TRUE)) {
            // Try to generate ssh-key.
            $command = 'ssh-keygen -t rsa -b 4096 -N "" -f "' . $keyfile . '"';
            $output = array();
            $status = 1;
            exec($command, $output, $status);

            if ($status != 0) {
              drupal_set_message(t('Unable to create ssh-key (using ssh-keygen) in %keyfile ! Please create it yourself and provide me access.', array(
                '%keyfile' => $keyfile,
              )), 'error');
            }
            else {
              drupal_set_message(t('Created SSH-key'));
            }
          }
        }
      }
    }
  }

}

/**
 * Get user-defined safe-paths for deploys.
 *
 * @param null|string $paths_string
 *   Derive safe-paths from this input string instead of system variable.
 *
 * @return array
 *   Safe-paths array, trimmed etc.
 */
function _deployer_get_safe_paths($paths_string = NULL) {
  $safe_paths = array();
  $safe_paths_raw = (!is_null($paths_string)) ? explode(PHP_EOL, $paths_string) : explode(PHP_EOL, variable_get('deployer_safe_paths', DEPLOYER_SAFE_PATHS_DEFAULT));
  foreach ($safe_paths_raw as $path) {
    $safe_paths[] = trim($path);
  }
  return $safe_paths;
}
