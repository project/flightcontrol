Install libraries
 phpseclib
 archive_tar
in sites/all/libraries/ .

System installation (used for phplibsec).
This will tremendously speed up sftp transfers.
 => Install php5-mcrypt
 => Add "extension=mcrypt.so" to php.ini.

PHP.ini
 => max_input_time = -1
 => max_execution_time = 0

Remote server dependencies (usually available on almost every GNU/linux distro):
 ssh with ssh-key root login
 find binary
 drush binary
 tar binary
 gzip binary
 gunzip binary


Notifications
Some submodules use email to send notifications.
Make sure to configure sendmail for all apache (of php-fpm if you use nginx), and CLI (used by drush).
