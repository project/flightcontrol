<?php
/**
 * @file
 * File to be called from CLI, to parse settings.php.
 */

/**
 * Get help message.
 *
 * @return string
 *   Message.
 */
function help() {
  $msg = PHP_EOL . 'Usage: [script] [settings-file-path]' . PHP_EOL;
  return $msg;
}

// Validate arguments.
if (!isset($argv[1])) {
  echo help();
  exit(1);
}

// Include settings file.
// This means that $file is executed as the user we are!
$file = $argv[1];
if (is_file($file)) {
  // TODO replace with http://php.net/manual/en/runkit.sandbox.php if possible.
  if (!@include $file) {
    echo "Unable to include file" . PHP_EOL;
    exit(1);
  }
}
else {
  echo "File does not exist" . PHP_EOL;
  exit(1);
}

// Get drupal 6/7 db properties.
if (isset($db_url)) {
  $db = parse_url($db_url);
  $type = $db['scheme'];
  if ($type != 'mysql') {
    echo "Driver $type not supported";
    exit(1);
  }

  $host = isset($db['host']) ? $db['host'] : '127.0.0.1';
  $username = $db['user'];
  $password = $db['pass'];
  $db = preg_replace('/^\//i', '', $db['path']);

  echo "--host='$host' --user='$username' --password='$password' '$db'";
  exit(0);
}
elseif (isset($databases['default'])) {
  $type = $databases['default']['default']['driver'];
  if ($type != 'mysql') {
    echo "Driver $type not supported";
    exit(1);
  }

  $db = $databases['default']['default']['database'];
  $username = $databases['default']['default']['username'];
  $password = $databases['default']['default']['password'];
  $host = isset($databases['default']['default']['host']) ? $databases['default']['default']['host'] : '127.0.0.1';

  echo "--host='$host' --user='$username' --password='$password' '$db'";
  exit(0);
}
else {
  echo "NODB" . PHP_EOL;
  exit(1);
}
