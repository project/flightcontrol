<?php
/**
 * @file
 * - Form alters for deployer.
 * - Node form validation (1 deploy per env at the same time).
 */

/**
 * Implements hook_form_alter().
 */
function deployer_form_alter(&$form, &$form_state, $form_id) {
  // Check for password fields.
  // Only save those if they are not empty.
  deployer_form_check_password_fields($form, $form_state);

  // Warn for removal of all projects / environments / snapshots related
  // to this client.
  if ($form_id == 'node_delete_confirm') {
    switch ($form['#node']->type) {
      case 'client':
        $warning = t('Please not that all underlying projects and environments will be removed from dashboard as well, including backups!');
        break;

      case 'project':
        $warning = t('Please not that all underlying environments will be removed from dashboard as well, including backups!');
        break;
    }

    if (isset($warning)) {
      $form['deployer_warning'] = array(
        '#type' => 'markup',
        '#markup' => theme('html_tag', array(
          'element' => array(
            '#tag' => 'div',
            '#value' => $warning,
          ),
        )),
        '#weight' => 0,
      );
    }
  }

  // Predefine project / other stuff for new environment.
  if (isset($form['type']['#value']) && $form['type']['#value'] == 'environment') {
    // Display public SSH key.
    if (!$rsa_keyfile = variable_get('deployer_rsa_keyfile')) {
      drupal_set_message(t('No SSH keyfile configured!<br/>You should fix this first.'));
    }
    else {
      if (!$pubkey = file_get_contents($rsa_keyfile . '.pub')) {
        drupal_set_message(t('Cannot read SSH public key!<br/>You should fix this first.'));
      }
      else {
        drupal_set_message(t('My public SSH key: <br/>%key', array('%key' => $pubkey)));
      }
    }

    // Hide health info and drupal version field.
    $form['field_health_info'][LANGUAGE_NONE][0]['value']['#type'] = 'hidden';
    $form['field_drupal_version'][LANGUAGE_NONE][0]['value']['#type'] = 'hidden';
    $form['field_last_checked'][LANGUAGE_NONE][0]['value']['#type'] = 'hidden';

    // Conditional fields.
    // Define switch on which to act.
    $switch = array(
      'select[name="field_connection_type[und]"]' => array('value' => 'ssh'),
    );

    // Set states for fields.
    foreach ($form['field_server_instance']['und'] as $key => &$element) {
      if (is_numeric($key)) {
        // Active mode, used for ftp.
        $element['field_active_mode']['und']['#states'] = array(
          'invisible' => $switch,
        );
        // Site instances, used for ftp.
        $element['field_site_instance']['#states'] = array(
          'invisible' => $switch,
          'optional' => $switch,
        );
        // Password, used for ftp.
        $element['field_password']['und'][0]['value']['#states'] = array(
          'invisible' => $switch,
        );

        // Owner (group), used for ssh.
        $element['field_owner_group']['und'][0]['value']['#states'] = array(
          'visible' => $switch,
          'required' => $switch,
        );
        // Owner (user), used for ssh.
        $element['field_owner_user']['und'][0]['value']['#states'] = array(
          'visible' => $switch,
          'required' => $switch,
        );
      }
    }
  }

  // Hide fields from project form.
  if (isset($form['type']['#value']) && $form['type']['#value'] == 'project') {
    // Hide field_repo_path.
    $form['field_repo_path']['#type'] = 'hidden';
  }
}

/**
 * Implements hook_node_validate().
 *
 * Make sure only 1 deploy is running on an environment
 * at the same time.
 */
function deployer_node_validate($node, $form, &$form_state) {
  if ($node->type == 'deploy') {
    if (isset($form_state['input']['field_environment_id']['und']) && !empty($form_state['input']['field_environment_id']['und'])) {

      // Only act on saving deploy node. Not on deleting.
      if ($form_state['clicked_button']['#submit'][0] == 'node_form_submit') {
        $env = $form_state['input']['field_environment_id']['und'];
        $query = db_select('node', 'n');
        $query->join('field_data_field_environment_id', 'env', 'n.nid = env.entity_id');
        $query->join('field_data_field_deploystatus', 'dstat', 'n.nid = dstat.entity_id');

        $query->fields('n', array('nid'));

        $query->condition('n.type', 'deploy');
        $query->condition('dstat.field_deploystatus_value', DEPLOY_STATUS_RUNNING);
        $query->condition('env.field_environment_id_target_id', $env);
        $result = $query->execute();

        $found = 0;
        foreach ($result as $row) {
          $found = 1;
        }

        if ($found) {
          form_set_error('Environment occupied', t('This environment is already occupied by another deploy! Please try again later.'));
        }
      }
    }
  }
}

/**
 * Check/fix form for password fields (type == '#deployer_password').
 *
 * Hide the password from the field, and only save
 * it if it's not empty.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 * @param string $field_name
 *   Field name. Only used in recursion.
 * @param array $parent_element
 *   Parent form array. Only used in recursion.
 */
function deployer_form_check_password_fields(&$form, &$form_state, $field_name = '', &$parent_element = array()) {
  if (is_array($form)) {
    if (isset($form['#deployer_password']) && $form['#deployer_password']) {
      // Protect this form element!
      if (isset($form_state['input']['op']) && empty($form_state['input'][$field_name])) {
        // Form submitted and password field is empty..
        unset($parent_element[$field_name]);
        return;
      }
      $form['#default_value'] = '';
    }

    foreach ($form as $key => &$element) {
      if (is_array($element)) {
        deployer_form_check_password_fields($element, $form_state, $key, $form);
      }
    }
  }
}
