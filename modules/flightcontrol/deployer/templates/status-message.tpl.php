<div class="deployer-status-notifications">
  <?php foreach ($messages as $message): ?>
    <div class="messge <?php print $message['class']; ?>">
      <div class="client"><?php print $message['client']; ?></div>
      <div class="project"><?php print $message['project']; ?></div>
      <div class="node-link"><?php print $message['link']; ?></div>
      <div class="message"><?php print $message['message']; ?></div>
    </div>
  <?php endforeach; ?>
</div>
