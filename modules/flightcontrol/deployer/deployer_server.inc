<?php
/**
 * @file
 * Server connections.
 */

/**
 * Create new ssh object to act on.
 *
 * The returned SFTP object is suitable for either shell-commands or
 * sftp-actions.
 *
 * @param string $host
 *   Hostname
 *
 * @param int $port
 *   Port
 *
 * @param string $username
 *   Username to use for login.
 *
 * @param string $application_path
 *   Application absolute path.
 *
 * @param string $webroot_path
 *   Webroot path, relative to app path.
 *
 * @return array
 *   Returns array.
 *   array[0]=false OR ssh/server object (Net_SFTP, which is extended Net_SSH2).
 *   array[1]=message string.
 */
function deployer_new_server_ssh($host, $port, $username, $application_path, $webroot_path) {

  $msg = '';
  _deployer_get_libraries();

  if (!$rsa_keyfile = variable_get('deployer_rsa_keyfile')) {
    $msg = "no rsa keyfile configured";
    return array(FALSE, $msg);
  }

  $server = new Net_SFTP($host, $port);
  $key = new Crypt_RSA();
  if (!$keyfile = file_get_contents($rsa_keyfile)) {
    $msg = "reading keyfile " . $rsa_keyfile . " not possible";
    return array(FALSE, $msg);
  }

  if (!$key->loadKey($keyfile)) {
    $msg = "Failed to load keyfile";
    return array(FALSE, $msg);
  }

  $start = time();
  if (!$server->login($username, $key)) {
    $duration = time() - $start;
    $msg = 'Cannot login, waited ' . $duration . 's';
    return array(FALSE, $msg);
  }

  // Check connection.
  $server->exec('exit 125');
  $status = $server->getExitStatus();
  if ($status !== 125) {
    $msg = 'Not able to connect';
    return array(FALSE, $msg);
  }

  $server->hostname = $host;
  $server->port = $port;
  $server->username = $username;
  $server->application_path = $application_path;

  // Set webroot relative to app-path.
  $webroot_path = ltrim($webroot_path, '/');
  if (!empty($webroot_path)) {
    $server->webroot_path_relative = $webroot_path;
    $server->webroot_path = $server->application_path . '/' . $webroot_path;
  }
  else {
    $server->webroot_path_relative = '';
    $server->webroot_path = $server->application_path;
  }

  $result = _deployer_server_basic_check_ssh($server);
  if (!$result[0]) {
    return array(FALSE, $result[1]);
  }

  return array($server, $msg);
}

/**
 * Create new ftp resource to act on.
 *
 * @param string $host
 *   Hostname
 *
 * @param int $port
 *   Port
 *
 * @param string $username
 *   Username to use for login.
 *
 * @param string $password
 *   Password to use for login.
 *
 * @param string $webroot
 *   Webroot relative path.
 *
 * @param int $active_mode
 *   1: active
 *   0: passive
 *
 * @return array
 *   Returns array.
 *   array[0]=false OR ftp resource.
 *   array[1]=message string.
 */
function deployer_new_server_ftp($host, $port, $username, $password, $webroot, $active_mode) {

  $msg = '';

  $server = new stdClass();
  $server->ftp = ftp_connect($host, $port, 10);
  $login_result = ftp_login($server->ftp, $username, $password);

  if ($active_mode) {
    ftp_pasv($server->ftp, FALSE);
  }
  else {
    ftp_pasv($server->ftp, TRUE);
  }

  // Check connection.
  if ((!$server->ftp) || (!$login_result)) {
    $msg .= t('FTP connection has failed to @u@@host:@port!', array(
      '@u' => $username,
      '@host' => $host,
      '@port' => $port,
    ));
  }
  else {
    _deployer_log(t('  => connected'));
    $server->hostname = $host;
    $server->port = $port;
    $server->username = $username;

    // Convert relative path to absolute path.
    if (strpos($webroot, "/") === 0) {
      $server->application_path = $webroot;
    }
    else {
      $init_path = ftp_pwd($server->ftp);
      $server->application_path = $init_path . '/' . $webroot;
    }

    _deployer_log('Webroot: ' . $server->application_path);

    $result = _deployer_server_basic_check_ftp($server);
    if (!$result[0]) {
      return array(FALSE, $result[1]);
    }
    $msg .= 'Login ok.';
  }

  return array($server, $msg);
}

/**
 * Reconnect and login ftp connection.
 *
 * @param object $server
 *   Server object.
 */
function deployer_reconnect_ftp(&$server) {
  _deployer_log(t('Reconnecting server'));
  ftp_close($server->ftp);
  unset($server->ftp);

  // Give server 2 seconds to rest :)  .
  sleep(2);

  $result = FALSE;

  if ($server->ftp = ftp_connect($server->hostname, $server->port, 10)) {
    // Login.
    $result = ftp_login($server->ftp, $server->username, $server->password);

    // Set mode.
    if ($server->active_mode) {
      ftp_pasv($server->ftp, FALSE);
    }
    else {
      ftp_pasv($server->ftp, TRUE);
    }

    // Change dir to where we were.
    if (isset($server->current_dir)) {
      _deployer_log(t('Changing back to @p', array('@p' => $server->current_dir)));
      _deployer_ftp_cmd('ftp_chdir', $server, array($server->current_dir), FALSE);
    }
  }

  if ($result) {
    _deployer_log(t('Reset connection succesfully'));
    return $server;
  }
  else {
    _deployer_log(t('Unable to reset connection'));
    return FALSE;
  }
}
