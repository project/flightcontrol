<?php
/**
 * @file
 * Provide code tags to clone / checkout.
 */

/**
 * Function to provide tags as options.
 *
 * Use hook_deployer_tag_option_provider() to provide option-providers for tags.
 * Expected return value: array(
 *   'type' => 'option_provider',
 * )
 * type: for example 'github' or 'company-subversion'
 * option_provider: function name providing options for this type,
 *
 * @param int $project_nid
 *   Node id of project.
 *
 * @param int $env_id
 *   Node id of environment.
 *
 * @return array
 *   Options array.
 */
function deployer_tags_list($env) {

  $options = array();
  $w = entity_metadata_wrapper('node', $env);
  $code_project = $w->field_project->field_code_project->value();

  // Return FALSE if data is missing.
  if (!$code_project) {
    return FALSE;
  }

  $repo_field = explode('|', $code_project);
  $repo_type = $repo_field[0];
  $repo = $repo_field[1];

  // Determine branch and environment type (test -> acc -> prod).
  $branch = $w->field_code_branch->value();
  $otap_type = $w->field_server_type->value();

  // Get current running version.
  $health_status = $w->field_health_info->value();
  if (!empty($health_status)) {
    $parts = explode('|', $health_status);
    if (isset($parts[2])) {
      $deployed_version = unserialize($parts[2]);
      $deployed_version = $deployed_version['tag'];
    }
  }

  $option_providers = module_invoke_all('deployer_tag_option_provider');
  foreach ($option_providers as $type => $provider) {
    if ($type == $repo_type) {
      if (function_exists($provider)) {
        if ($result = call_user_func_array($provider,
            array($repo, $branch, $otap_type))) {
          foreach ($result as $key => $value) {
            // Mark current version.
            if (isset($deployed_version)) {
              $key_parts = explode('|', $key);
              $tag = $key_parts[count($key_parts) - 1];
              if ($tag == $deployed_version) {
                $value = ' == [CURRENT] ==  ' . $value;
              }
            }
            $options[$key] = $value;
          }
        }
      }
    }
  }
  return $options;
}

/**
 * Function to provide code tarballs from provider.
 *
 * Use hook_deployer_codebase_provider(); to provide
 * codebase-path-providers for code.
 *
 * Expected return value: array(
 *   'type' => 'codebase-provider',
 * )
 * type: for example 'github' or 'company-subversion'
 * codebase-provider: function name providing a path to codebase.
 * This MUST contain a webroot without a parent folder.
 *
 * @param int $project_nid
 *   Node id of project.
 * @param string $tag
 *   Hash or tag.
 *
 * @return string
 *   Real filename of archive, or FALSE.
 */
function deployer_get_tarball_filename($project_nid, $tag) {

  // Extra libraries.
  $libraries = libraries_get_libraries();
  if (!isset($libraries['archive_tar'])) {
    watchdog('deployer', 'archive_tar LIBRARY NOT FOUND!', array(), WATCHDOG_ALERT, $link = NULL);
    return FALSE;
  }
  require_once $libraries['archive_tar'] . '/Archive/Tar.php';

  // Get repo, tag etc.
  $node = node_load($project_nid);
  if (!empty($node)) {
    $code_project = field_get_items('node', $node, 'field_code_project');
    $code_project = $code_project[0]['value'];
    $repo_field = explode('|', $code_project);
    $repo_type = $repo_field[0];
    $repo = $repo_field[1];
  }
  else {
    return FALSE;
  }

  // Fetch codebase.
  $codebase_path = FALSE;
  $codebase_providers = module_invoke_all('deployer_codebase_provider');
  foreach ($codebase_providers as $type => $provider) {
    if ($type == $repo_type) {
      if (function_exists($provider)) {
        if ($result = call_user_func_array($provider, array($repo, $tag))) {
          $codebase_path = $result;
          // Only first one.
          break;
        }
      }
    }
  }

  // Call codebase_alter hook, to provide interaction with codebase.
  // Then, pack the codebase into tarball, ready to deploy.
  if ($codebase_path) {
    // Call codebase-alters.
    $context = array(
      'type' => $repo_type,
      'path' => $codebase_path,
      'tag' => $tag,
      'project_nid' => $project_nid,
    );
    drupal_alter('deployer_codebase', $codebase_path, $context);

    // Create tarball.
    $identifier = uniqid();
    $temp_folder = file_directory_temp() . '/deploy';
    drupal_mkdir($temp_folder, NULL, TRUE);
    $filename = $temp_folder . '/codebase' . $identifier . '.tar.gz';
    $tar_object = new Archive_Tar($filename);

    $filelist = array();
    $filelist[0] = $codebase_path . '/';

    // Pack archive.
    $result = $tar_object->createModify($filelist, './', $codebase_path . '/');

    // Cleanup.
    // Remove temp folder.
    if (!variable_get('flightcontrol_keep_codebases_for_debug', FALSE)) {
      exec('rm -rf ' . $codebase_path);
    }

    if (!$result) {
      watchdog('deployer', 'Cannot repack archive from %f', array('%f' => $codebase_path), WATCHDOG_ALERT, $link = NULL);
      return FALSE;
    }

    return $filename;
  }

  return FALSE;
}

/**
 * Function to provide link for a tag.
 *
 * Use hook_deployer_tag_link_option_provider()
 * to provide option-providers for tag links.
 * Expected return value: array(
 *   'type' => 'option_provider',
 * )
 * type: for example 'github' or 'company-subversion'
 * option_provider: function name providing options for this type,
 *
 * @param int $project_nid
 *   Node id of project.
 *
 * @param int $env_id
 *   Node id of environment.
 *
 * @param string $tag_str
 *   Tag string.
 *
 * @param string $format
 *   Format for link. Full or short.
 *
 * @return array
 *   Options array.
 */
function deployer_tag_link($project_nid, $env_id, $tag_str, $format = 'full') {
  $node = node_load($project_nid);
  if (!empty($node)) {
    $code_project = field_get_items('node', $node, 'field_code_project');
    $code_project = $code_project[0]['value'];
    $repo_field = explode('|', $code_project);
    $repo_type = $repo_field[0];
    $repo = $repo_field[1];
  }
  else {
    return FALSE;
  }

  $env = node_load($env_id);
  $branch = field_get_items('node', $env, 'field_code_branch');
  $branch = $branch[0]['value'];

  $option_providers = module_invoke_all('deployer_tag_link_option_provider');
  foreach ($option_providers as $type => $provider) {
    if ($type == $repo_type) {
      if (function_exists($provider)) {
        if ($result = call_user_func_array($provider,
          array($repo, $branch, $tag_str, $format))) {
          if ($result) {
            return $result;
          }
        }
      }
    }
  }
  return FALSE;
}

/**
 * Function to provide link for a repository.
 *
 * Use hook_deployer_repo_link_option_provider()
 * to provide option-providers for repo-links.
 *
 * Expected return value: array(
 *   'type' => 'option_provider',
 * )
 * type: for example 'github' or 'company-subversion'
 * option_provider: function name providing options for this type,
 *
 * @param string $repo_str
 *   Tag string.
 *
 * @param string $format
 *   Could be full (whole link), or other (usually just plain url).
 *
 * @return string
 *   Link html string.
 */
function deployer_repo_link($repo_str, $format = 'full') {

  if (empty($repo_str)) {
    return $repo_str;
  }

  $repo_field = explode('|', $repo_str);
  $repo_type = $repo_field[0];
  $repo = $repo_field[1];

  $option_providers = module_invoke_all('deployer_repo_link_option_provider');
  if (!empty($option_providers)) {
    foreach ($option_providers as $type => $provider) {
      if ($type == $repo_type) {
        if (function_exists($provider)) {
          if ($result = call_user_func_array($provider,
            array($repo, $format))) {
            if ($result) {
              return $result;
            }
          }
        }
      }
    }
  }
  return $repo_str;
}

/**
 * Function to provide link for a tag.
 *
 * Use hook_deployer_tag_link_option_provider()
 * to provide option-providers for tag links.
 * Expected return value: array(
 *   'type' => 'option_provider',
 * )
 * type: for example 'github' or 'company-subversion'
 * option_provider: function name providing options for this type,
 *
 * @param int $project_nid
 *   Node id of project.
 *
 * @return array
 *   Options array.
 */
function deployer_repo_git_link($project_nid) {

  // Load project node,
  // find repository.
  $node = node_load($project_nid);
  if (!empty($node)) {
    $code_project = field_get_items('node', $node, 'field_code_project');
    $code_project = $code_project[0]['value'];
    $repo_field = explode('|', $code_project);
    $repo_type = $repo_field[0];
    $repo = $repo_field[1];
  }
  else {
    return FALSE;
  }

  // Search for backend providing clone-link for
  // repo-type (stash, github etc).
  $option_providers = module_invoke_all('deployer_git_repo_link_option_provider');
  foreach ($option_providers as $type => $provider) {
    if ($type == $repo_type) {
      if (function_exists($provider)) {
        if ($result = call_user_func_array($provider,
          array($repo))) {
          if ($result) {
            return $result;
          }
        }
      }
    }
  }
  return FALSE;
}
