<?php
/**
 * @file
 * Deploy batch actions / deploy logic.
 */

include_once 'deployer_batch_common_functions.inc';

/**
 * Main deploy function.
 *
 * Initiate deploy routine.
 * Batch Operation Callback.
 *
 * @param object $deploynode
 *   Deploy node.
 * @param array $context
 *   Context array from batch api.
 */
function deployer_batch_action_deploy_main($deploynode, &$context) {

  _deployer_log(t('Initiating routine.'), $deploynode, DEPLOYER_NO_DEBUG);

  // Create lockfile for this process.
  // It's will be removed on php shutdown, so we can see
  // if a deploy crashed or not.
  $lock = new DeployerLockNode($deploynode->nid);
  $lock->lock();

  // Set deploystatus on node to 0.5 (running).
  $temp_node = node_load($deploynode->nid);
  $temp_node->field_deploystatus[LANGUAGE_NONE][0]['value'] = DEPLOY_STATUS_RUNNING;
  node_save($temp_node);

  // Init vars.
  $servers = array();
  $deploy_context = &deployer_current_deploy_context();

  // Deploy-node.
  $deploy_context['deploy_node'] = $deploynode;

  // Set init status.
  // Setting this to one in the process will trigger rollback and failure.
  $deploy_context['failed'] = 0;

  // Set deploy mode.
  $deploy_context['mode'] = _deployer_ct_deploy_type($deploynode);

  // Set if backup is required.
  $webroot_backup = field_get_items('node', $deploynode, 'field_backup_webroot');
  $deploy_context['webroot_backup'] = $webroot_backup[0]['value'];
  $database_backup = field_get_items('node', $deploynode, 'field_backup_database');
  $deploy_context['database_backup'] = $database_backup[0]['value'];

  // Set connection type.
  // Default is ssh.
  // Load environment on which deploy is done.
  $env_field = field_get_items('node', $deploynode, 'field_environment_id');
  $deploy_context['env_id'] = $env_field[0]['target_id'];
  $env = node_load($deploy_context['env_id']);
  $deploy_context['environment'] = $env;
  $deploy_context['env_name'] = $env->title;

  // Store project / client name to context.
  $project = field_get_items('node', $env, 'field_project');
  $deploy_context['project_id'] = $project[0]['target_id'];
  $project = node_load($project[0]['target_id']);
  $deploy_context['project'] = $project;
  $deploy_context['project_name'] = $project->title;

  $client = field_get_items('node', $project, 'field_client');
  $deploy_context['client_id'] = $client[0]['target_id'];
  $client = node_load($client[0]['target_id']);
  $deploy_context['client_name'] = $client->title;

  // Connection type (ftp or ssh).
  $connection_type = field_get_items('node', $env, 'field_connection_type');
  if (!empty($connection_type)) {
    $connection_type = $connection_type[0]['value'];
  }
  else {
    $connection_type = 'ssh';
  }

  // Major drupal version.
  $deploy_context['major_version'] = field_get_items('node', $project, 'field_drupal_major_version');
  $deploy_context['major_version'] = $deploy_context['major_version'][0]['value'];

  // Deploy options.
  $deploy_options = field_get_items('node', $deploynode, 'field_deploy_options');
  $deploy_options = isset($deploy_options[0]['value']) ? json_decode($deploy_options[0]['value'], TRUE) : array();
  $deploy_context['deploy_options'] = $deploy_options;

  // Build with composer ?
  $build = field_get_items('node', $project, 'field_composer_build');
  $deploy_context['composer_build'] = isset($build[0]['value']) ? $build[0]['value'] : FALSE;

  // Get libraries.
  _deployer_get_libraries($deploy_context);

  // Trigger hooks for predeploy.
  deployer_batch_action_trigger_predeploy($deploynode, $deploy_context);

  if ($connection_type == 'ssh') {
    include_once 'deployer_batch_ssh_functions.inc';

    // Build servers.
    deployer_batch_action_init_servers_ssh($deploynode, $deploy_context, $servers);

    // Check for free disk space on remote machines.
    deployer_batch_action_estimate_free_space_ssh($deploynode, $deploy_context, $servers);

    // Retrieve codebase archive.
    deployer_batch_action_get_code_from_repo($deploynode, $deploy_context);

    // Check code archive.
    deployer_batch_action_check_code($deploynode, $deploy_context);

    // Set restore properties if any.
    deployer_batch_action_set_restore_props($deploynode, $deploy_context, $servers);

    // Check for drush and transport.
    deployer_batch_action_set_transport_drush_ssh($deploynode, $deploy_context, $servers);

    // Transport RPC to remote servers in case of restore.
    deployer_batch_action_transport_rpc_ssh($deploynode, $deploy_context, $servers);

    // Send webroot archive to remote server.
    deployer_batch_action_transport_archive_ssh($deploynode, $deploy_context, $servers);

    // Read site aliases with drush.
    deployer_batch_action_read_site_aliases_ssh($deploynode, $deploy_context, $servers);

    // Backup old database.
    deployer_batch_action_backup_old_database_ssh($deploynode, $deploy_context, $servers);

    // Backup old webroot.
    deployer_batch_action_backup_old_webroot_ssh($deploynode, $deploy_context, $servers);

    // Unpack webroot on remote server.
    deployer_batch_action_unpack_webroot_ssh($deploynode, $deploy_context, $servers);

    // Load databases in case of restore.
    deployer_batch_action_unpack_databases_ssh($deploynode, $deploy_context, $servers);

    // Rollback webroot in case of failure.
    deployer_batch_action_rollback_webroot_ssh($deploynode, $deploy_context, $servers);

    // Rollback databases in case of dailure.
    deployer_batch_action_rollback_database_ssh($deploynode, $deploy_context, $servers);

    // Finish deploy, cleanup etc.
    deployer_batch_action_finish_ssh($deploynode, $deploy_context, $servers);
  }

  elseif ($connection_type == 'ftp') {
    include_once 'deployer_batch_ftp_functions.inc';

    // Build servers.
    deployer_batch_action_init_servers_ftp($deploynode, $deploy_context, $servers);

    // Retrieve codebase archive.
    deployer_batch_action_get_code_from_repo($deploynode, $deploy_context);

    // Check code archive.
    deployer_batch_action_check_code($deploynode, $deploy_context);

    // Set restore properties if any.
    deployer_batch_action_set_restore_props($deploynode, $deploy_context, $servers);

    // Send webroot archive to remote server.
    deployer_batch_action_transport_webroot_ftp($deploynode, $deploy_context, $servers);

    // Put RPC on servers.
    // Makes me able to create db backups etc.
    deployer_batch_action_put_rpc_ftp($deploynode, $deploy_context, $servers);

    // Set sites on maintenance mode.
    deployer_batch_action_set_maintenance_on_ftp($deploynode, $deploy_context, $servers);

    // Backup old webroot.
    deployer_batch_action_backup_old_webroot_ftp($deploynode, $deploy_context, $servers);

    // Backup old database.
    deployer_batch_action_backup_old_database_ftp($deploynode, $deploy_context, $servers);

    // Unpack webroot on remote server.
    deployer_batch_action_place_webroot_ftp($deploynode, $deploy_context, $servers);

    // Load databases in case of restore.
    deployer_batch_action_unpack_databases_ftp($deploynode, $deploy_context, $servers);

    // Rollback webroot in case of failure.
    deployer_batch_action_rollback_webroot_ftp($deploynode, $deploy_context, $servers);

    // Rollback databases in case of dailure.
    deployer_batch_action_rollback_database_ftp($deploynode, $deploy_context, $servers);

    // Set sites on maintenance mode.
    deployer_batch_action_set_maintenance_off_ftp($deploynode, $deploy_context, $servers);

    // Finish deploy, cleanup etc.
    deployer_batch_action_finish_ftp($deploynode, $deploy_context, $servers);
  }

  if ($deploy_context['failed']) {
    $context['message'] = t('Deploy routine failed.');
  }
  else {
    $context['message'] = t('Deploy routine finished.');
  }

  // Trigger hooks for post deploy.
  module_invoke_all('deployer_postdeploy', $deploynode, $deploy_context);

}
