<?php
/**
 * @file
 * Deploy actions on node (deploy) save / publish.
 */

/**
 * Create deploy-batch for batch api.
 *
 * @param object $deploynode
 *   Node object (deploy node).
 */
function deployer_batch_deploy_project_to_env($deploynode) {

  // Set deploystatus on node to 0.1 (batch starting).
  // 0.5 means running.
  $temp_node = node_load($deploynode->nid);
  $temp_node->field_deploystatus[LANGUAGE_NONE][0]['value'] = DEPLOY_STATUS_INITIATING;

  $env_id = field_get_items('node', $deploynode, 'field_environment_id');
  $env_id = $env_id[0]['target_id'];
  $env = node_load($env_id);

  $project_id = field_get_items('node', $env, 'field_project');
  $project_id = $project_id[0]['target_id'];

  // Set tag link.
  if (_deployer_ct_deploy_type($deploynode) == 'deploy') {
    $tag_str = field_get_items('node', $deploynode, 'field_commit_tag');
    $tag_str = $tag_str[0]['value'];
    $temp_node->field_tag_link[LANGUAGE_NONE][0] = deployer_tag_link($project_id, $env_id, $tag_str, 'short');
  }

  // Save node.
  node_save($temp_node);

  $deploy_batch = array(
    'operations' => array(
      array('deployer_batch_action_deploy_main', array($deploynode)),
    ),
    'finished' => 'deployer_batch_finished',
    'title' => t('Processing deploy batch'),
    'init_message' => t('Batch is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Deploy has encountered an error.'),
    'file' => drupal_get_path('module', 'deployer') . '/deployer_batch.inc',
  );
  batch_set($deploy_batch);
  background_batch_process_batch('/node/' . $deploynode->nid);
}

/**
 * Batch 'finished' callback.
 */
function deployer_batch_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = count($results) . ' processed.';

    // D6 syntax.
    $message .= theme('item_list', $results);
    drupal_set_message($message);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments',
        array(
          '%error_operation' => $error_operation[0],
          '@arguments' => print_r($error_operation[1], TRUE),
        )
    );
    drupal_set_message($message, 'error');
  }
}
