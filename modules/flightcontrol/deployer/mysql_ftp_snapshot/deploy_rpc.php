<?php

/**
 * @file
 * Remote procedure call.
 *
 * Restore a mysql dump:
 * Post a file with name 'file'.
 *
 * Test url: http://bsg.dummy/deploy_rpc.php?key=[KEY]&site=default&action=dump
 */

include_once 'rpc_inc/mysqldump.php';
use Clouddueling\Mysqldump\Mysqldump;

global $rpc_ok, $error_msg;
$rpc_ok = 0;
$error_msg = array();

// Include context file with uuid etc.
if (is_file('rpc_inc/.rpc_context.php')) {
  include_once 'rpc_inc/.rpc_context.php';

  // Authenticate.
  if (isset($key) && !empty($key) && isset($_GET['key']) && $_GET['key'] == $key) {
    if (isset($_GET['site']) && !empty($_GET['site'])) {
      $set_file = 'sites/' . $_GET['site'] . '/settings.php';
      if (strpos($_GET['site'], '..') == FALSE && is_file($set_file) && filesize($set_file)) {
        include_once $set_file;

        // Set drupal major version and database settings.
        main_db_settings();

        switch ($_GET['action']) {
          case 'dump':
            dump_db();
            break;

          case 'restore':
            restore_db();
            break;

          case 'drop':
            drop_tables();
            break;

          case 'restore_no_drop':
            restore_tables();
            break;

          case 'maintenance_on':
            bootstrap_drupal();
            variable_set('site_offline', 1);
            $rpc_ok = 1;
            break;

          case 'maintenance_off':
            bootstrap_drupal();
            variable_set('site_offline', 0);
            $rpc_ok = 1;
            break;

        }
      }
      else {
        $error_msg[] = 'No site';
      }
    }
  }
  else {
    $error_msg[] = 'AUTH';
  }
}

print_status();
exit();

/**
 * Print status of RPC.
 */
function print_status() {
  global $rpc_ok, $error_msg;

  if ($rpc_ok) {
    echo 'ok';
  }
  else {
    header("HTTP/1.0 500 Internal Server Error");
    echo 'FALSE' . "\n";
    echo implode("\n", $error_msg);
  }
}

/**
 * Get database connection settings, set drupal version.
 */
function main_db_settings() {
  global $db, $type, $username, $password, $host;
  global $rpc_drupal_version, $databases, $db_url;

  // Get drupal 6/7 db properties.
  if (isset($db_url)) {
    $db = parse_url($db_url);
    $type = $db['scheme'];
    $host = $db['host'];
    $username = $db['user'];
    $password = $db['pass'];
    $db = preg_replace('/^\//i', '', $db['path']);
    $rpc_drupal_version = 6;
  }
  elseif (isset($databases)) {
    $type = $databases['default']['default']['driver'];
    $db = $databases['default']['default']['database'];
    $username = $databases['default']['default']['username'];
    $password = $databases['default']['default']['password'];
    $host = $databases['default']['default']['host'];
    $rpc_drupal_version = 7;
  }
  else {
    echo "NODB";
    exit();
  }
}

/**
 * Bootstrap drupal.
 */
function bootstrap_drupal() {
  define('DRUPAL_ROOT', getcwd());
  require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
}

/**
 * Dump drupal database for the called site alias.
 */
function dump_db() {
  global $rpc_ok, $error_msg;
  global $db, $type, $username, $password, $host;

  // Set dump properties.
  $dump_settings = array(
    'include-tables' => array(),
    'exclude-tables' => array(),
    'compress' => 'GZIP',
    'no-data' => FALSE,
    'add-drop-database' => FALSE,
    'add-drop-table' => FALSE,
    'single-transaction' => FALSE,
    'lock-tables' => FALSE,
    'add-locks' => TRUE,
    'extended-insert' => TRUE,
    'disable-foreign-keys-check' => FALSE,
  );

  if (isset($db) && isset($username) && isset($password) && isset($host) && isset($type)) {
    if ($type == 'mysqli') {
      $type = 'mysql';
    }

    // Only dump mysql files.
    if ($type == 'mysql') {
      $tempfile = sys_get_temp_dir() . '/._mysqldump_' . uniqid() . '.sql';

      $dump = new Mysqldump($db, $username, $password, $host, $type, $dump_settings);

      $dump->start($tempfile);

      $tempfile .= '.gz';
      if (file_exists($tempfile) && filesize($tempfile)) {
        if ($handler = fopen($tempfile, 'r')) {
          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Transfer-Encoding: binary');
          header('Expires: 0');
          header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
          header('Pragma: public');
          header('Content-Disposition: attachment; filename="' . basename($tempfile) . '"');
          header('Content-Length: ' . filesize($tempfile));

          // Send the content in chunks.
          while ($chunk = fread($handler, 4096)) {
            echo $chunk;
          }

        }

        unlink($tempfile);
      }
      exit();
    }
  }
  else {
    $error_msg[] = 'DB settings not complete';
  }
}

/**
 * Main drop / restore function.
 */
function restore_db() {
  global $rpc_ok, $error_msg;

  if (isset($_GET['file']) && file_exists($_GET['file'])) {
    // Drop tables.
    drop_tables();

    // Restore database.
    if ($rpc_ok) {
      $rpc_ok = 0;
      restore_tables();
    }
    else {
      $error_msg[] = 'Unable to drop db';
    }
  }
  else {
    $error_msg[] = 'Upload failed';
  }
}

/**
 * Drop database tables for 'this' site alias.
 */
function drop_tables() {
  global $rpc_ok;
  global $db, $type, $username, $password, $host;

  if (isset($db) && isset($username) && isset($password) && isset($host) && isset($type)) {
    if ($type == 'mysqli') {
      $type = 'mysql';
    }

    /* Connect to an ODBC database using driver invocation */
    $dsn = 'mysql:dbname=' . $db . ';host=' . $host;

    $dbh = new PDO($dsn, $username, $password);
    if ($dbh) {
      // Get all tables of db from information_schema db.
      $sql = 'SELECT table_name FROM information_schema.tables WHERE information_schema.tables.table_schema = "' . $db . '"';
      $result = $dbh->query($sql);

      // Drop all tables.
      foreach ($result as $row) {
        $sql = 'DROP TABLE IF EXISTS `' . $db . '`.`' . $row[0] . '`';
        $dbh->exec($sql);
      }
      $rpc_ok = 1;
    }
  }
}

/**
 * Restore a databasedump from post upload.
 */
function restore_tables() {
  global $rpc_ok, $error_msg;
  global $db, $type, $username, $password, $host, $dump_file;

  if (isset($_GET['file']) && file_exists($_GET['file'])) {
    $dump_file = $_GET['file'];

    if (isset($db) && isset($username) && isset($password) && isset($host) && isset($type)) {
      if ($type == 'mysqli') {
        $type = 'mysql';
      }

      if (is_file('rpc_inc/mysql_load.php')) {
        $rpc_ok = 1;
        include_once 'rpc_inc/mysql_load.php';
      }
    }
  }
  else {
    $error_msg[] = 'Unable to restore db';
  }
}
