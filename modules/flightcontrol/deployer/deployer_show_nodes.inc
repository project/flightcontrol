<?php
/**
 * @file
 * Show deploy information after it has happened.
 */

/**
 * Display health for server.
 *
 * @param object $field
 *   Field object.
 *
 * @return string
 *   Health information html.
 */
function deployer_show_server_health_full($field) {

  $cid = $field['entity']->item_id;

  $query = db_select('deployer_health_info', 's');
  $query->condition('s.cid', $cid);
  $query->fields('s', array(
    'cid',
    'alive',
    'is_drupal',
    'version',
    'updates',
    'deployed_version',
  ));
  $results = $query->execute();

  $variables['title'] = t('Health information');
  $variables['type'] = 'ul';
  $variables['attributes'] = array();
  $variables['items'] = array();

  $rowcount = $results->rowCount();
  if (empty($rowcount)) {
    $variables['title'] = t('Health information not available');
  }
  else {
    foreach ($results as $row) {

      // Server alive.
      if ($row->alive) {
        $alive = t('running');
      }
      else {
        $alive = t('down');
      }
      $variables['items'][] = t('Server status:') . ' ' . $alive;

      // Is drupal webroot.
      if ($row->is_drupal) {
        $drupal = t('yes');
      }
      else {
        $drupal = t('no');
      }
      $variables['items'][] = t('Drupal webroot:') . ' ' . $drupal;

      // Running version tag.
      $deployed_version_parts = unserialize(base64_decode($row->deployed_version));
      if (!empty($deployed_version_parts)) {
        $variables['items'][] = t('Running version / tag:') . ' ' . l($deployed_version_parts['tag'], $deployed_version_parts['url']);
      }

      // Drupal version.
      if (empty($row->version)) {
        $version = t('unknown');
      }
      else {
        $version = $row->version;
      }
      $variables['items'][] = t('Drupal version:') . ' ' . $version;

      // Updates.
      $updates = unserialize($row->updates);
      if (!empty($updates)) {
        $update_array['data'] = t('Updates');
        foreach ($updates as $update) {
          $mod_array = array();
          $mod_array['data'] = l($update['module'], 'https://drupal.org/project/' . $update['module']);
          $mod_array['children'][] = t('Current version:') . ' ' . $update['version'];
          $mod_array['children'][] = t('New version:') . ' ' . $update['new_version'];
          $mod_array['children'][] = t('Type:') . ' ' . $update['type'];

          $update_array['children'][] = $mod_array;
        }
        $variables['items'][] = $update_array;
      }
      else {
        $variables['items'][] = t('No updates');
      }

      break;
    }
  }

  return theme_item_list($variables);
}

/**
 * Return backup link for environment.
 *
 * @param object $field
 *   Field object, containing node.
 *
 * @return string
 *   HTML.
 */
function deployer_env_backup_link($field) {
  $node = $field['entity'];
  if ($node->type == 'environment') {
    return l(t('Backup'), 'node/add/deploy', array('query' => array('env' => $node->nid, 'backup' => 1)));
  }

  return FALSE;
}

/**
 * Return deploy link for environment.
 *
 * @param object $field
 *   Field object, containing node.
 *
 * @return string
 *   HTML.
 */
function deployer_env_deploy_link($field) {
  $node = $field['entity'];
  if ($node->type == 'environment') {
    return l(t('Deploy'), 'node/add/deploy', array('query' => array('env' => $node->nid)));
  }
  return FALSE;
}


/**
 * Return connected projects output for client.
 *
 * @param object $field
 *   Field object.
 *
 * @return string|bool
 *   HTML output or FALSE.
 */
function deployer_client_connected_projects($field) {
  return views_embed_view('projects', 'block', $field['entity']->nid);
}
