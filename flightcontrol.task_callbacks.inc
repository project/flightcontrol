<?php
/**
 * @file
 * This file includes helper functions for the flightcontrol profile.
 */

/**
 * Revert dfc features.
 */
function _flightcontrol_revert_features() {

  $features = array(
    'deployer_feature_fc_server_site',
    'deployer_feature_misc_config',
    'deployer_feature_view_running_deploys',
    'deployer_feature_view_statistics',
//    'deployer_update_feature_view_auto_updates',
    'deployer_update_fields_project_auto_updates',
  );

  $reverts = array();
  foreach ($features as $feature) {
    $feature_module = features_get_features($feature);
    $components = array_keys($feature_module->info['features']);
    $reverts[$feature] = $components;
  }

  features_revert($reverts);
}

/**
 * BatchAPI callback.
 *
 * Enable and set a default theme.
 */
function _flightcontrol_enable_theme($theme, &$context) {
  theme_enable(array($theme));
  variable_set('theme_default', $theme);
  theme_disable(array(
    'bartik',
    'seven',
    'stark',
  ));
}

/**
 * BatchAPI callback.
 *
 * Set a logo.
 */
function _flightcontrol_set_logo($operation, &$context) {

  $garland_settings = variable_get('theme_garland_settings', array());
  $garland_settings['logo_path'] = 'profiles/flightcontrol/modules/flightcontrol/deployer/images/logo.png';
  $garland_settings['default_logo'] = 0;

  variable_set('theme_garland_settings', $garland_settings);

  $context['message'] = t('@operation', array('@operation' => $operation));
}

/**
 * BatchAPI callback.
 *
 * Configure filesystem.
 */
function _flightcontrol_configure_filesystem($operation, &$context) {
  // Configure filesystem.
  $pub_path = variable_get('file_public_path', 'sites/default/files');
  if (!is_dir($pub_path . '/private') && !drupal_mkdir($pub_path . '/private', NULL, TRUE)) {
    drupal_set_message('Unable to configure private filesystem. Please do so manually, or Drupal Fight Control will fail!');
  }
  else {
    variable_set('file_private_path', $pub_path . '/private');
    variable_set('file_default_scheme', 'private');
  }

  $context['message'] = t('@operation', array('@operation' => $operation));
}

/**
 * Install task callback.
 *
 * Put blocks in place.
 */
function _flightcontrol_set_blocks() {
  // Define each block here, and set properties accordingly.
  // Note, visibility => 0 means 'All pages except those listed'.
  // visibility => 1 means 'Only the listed pages'.
  $blocks = array(
    'main' => array(
      'module' => 'system',
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
    ),
    'login' => array(
      'module' => 'user',
      'status' => 1,
      'weight' => 0,
      'region' => 'sidebar_first',
    ),
    'navigation' => array(
      'module' => 'system',
      'status' => 1,
      'weight' => 0,
      'region' => 'sidebar_first',
    ),
    'management' => array(
      'module' => 'system',
      'status' => 1,
      'weight' => 1,
      'region' => 'sidebar_first',
    ),
    'help' => array(
      'module' => 'system',
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
    ),
    'deployer_help' => array(
      'module' => 'deployer_help',
      'region' => 'content',
      'weight' => -11,
      'pages' => '<front>',
      'visibility' => 1,
    ),
    'deployer_status_messages' => array(
      'module' => 'deployer',
      'region' => 'content',
      'weight' => 0,
      'pages' => '<front>',
      'visibility' => 1,
    ),
    'deployer_manage_links' => array(
      'module' => 'deployer',
      'region' => 'sidebar_first',
      'weight' => 0,
    ),
    'deployer_context_block' => array(
      'module' => 'deployer',
      'region' => 'help',
      'weight' => 0,
      'pages' => '<front>',
      'visibility' => 0,
    ),
  );

  // Build / execute query.
  $query = db_insert('block')->fields(array(
    'module',
    'delta',
    'theme',
    'status',
    'weight',
    'region',
    'pages',
    'cache',
  ));
  $query->execute();
  foreach ($blocks as $delta => $info) {
    db_merge('block')
      ->key(array(
        'theme'  => 'garland',
        'delta'  => $delta,
        'module' => $info['module'],
      ))
      ->fields(array(
        'region'     => $info['region'],
        'pages'      => isset($info['pages']) ? $info['pages'] : '',
        'status'     => 1,
        'weight'     => $info['weight'],
        'visibility' => isset($info['visibility']) ? $info['visibility'] : 0,
      ))
      ->execute();
  }
}

/**
 * BatchAPI callback.
 *
 * Enable a module.
 */
function _flightcontrol_enable_module($module, &$context) {
  module_enable(array($module), FALSE);
  $context['message'] = st('Installed %module module.', array('%module' => $module));
}

/**
 * Install or add variable configurations to the system.
 */
function _flightcontrol_variable_configurations() {
  $exports = module_invoke_all('flightcontrol_variable_configs');

  // Default variables.
  $exports += array(
    // Drush versions path.
    'deployer_drush_versions_path' => 'private://drush',
    // Block-refresh settings.
    'block_refresh_settings' => array(
      'block-views-running-deploys-block' => array(
        'element' => 'block-views-running-deploys-block',
        'auto' => 1,
        'manual' => 0,
        'init' => 0,
        'arguments' => 1,
        'panels' => 0,
        'timer' => '3',
        'block' => array(
          'block' => 'views',
          'delta' => 'running_deploys-block',
        ),
        'bypass_page_cache' => 0,
        'bypass_external_cache' => '',
      ),
      'block-deployer-flightcontrol-bookmarks' => array(
        'element' => 'block-deployer-flightcontrol-bookmarks',
        'auto' => 0,
        'manual' => 1,
        'init' => 0,
        'arguments' => 1,
        'panels' => 0,
        'timer' => '3',
        'block' => array(
          'block' => 'deployer',
          'delta' => 'flightcontrol_bookmarks',
        ),
        'bypass_page_cache' => 0,
        'bypass_external_cache' => '',
      ),
    ),
  );

  foreach ($exports as $key => $value) {
    variable_set($key, $value);
  }
}

/**
 * Init (or re-init) SSH keyfile.
 *
 * This will create a new, unique folder in the private filesystem and
 * generate a keypair.
 *
 * NOTE: Running this will break current authentication with all remote systems.
 */
function _flightcontrol_init_ssh_key() {
  $keyfile = 'private://.ssh_' . uniqid() . '/id_rsa';
  variable_set('deployer_rsa_keyfile', $keyfile);
  deployer_create_ssh_key($keyfile);
}
