<?php
/**
 * @file
 * This file holds some common helper functions.
 */

/**
 * Callback: array_filter() callback.
 *
 * Used to filter out already installed dependencies.
 */
function _flightcontrol_filter_dependencies($dependency) {
  return !module_exists($dependency);
}

/**
 * Resolve module dependencies.
 *
 * Resolve the dependencies now, so that module_enable() doesn't need to
 * do it later for each individual module (which kills performance).
 *
 * @param array $modules
 *   Modules array.
 */
function _flightcontrol_resolve_dependencies(array &$modules) {
  $files = system_rebuild_module_data();
  $modules_sorted = array();
  foreach ($modules as $module) {
    if ($files[$module]->requires) {
      // Create a list of dependencies that haven't been installed yet.
      $dependencies = array_keys($files[$module]->requires);
      $dependencies = array_filter($dependencies, '_flightcontrol_filter_dependencies');
      // Add them to the module list.
      $modules = array_merge($modules, $dependencies);
    }
  }

  $modules = array_unique($modules);
  foreach ($modules as $module) {
    $modules_sorted[$module] = $files[$module]->sort;
  }
  arsort($modules_sorted);
  $modules = $modules_sorted;
}

/**
 * BatchAPI callback.
 *
 * Flush cache.
 */
function _flightcontrol_flush_caches($operation, &$context) {
  $context['message'] = t('@operation', array('@operation' => $operation));
  drupal_flush_all_caches();
}
