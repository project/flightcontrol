<?php
/**
 * @file
 * This file holds installer tasks.
 */

/**
 * Enable all the necessary modules and install settings.
 *
 * Here i only enable modules but don't do the actual installments
 * e.g of content types.
 *
 * @return array
 *   A list of batch process that will take place.
 */
function _flightcontrol_enable_modules() {

  // Modules to enable.
  $modules = array(
    'deployer',
    'deployer_ct_client',
    'deployer_ct_deploy',
    'deployer_ct_project',
    'deployer_ct_environment',
    'deployer_ctypes',
    'deployer_ds_and_fieldgroups',
    'deployer_export_blocks',
    'deployer_permissions',
    'deployer_views',
    'deployer_gitlab',
    'deployer_git_api',
    'deployer_bitbucket',
    'deployer_stash',
    'deployer_github',
    'deployer_hipchat',
    'deployer_update',
    'deployer_help',
    'deployer_drush_manager',
    'deployer_build_composer',
    'deployer_drupal7',
  );

  $operations = array();

  // Enable and set as default the correct theme.
  $theme = 'garland';
  $operations[] = array('_flightcontrol_enable_theme', array($theme));

  // Set logo.
  $operations[] = array(
    '_flightcontrol_set_logo',
    array('Setting flight control logo.'),
  );

  // Enable the selected modules.
  foreach ($modules as $module) {
    $operations[] = array(
      '_flightcontrol_enable_module',
      array($module),
    );
  }

  // Configure filesystem.
  $operations[] = array(
    '_flightcontrol_configure_filesystem',
    array('Configure filesystem.'),
  );

  // Put some blocks in place.
  $operations[] = array(
    '_flightcontrol_set_blocks',
    array('Put blocks in place'),
  );

  // Flush cache.
  $operations[] = array(
    '_flightcontrol_flush_caches',
    array('Flushing caches.'),
  );

  // Add all operations to the batch.
  $batch = array(
    'title'      => t('Installing flight control modules'),
    'operations' => $operations,
    'file' => drupal_get_path('profile', 'flightcontrol') . '/flightcontrol.task_callbacks.inc',
  );

  return $batch;
}

/**
 * Task callback: Install settings / configurations.
 */
function _flightcontrol_install_settings() {

  // These functions are logically ordered.
  // Please don't change the order unless you are sure of what you are doing.
  $operations = array(
    array('_flightcontrol_variable_configurations', array()),
    array('_deployer_permissions_install_roles', array()),
    array('_deployer_ctypes_install_entity_view_modes', array()),
    array('_deployer_ctypes_add_content_types', array()),
    array('_deployer_export_blocks_install', array()),
    array('_deployer_permissions_install_perms', array()),
    array('deployer_drush_manager_fetch_latest', array()),
    array('_flightcontrol_init_ssh_key', array()),
  );

  $batch = array(
    'title'      => t('Installing additional functionality'),
    'operations' => $operations,
    'file' => drupal_get_path('profile', 'flightcontrol') . '/flightcontrol.task_callbacks.inc',
  );

  return $batch;
}
